QT += core network widgets
QT -= gui

CONFIG += c++11

TARGET = server_NotHTML
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    mytcpserver.cpp \
    ../ProtocBuff/PB/JavaScript.pb.cc \
    ../ProtocBuff/PB/Layout.pb.cc \
    ../ProtocBuff/PB/StyleSheet.pb.cc

HEADERS += \
    mytcpserver.h \
    ../ProtocBuff/PB/JavaScript.pb.h \
    ../ProtocBuff/PB/Layout.pb.h \
    ../ProtocBuff/PB/StyleSheet.pb.h \
    ../ImageLogic.h

SOURCES +=../ProtocBuff/PB/Picture.pb.cc \
    ../ProtocBuff/PB/Element.pb.cc \
    ../ProtocBuff/PB/Page.pb.cc \
    ../ProtocBuff/PB/Button.pb.cc \
    ../ProtocBuff/PB/Rect.pb.cc \
    ../ProtocBuff/PB/Light_Element.pb.cc

HEADERS +=../ProtocBuff/PB/Picture.pb.h \
    ../ProtocBuff/PB/Element.pb.h \
    ../ProtocBuff/PB/Page.pb.h \
    ../ProtocBuff/PB/Button.pb.h \
    ../ProtocBuff/PB/Rect.pb.h \
    ../ProtocBuff/PB/Light_Element.pb.h

win32: LIBS += -L$$PWD/../ProtocBuff/lib_X64_PB_31/ -llibprotobuf
INCLUDEPATH += $$PWD/../ProtocBuff/Sourse

VERSION = 0.0.0.2
QMAKE_TARGET_COMPANY = Zcom
QMAKE_TARGET_PRODUCT = qmake demo nothtml
QMAKE_TARGET_DESCRIPTION = not web not html
QMAKE_TARGET_COPYRIGHT = (c) Andrey Z
