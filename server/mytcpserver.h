#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>

#include <QAbstractSocket>
#include <QTcpServer>
#include <QTcpSocket>
#include <QCache>
#include <QString>

#include <PB/Light_Element.pb.h>

//class MyTcpServer : public QTcpServer
class MyTcpServer : public QObject
{
    Q_OBJECT

    QTcpServer tcpServer;
    QMap<int,QTcpSocket *> ClientsConnect;
    void ServerSend(QTcpSocket* conectSoket);
    QTcpSocket* clientSocket;


public:
    explicit MyTcpServer(QObject *parent = 0);
    //MyTcpServer();
    ~MyTcpServer();
    void acceptConnection();
    void SocketError(QAbstractSocket::SocketError socketError);

    QString SendLogic();
	

	QByteArray SendLogic2();
	QByteArray SendLogicN2();
    QCache<QString,QString> MessegeCache;
	
};

#endif // MYTCPSERVER_H
