#include "mytcpserver.h"

#ifdef _MSC_EXTENSIONS
#pragma warning( push )
#pragma warning( disable : 4100 ; disable : 4267)
#endif // _MSC_EXTENSIONS

#include <PB/Page.pb.h>
#include <PB/Light_Element.pb.h>
#include <PB/Button.pb.h>
#include <PB/Layout.pb.h>
#include <PB/Label.pb.h>
#include <PB/Picture.pb.h>
#include <PB/StyleSheet.pb.h>
#include <PB/JavaScript.pb.h>

#ifdef _MSC_EXTENSIONS
#pragma warning( pop )
#endif // _MSC_EXTENSIONS

#include <google\protobuf\message.h>

#include "../for_messege.h"
#include "../ImageLogic.h"
#include <QtConcurrent>
using namespace QtConcurrent;

MyTcpServer::MyTcpServer(QObject *parent) : QObject(parent)
//MyTcpServer::MyTcpServer()
{
    qInfo() << "Start MyTcpServer" << endl;
//    tcpServer.listen(QHostAddress::Any, 188);
    //connect(&tcpServer, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),this, &MyTcpServer::SocketError);
    connect(&tcpServer, &QTcpServer::newConnection, this, &MyTcpServer::acceptConnection);

   tcpServer.listen(QHostAddress::Any, 188);
   SendLogicN2();
}

MyTcpServer::~MyTcpServer()
{
    qInfo() << "Server Fini";
    tcpServer.close();

}


/**
 * @brief      generet messege
 *
 * @param[in]  need  The need
 *
 * @return     array of messege
 */
QString genereete_messege(QString need){
    QString mess_ret;

    ///todo: to enum
    QChar firstC = need[0];
    int cas;
    if(firstC == "h") cas = 1;
    if(firstC == "b") cas = 2;
    if(firstC == "p") cas = 3;
    if(firstC == "l") cas = 4;
    if(firstC == "t") cas = 5;
    //if(firstC == "e")
    switch (cas){
    case 1:{
        nothtml::Page page1;
        page1.set_vers(0);
        page1.set_num(1);

        std::string *std_str;
        std_str = new std::string;
        page1.SerializeToString(std_str);

        mess_ret = QString::fromStdString(*std_str);
        break;
    }
    case 2:{
        nothtml::Button Button1;
        Button1.set_text("nananana");
        Button1.set_action("digitClicked");

        nothtml::Rect *pbRect = Button1.mutable_position();
        pbRect->set_x(30);
        pbRect->set_y(30);
        pbRect->set_height(50);
        pbRect->set_width(100);

        std::string *std_str;
        std_str = new std::string;
        Button1.SerializeToString(std_str);

        mess_ret = QString::fromStdString(*std_str);
        break;
    }
    case 3:{
        nothtml::Picture picture1;

        picture1.set_pict_array(ImegToStr("C:/Users/Andrey/OneDrive/Pictures/windows fothos/unnamed.jpg").toStdString());

        std::string *std_str;
        std_str = new std::string;
        picture1.SerializeToString(std_str);

        mess_ret = QString::fromStdString(*std_str);
        break;
    }
    case 4:{
        nothtml::Layout layaut1;
        layaut1.set_num(2);
        layaut1.set_type(nothtml::Layout_Element::Vertical);

        std::string *std_str;
        std_str = new std::string;
        layaut1.SerializeToString(std_str);

        mess_ret = QString::fromStdString(*std_str);
        break;
    }
    default:{

        break;
    }
    }

    return mess_ret;
}


/**
 * @brief      Sends a logic. cache messege
 *
 * @return     { description_of_the_return_value }
 * todo: 
 */
QString MyTcpServer::SendLogic(){
    QString ret_str;


    if(MessegeCache.contains("head")){
        ret_str += *MessegeCache.object("head");
    }else{
        QString *temp_str;
        temp_str = new QString(genereete_messege("head"));
        QString temp("head");
        MessegeCache.insert(temp,temp_str);
        ret_str+=temp_str;
    }

    QString elem_need("layout");
    if(MessegeCache.contains(elem_need)){
        ret_str += *MessegeCache.object(elem_need);
    }else{
        QString *temp_str;
        temp_str = new QString(genereete_messege(elem_need));
        MessegeCache.insert(elem_need,temp_str);
        ret_str+=temp_str;
    }




    return ret_str;
}


/**
 * @brief      Sends a logic 2.
 *  temp function -> todo:
 * @return     { description_of_the_return_value }
 */
QByteArray MyTcpServer::SendLogic2(){
	QByteArray ret_ser;

    return ret_ser;

}

QByteArray MyTcpServer::SendLogicN2()
{
	QByteArray ret_Arr;

	nothtml::Page page1;
	page1.set_vers(33);
	page1.set_num(1);
	//page1.set_type(nothtml::LAYOUT);
	
	std::string *std_str = new std::string;
	page1.SerializeToString(std_str);
	ret_Arr.append(std_str->data(), std_str->length());
	delete std_str;

	nothtml::Layout layaut1;
	layaut1.set_num(6);
	layaut1.set_type(nothtml::Layout_Element::Vertical);
	ret_Arr += convert(layaut1);

	nothtml::Button Button1;
	Button1.set_text("nananana");
	Button1.set_action("digitClicked");

	nothtml::Rect *pbRect = Button1.mutable_position();
	pbRect->set_x(30);
	pbRect->set_y(30);
	pbRect->set_height(50);
	pbRect->set_width(100);

	ret_Arr += convert(Button1);

	Button1.set_text("olololo");
	Button1.set_action("digitClicked");

	pbRect = Button1.mutable_position();
	pbRect->set_height(50);
	pbRect->set_width(100);
	pbRect->set_x(80);
	pbRect->set_y(130);

	ret_Arr += convert(Button1);

	nothtml::JavaScript jscript1;
	QFile file("./../client/Code2.js");
	file.open(QIODevice::ReadOnly);
	QByteArray temp_arr = file.readAll();
	file.close();

	jscript1.set_script(temp_arr.toStdString());

	ret_Arr += convert(jscript1);
	
	nothtml::Picture picture1;
	//picture1.set_pict_array(ImegToStr("./../frfr.jpg").toStdString());
	picture1.set_pict_array("./../frfr.jpg");
	
	ret_Arr += convert(picture1);

	nothtml::Label label1;
	label1.set_text("hellow text");

	ret_Arr += convert(label1);

	nothtml::StyleSheet style1;
	style1.set_sheet("* { color: blue; background: yellow; float: right }");

	ret_Arr += convert(style1);

	return ret_Arr;
}

/**
 * @brief      send to client
 *
 * @param      conectSoket  The conect soket
 */
void MyTcpServer::ServerSend(QTcpSocket *conectSoket){
    qInfo() << "ServerSend";
    
	QByteArray tosend;
	tosend = SendLogicN2();
	conectSoket->write(tosend);
    conectSoket->close();
    qInfo() << "EndSend";
    
    
    /*
    qInfo() << "ServerSend";
    QByteArray block("hi mt tcp");
    QDataStream out();// &block);

    QTcpSocket* clientSocket = (QTcpSocket*)QObject::sender();
    clientSocket->write(block);
    clientSocket->close();
    qInfo() << "EndSend";
    */
}


/**
 * @brief      accept conect oure user
 */
void MyTcpServer::acceptConnection()
{

	qInfo() << "acceptConnection";
	//tcpServerConnection = tcpServer.nextPendingConnection();
    //QTcpSocket* clientSocket = tcpServer.nextPendingConnection();
	QTcpSocket* Socket_temp = tcpServer.nextPendingConnection();

	///todo:
	//QFuture<void> future = QtConcurrent::run(this, &MyTcpServer::ServerSend, Socket_temp);

    ServerSend(Socket_temp);

    qInfo() << "acceptConnection end";

}


void MyTcpServer::SocketError(QAbstractSocket::SocketError socketError)
{
    if (socketError == QTcpSocket::RemoteHostClosedError)
        return;

    //QTcpSocket* clientSocket = (QTcpSocket*)sender;
    qWarning() << "Network error" << "The following error occurred:" ;//<< clientSocket.errorString();
}
