#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtGuiApplication1test.h"

class QtGuiApplication1test : public QMainWindow
{
	Q_OBJECT

public:
	QtGuiApplication1test(QWidget *parent = Q_NULLPTR);

	void SetTitle(QString str);

public slots:
	void on_pushButton_clicked();

private:
	Ui::QtGuiApplication1testClass ui;
};
