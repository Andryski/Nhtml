#include "QtGuiApplication1test.h"

//#include "../JSModule/JSModule.h"

#include <QLibrary>

#include "../Dino/Api/IRuner.h"
#include <functional>
#include "../Dino/Api/IRunerCallBack.h"

class IRunerCallBack;

QtGuiApplication1test::QtGuiApplication1test(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

void QtGuiApplication1test::SetTitle(QString str)
{
	QString test = str;
	setWindowTitle(test);
	//QCoreApplication::setT()
}

void QtGuiApplication1test::on_pushButton_clicked()
{
	QLibrary myLib("JSModule");
	if (myLib.load())
	{
		IRuner *runer;
		typedef IRuner* (*create_Runer_from_DLL)();
		
		typedef int* *intres;
		create_Runer_from_DLL create_TestDLL_lib = (create_Runer_from_DLL)myLib.resolve("getNewClass");
		
		typedef void (*seter)(int);
		seter perem = (seter)myLib.resolve("SetInt");
		
		if(perem)
		{
			perem(3);
			int g = 0;
			g+=3;
		}

		ISRunerCallBack map;
		map.setTitle = [&](QString rt)
		{
			SetTitle(rt);
		};

		typedef void(*setersettitle)(std::function<void(QString)>);
		setersettitle peremsettitle = (setersettitle)myLib.resolve("Setfunsettitle");
		if(peremsettitle)
		{
			/*peremsettitle([&](QString rt)
			{
				SetTitle(rt);
			});*/
		}

		typedef void(*calbacke)(ISRunerCallBack);
		calbacke peremsettitle2 = (calbacke)myLib.resolve("Setfuncalbacke");
		if (peremsettitle2)
		{
			peremsettitle2(map);
		}

		if (create_TestDLL_lib)
			runer = create_TestDLL_lib();

		runer->Start();
		QString temp_test1 = runer->gettest();
		this->ui.lineEdit->setText(QString("test"));
	}
}

