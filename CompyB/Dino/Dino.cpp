#include "Dino.h"

#include <iostream>
#include <string>
#include <functional>

#include <QtWidgets>
#include <QBoxLayout>

#include "Api/IRunerCallBack.h"
#include "Api/IRAndC.h"
#include "Api/Media/MediaApp.h"
#include <Gui_draw_engine/LayoutDrawer.h>
#include "Gui_draw_engine/DrawEngine_Crutch.h"
#include "LayoutReader.h"
#include "../DinoHearts/DinoHearts.h"

#include <boost/di.hpp>
namespace di = boost::di;

Dino::Dino(QObject *parent) : QObject(parent)
{
}

Dino::~Dino()
{
}

void Dino::LoadDino()
{
	LoadDino_InitDrawEngine();
	LoadDino_layout();
	LoadDino_runer();
}

void Dino::LoadDino_InitDrawEngine() {

	auto temp = new dino::DrawEngine_Crutch();
	drawEngine = temp;
	cuApiInner = temp;
	drawEngine->pushMaper = [&](int id) {
		runer->PushButton(id);
	};
	drawEngine->me = me;

	//connect(drawEngineCrutch, &dino::DrawEngine_Crutch::sig_gui,
	//	[&](std::function<void(void)> fun) {emit sig_gui(fun); });
	drawEngine->sig_gui =
			[&](std::function<void(void)> fun) {
				emit sig_gui(fun);
			};

	drawEngine->sig_gui_ret =
			[&](std::function<QObject*(void)> fun)->QObject* {
				return emit sig_gui_ret(fun);
			};
}

void Dino::LoadDino_layout()
{
	dino::Draw::LayoutDrawer drawer(drawEngine);
	drawer.Load(dino_info.file_layaut);
}

void Dino::LoadDino_runer()
{
	if (!dino_info.type_script.isNull()) {
		auto test = QApplication::instance()->thread();
		auto test2 = QThread::currentThread();

		dApi = LoadDino_getDinoApi();

		std::shared_ptr<IRAndC> heart;
		auto path = dino_info.Path_folder + "/" + dino_info.name_title;
        auto hearts = new DinoHearts(path.toStdString());
		try {
			heart = hearts->GetInstance(dino_info.type_script.toStdString());
		}
		catch (std::exception &exp){
			qWarning() << exp.what();
		}

        if(heart == nullptr)
		{
			//delete dApi;
			qInfo() << "engine not loaded";
		}else{
	        heart->SetCallBack2(dApi->API);
            auto temp_test1 = heart->gettest();
            if (temp_test1 != "3")
            {
                qFatal("engine not work");
            }
	        heart->Start(dino_info.file_script.toStdString());
            runer = heart;
	        qInfo() << "engine success loaded and run";
        }
	}
}

std::shared_ptr<DinoApi> Dino::LoadDino_getDinoApi(){
	//auto cUApiInner = (ICUApiInner*)drawEngine;
	IUApiInner *temp = new IUApiInner();
	//temp->GetWidget = [](){drawEngine.GetWidget()}; //std::bind(&CoMediaPlayer::setMedia, this, std::placeholders::_1);

	//😭😭😭 i not so full undestent how it's work\use(so hard)., ok'ay try use fruit
	const auto injector = di::make_injector(
			di::bind<QString>.to(dino_info.Path_folder),
			di::bind<IMediaApp>.to<MediaApp>()//,
			//di::bind<ICUApiInner*>.to(cUApiInner) //i not understand how this use
	);
	//std::unique_ptr<example>
	//CoMediaPlayer *obj = injector.create<CoMediaPlayer*>();

	auto coMediaPlayer = injector.create<CoMediaPlayer*>();
	//coMediaPlayer->cUApiInner = cUApiInner;
	//coMediaPlayer->cUApiInner = std::bind(drawEngine, &DrawEngine_Crutch::, this, std::placeholders::_1);;
	coMediaPlayer->cuApiInner = cuApiInner;
	coMediaPlayer->draw_engine = drawEngine;

	auto pdApi = new DinoApi();
	pdApi->API->UAapi = drawEngine->getMe();
	pdApi->API->CoMediaPlayer = coMediaPlayer->getMe();
	pdApi->API->MediaApp = injector.create<IMediaApp*>();

	return std::shared_ptr<DinoApi>(pdApi);
}

void Dino::SetDinoLoaderInfo(const Dino_content_info &info) {
	dino_info = info;
}

void Dino::CloseDino()
{
}

void Dino::ChangedDino()
{
}

void Dino::UnChangedDino()
{
}

//void Dino::RunerSetTitle(QString str)
//{
//}


//std::function<void(const QString &name, QWidget *widget)> Dino::CallNewMuzzle;