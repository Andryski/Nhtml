#pragma once

#include <QObject>
#include <QString>
#include <QCoreApplication>
#include <QEvent>
#include <QSignalMapper>
#include <QtWidgets/QPushButton>
#include <QMap>

#include <functional>

#include "dino_global.h"
#include "Api/IRuner.h"
#include "DinoApi.h"
#include "Gui_draw_engine/IDraw_engine.h"
#include "Gui_draw_engine/DrawEngine_Crutch.h"
#include "../MasterDino/Dino_content_info.h"
using namespace DinoNames;

/// @author Andry
class DINO_EXPORT Dino : public QObject
{
	Q_OBJECT
public:
	Dino(QObject *parent = Q_NULLPTR);
	~Dino();
	void LoadDino();
	void LoadDino_runer();
	void LoadDino_layout();
	void CloseDino();

	//void SetDinoLoaderInfo(const QString &info);
	void SetDinoLoaderInfo(const Dino_content_info &info);

	void ChangedDino();
	void UnChangedDino();

	bool isRunerHas = false;

	QScopedPointer<IRuner> Point_runer;

	//IRuner *runer;
	std::shared_ptr<IRuner> runer;
	QString runer_name;

	QString dino_name;

	Dino_content_info dino_info;

	QObject *parent;
	QWidget *me;

	//void RunerSetTitle(QString);
	void RunerCallNotification(QString);


	std::shared_ptr<DinoApi> dApi;

	dino::IDraw_engine *drawEngine;
	ICUApiInner *cuApiInner;
	dino::DrawEngine_Crutch *drawEngineCrutch;


	std::function<void(const QString &name, QWidget *widget)> CallNewMuzzle = nullptr;

private:
	void LoadDino_InitDrawEngine();
	std::shared_ptr<DinoApi> LoadDino_getDinoApi();

signals:
	void sig_gui(std::function<void(void)>);
	QObject* sig_gui_ret(std::function<QObject*(void)>);
};
