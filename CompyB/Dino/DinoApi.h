#pragma once
#include "Api/IRAndC.h"
#include "Api/Sockets/WebSocketAPI.h"
#include "Api/Sockets/WebSocketFabricAPI.h"
#include "Api/IConsoleLog.h"
#include "ConsoleLog.h"
#include "Api/UAapi.h"
#include "Api/Media/CoMediaPlayer.h"
#include "Api/Network/XMLHttpRequest.h"

class DinoApi : public QObject
{
	Q_OBJECT
public:
	//DinoApi(IRAndC *Runer):RunerCalbecks(Runer)
	DinoApi(QObject *parent = Q_NULLPTR) : QObject(parent)
	{
		//API.reset(new IDinoAPI());
		API = new IDinoAPI();
		webSocetFabric = new WebSocketFabric(this);
		//API->WebSocketFabric = static_cast<IWebSocketFabric>(webSocetFabric);
		//API->WebSocketFabric = (IWebSocketFabric*)webSocetFabric;
		//IWebSocketFabric test1 = *webSocetFabric;
		API->WebSocketFabric = webSocetFabric->fabrik;
		//<IWebSocketFabric*>(webSocetFabric);
		//RunerCalbecks->DinoAPI = API;

		loger = new IConsoleLogS();
		loger->Debug = [](const std::string &mes){ConsoleLog::Debug(QString::fromStdString(mes));};
		loger->Info = [](const std::string &mes){ConsoleLog::Info(QString::fromStdString(mes));};
		loger->Warning = [](const std::string &mes){ConsoleLog::Warning(QString::fromStdString(mes));};
		loger->Critical = [](const std::string &mes){ConsoleLog::Critical(QString::fromStdString(mes));};
		loger->Fatal = [](const std::string &mes){ConsoleLog::Fatal(QString::fromStdString(mes));};

		API->ConsoleLog = loger;

//		VCoMediaPlayer.reset(new CoMediaPlayer());
		IVCoMediaPlayer.reset(VCoMediaPlayer->getMe());
		API->CoMediaPlayer = IVCoMediaPlayer.data();

		API->ICoXMLHttpRequestFabric = new XMLHttpRequestFabric();
		
	};
	~DinoApi() {
		delete API;
		delete webSocetFabric;
	};

	//IUAapiS *nui;
	UAapi *UI;
	IConsoleLogS *loger;
	//QScopedPointer<IDinoAPI> API;
	IDinoAPI *API;
	WebSocketFabric *webSocetFabric;
	IRAndC *RunerCalbecks;
	QScopedPointer<CoMediaPlayer> VCoMediaPlayer;
	QScopedPointer<ICoMediaPlayer> IVCoMediaPlayer;
};
