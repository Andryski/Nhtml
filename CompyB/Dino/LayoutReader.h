#pragma once
#include <QVector>

class LayoutReader
{
public:
	enum obj_type
	{
		Button,
		Field,
		Label,
		lay_h,
		lay_v,
		lay_t
	};
	struct obj
	{
		int id;
		QString systemName;
		QString text;
		obj_type type;
	};
	LayoutReader() {}
	LayoutReader(QString fileContent) { Read(fileContent); }
	~LayoutReader()
	{
	}
	/*
	void Read(QString fileContent)
	{
		QStringList sList = fileContent.split(QRegExp("[\r\n]"));
		for (QString element : sList)
		{
			QStringList lObj = element.split(":");
			obj temp1;
			temp1.id = lObj[0].toInt();
			temp1.type = static_cast<obj_type>(lObj[1].toInt());
			temp1.text = lObj[2];
			list.push_back(temp1);
		}
	}
	*/

	void Read(QString fileContent)
	{
		QStringList sList = fileContent.split(QRegExp("[\r\n]"));
		for (QString element : sList)
		{
			QStringList lObj = element.split(":");
			obj temp1;
			temp1.id = lObj[0].toInt();
			temp1.type = static_cast<obj_type>(lObj[1].toInt());
			temp1.text = lObj[2];
			list.push_back(temp1);
		}
	}

	QVector<obj> list;
private:
	
};

