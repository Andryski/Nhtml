//
// Created by Andry Z on 02.05.2018.
//

#ifndef COMPYB_CM_LAYOUTDRAWER_H
#define COMPYB_CM_LAYOUTDRAWER_H


#include <QString>
#include "ILayoutDrawer.h"
#include "IDraw_engine.h"

namespace dino::Draw {

class LayoutDrawer : public ILayoutDrawer {
public:
	LayoutDrawer(IDraw_engine *dEngine);
	~LayoutDrawer() override;
	void Load(const QString &layout) override;

private:
	IDraw_engine *DEngine;
};

}


#endif //COMPYB_CM_LAYOUTDRAWER_H
