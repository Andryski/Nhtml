﻿
#include <functional>
#include "DrawEngine_Crutch.h"
#include <QtWidgets>
#include "Elements_enum.h"
#include "../../MasterDino/MasterDino.h"

#ifdef _MSC_VER
#include <ciso646> // resolve problem with keywords or\and\not
#endif // _MSC_VER

namespace dino {
	DrawEngine_Crutch::DrawEngine_Crutch(QObject* parent)
		: QObject(parent)
	{
		signalMapper = new QSignalMapper(this);

		connect(signalMapper,
			static_cast<void(QSignalMapper::*)(int)>(&QSignalMapper::mapped),
			this, &DrawEngine_Crutch::MapClicked);
	}

	DrawEngine_Crutch::~DrawEngine_Crutch()
	{
	}

	//fixme: create enum for type
	/**
	* \brief
	* create new element in gui
	*/
	int DrawEngine_Crutch::CreteElement(int parent, int type)
	{
		unsigned int randi = GetFreeId();
		auto *obj = idMap[parent];
		if (obj != nullptr)
		{

			if (idMapTypeEnum.value(parent) == Elements_enum::HBoxLayout || idMapTypeEnum.value(parent) == Elements_enum::HBoxLayout)
			{
				CreteElementWithId(parent,type,randi);
				return randi;
			}else{
				qWarning() << "You cant create element not in layout";
			}
		}else{
			qWarning() << "element with id="<< parent << " not find";
		}
		return -1;
	}


	void DrawEngine_Crutch::getElementById()
	{
	}

	void DrawEngine_Crutch::setStyle(int id, QString style)
	{

		auto *obj = idMap[id];
		if (obj != nullptr)
		{
			auto *element = dynamic_cast<QWidget*>(obj);
			if(element) {
                auto lam = [&]() {
                    element->setStyleSheet(style);
                };
                emit sig_gui(lam);
            }else{
			    qInfo()<< "cant set Style to non Widget children";
			}
		}
	}

	void DrawEngine_Crutch::setText(int id, QString str)
	{
		auto *obj = idMap[id];
		if (obj != nullptr)
		{
			if (idMapTypeEnum[id] == Elements_enum::Button)
			{
				QPushButton *button = static_cast<QPushButton*>(obj);
				auto lam = [&]() {
					button->setText(str);
				};
				emit sig_gui(lam);
			}else
			if (idMapTypeEnum[id] == Elements_enum::LineEdit)
			{
				QLineEdit *lineE = static_cast<QLineEdit*>(obj);
				auto lam = [&]() {
					lineE->setText(str);
				};
				emit sig_gui(lam);
			}else
			if (idMapTypeEnum[id] == Elements_enum::Lable)
			{
				QLabel *label = static_cast<QLabel*>(obj);
				auto lam = [&]() {
					label->setText(str);
				};
				emit sig_gui(lam);
			}
		}
	}

	QString DrawEngine_Crutch::getText(int id)
	{
		auto *obj = idMap[id];
		if (obj != nullptr)
		{
			if (idMapTypeEnum[id] == Elements_enum::Button)
			{
				QPushButton *button = static_cast<QPushButton*>(obj);
				return button->text();
			}
			if (idMapTypeEnum[id] == Elements_enum::LineEdit)
			{
				QLineEdit *lineE = static_cast<QLineEdit*>(obj);
				return lineE->text();
			}
			if (idMapTypeEnum[id] == Elements_enum::Lable)
			{
				QLabel *label = static_cast<QLabel*>(obj);
				return label->text();
			}
		}
		return QString();
	}

	void DrawEngine_Crutch::MapClicked(int id)
	{
		pushMaper(id);
	}

	IUAapiS* DrawEngine_Crutch::getMe()
	{
		IUAapiS *temp = new IUAapiS;
		temp->GetText = [&](int id) -> std::string {
			return getText(id).toStdString(); };
		temp->SetText = [&](int id, std::string str) {
			setText(id, QString::fromStdString(str)); };
		temp->SetStyle = [&](int id, std::string str) {
			setStyle(id, QString::fromStdString(str)); };
		temp->CreteElement = [&](int parent, int type)->int {
			return CreteElement(parent, type); };
//		temp->CreteElementWithId = std::bind(&dino::DrawEngine_Crutch::CreteElementWithId,
//		                                     this, std::placeholders::_1,
//		                                     std::placeholders::_2, std::placeholders::_3);
		temp->CreteElementWithId = [this](int parent, int type, int id){ CreteElementWithId(parent,type,id); };
		temp->CreteMuzzle = [this](int parent, const std::string &name){ CreteMuzzle(parent, QString::fromStdString(name)); };
		return temp;
	}

    unsigned long DrawEngine_Crutch::GetFreeId() {
        return qrand() % 1000;
    }

	//std::function<void(const QString &name, QWidget *widget)> DrawEngine_Crutch::CallNewMuzzle;

	void DrawEngine_Crutch::CreteMuzzle(int parent, const QString &name)
	{
		auto *obj = (QWidget *)idMap[parent];
		if (obj != nullptr)
		{
			if (idMapTypeEnum[parent] == Elements_enum::Widget)
			{
				CallNewMuzzle(name, obj);
			}else{
				qWarning() << "You can't create Muzzle not in Widget";
			}
		}else{
			qWarning() << "element with id="<< parent << " not find";
		}
	}

	void DrawEngine_Crutch::CreteElementWithId(int parent, int type, int id) {
		CreteElementWithId(parent, Elements_enum(type), id);
	}

	void DrawEngine_Crutch::CreteElementWithId(int parent, Elements_enum type, int id) {
		//CreteElementWithId(parent,(int)type,id);
		//QWidget *element = nullptr;
		QObject *element = nullptr;
		QBoxLayout *layout = nullptr;
		switch(type){
			case Elements_enum::Lable:
				element = CreateQLabel(nullptr);
				break;
			case Elements_enum::Button:
				element = CreateQPushButton(nullptr);
				connect(element, SIGNAL(clicked()), signalMapper, SLOT(map()));
				signalMapper->setMapping(element, id);
				break;
			case Elements_enum::LineEdit:
				element = CreateQLineEdit(nullptr);
				break;
			case Elements_enum::HSlider:
				element = CreateQSlider(nullptr, Qt::Orientation::Horizontal);
				break;
			case Elements_enum::Widget:
				element = CreateQWidget(nullptr);
				break;
			case Elements_enum::VideoWidget:
			#ifndef __ANDROID__
				element = CreateQVideoWidget(nullptr);
			#else
				qWarning() << "VideoWidget not supportet on android";
				return;
			#endif
				break;

			case Elements_enum::VBoxLayout:
				layout = CreateQVBoxLayout(nullptr);
				break;
			case Elements_enum::HBoxLayout:
				layout = CreateQHBoxLayout(nullptr);
				break;
			default:
				qWarning() << "Uncnown Elements_enum";
				break;
		}

		auto parentType =idMapTypeEnum[parent];
		QWidget *parentWidget = nullptr;
		QBoxLayout *parentLayout = nullptr;

		auto parentTypeStr = QString::fromStdString(std::to_string((int)parentType));
		auto TypeStr = QString::fromStdString(std::to_string((int)type));

		if( parentType == Elements_enum::VBoxLayout or
		    parentType == Elements_enum::HBoxLayout or
		    parentType == Elements_enum::BoxLayout )
		{
			parentLayout = (QBoxLayout*)idMap[parent];
		}else if(parentType == Elements_enum::Widget){
			parentWidget = (QWidget*)idMap[parent];
		}else{
			qWarning() << "parentType != Widget or ?BoxLayout parentType=" + parentTypeStr + " Type=" + TypeStr;
		}

		if(parent == -1){
			parentType = Elements_enum::Widget;
			parentWidget = me;
		}

		if(parentType == Elements_enum::Widget){
			auto pLay = parentWidget->layout();
			if(pLay != nullptr){
				parentType = Elements_enum::BoxLayout;
				parentWidget = nullptr;
				parentLayout = (QBoxLayout *)pLay;
			}
		}

		idMapTypeEnum.insert(id, type);
		if(element != nullptr){
			if(parentLayout != nullptr){

				auto fun = [parentLayout, element]()->void {
					//parentLayout->addWidget(element);
					parentLayout->addWidget(static_cast<QWidget*>(element));
				};
				func_draw(fun);

				//parentLayout->addWidget(element); //up is fix for windows on new qt
			}else{
				qWarning() << "element == Widget but parentLayout = null(parent = Widget) how Widget can contain Widget hmmm, parentType=" + parentTypeStr+ " Type=" + TypeStr;
			};
			idMap.insert(id, element);
		}else if(layout != nullptr)
		{
			if(parentLayout != nullptr){

				auto fun = [parentLayout, layout]()->void {
					parentLayout->addLayout(layout);
				};
				func_draw(fun);

				//parentLayout->addLayout(layout); //up is fix for windows on new qt
			}else if(parentWidget != nullptr){
				
				auto fun = [parentWidget, layout]()->void {
					parentWidget->setLayout(layout);
				};
				func_draw(fun);

				//parentWidget->setLayout(layout); //up is fix for windows on new qt
			} else{
				qWarning() << "wtf";
			}
			idMap.insert(id, layout);
		}else{
			qWarning() << "wtf2";
		}
	}

	QWidget* DrawEngine_Crutch::GetWidget(int id) {
		if(idMap.contains(id)) {
			return (QWidget*)idMap[id];
		}
		qWarning() << "GetWidget with id=" + QString(id) + " not find";
		return nullptr;
	};
}
