#pragma once
#include <functional>
#include <QObject>
#include <QMap>
#include <QSignalMapper>
#include <QBoxLayout>

#include "IDraw_engine.h"
#include "DrawEngineBase.h"
#include "../Api/IUAapi.h"
#include "../Api/UI/IUApiInner.h"

/// @author Andry
namespace dino {

	class DrawEngine_Crutch : public QObject, public DrawEngineBase, public ICUApiInner
	{
		Q_OBJECT
	public:
		DrawEngine_Crutch(QObject *parent = Q_NULLPTR);
		~DrawEngine_Crutch() final;
		int CreteElement(int parent, int type) final;
		void getElementById() final;
		void setStyle(int id, QString style) final;
		void setText(int id, QString str) final;
		QString getText(int id) final;

		void MapClicked(int) final;

		IUAapiS* getMe() final;

		QMap<int, QObject*> idMap;
		QMap<int, Elements_enum> idMapTypeEnum;
		QSignalMapper *signalMapper;


		QWidget* GetWidget(int id) override;

		//todo
		//QWidget *me;

		//std::function<void(int)> pushMaper;

	//signals:
	//	void sig_gui(std::function<void(void)>);

       // std::function<void(std::function<void(void)>)> sig_gui ;

		std::function<void(const QString &name, QWidget *widget)> CallNewMuzzle = nullptr;

		void CreteMuzzle(int parent, const QString &name) override;
		void CreteElementWithId(int parent, int type, int id) override;
        void CreteElementWithId(int parent, Elements_enum type, int id) override;

        void createElement(std::string &name);

	private:
		unsigned long GetFreeId();
	};

}
