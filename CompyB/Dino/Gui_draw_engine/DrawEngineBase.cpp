#include "DrawEngineBase.h"
#include <QThread>
#include <QApplication>

#ifndef __ANDROID__
#include <QVideoWidget>
#endif

#include "../../../ThirdParty/qt-material-widgets/components/qtmaterialflatbutton.h"

namespace dino {
	bool DrawEngineBase::is_it_thread()
	{
		return QThread::currentThread() != QApplication::instance()->thread();
	}

	//void DrawEngineBase::func_draw(const std::function<QObject*()> &tc, const std::function<void(QWidget *)> &st)
	void DrawEngineBase::func_draw(const std::function<void()> &tc)
	{
		if (is_it_thread()) {
			sig_gui(tc);
		}
		else {
			//if (st == nullptr) {
				tc();
			//}
		}
	}

	//QObject * DrawEngineBase::func_draw_ret(const std::function<QObject*()> &tc, const std::function<void(QWidget *)> &st)
	QObject* DrawEngineBase::func_draw_ret(const std::function<QObject*()> &tc)
	{
		if (is_it_thread()) {
			return sig_gui_ret(tc);
		}
		else {
			//if (st == nullptr) {
				return tc();
			//}
		}
		return nullptr;
	}

	void func_draw(const std::function<QObject*()> &tc);
	QObject * func_draw_ret(const std::function<QObject*()> &tc);

	template <typename T>
	T* DrawEngineBase::CreateT(QWidget * parent) {
		if (parent != nullptr){
			auto fun = [parent]() -> T * {
				return new T(parent);
			};
			auto wid = (T *) func_draw_ret(fun);
			return wid;
		}else{
			auto wid = new T();
			return wid;
		}
	}

	template <typename T>
	unsigned long DrawEngineBase::CreateDynamycT(QBoxLayout *lay) {
		auto obj = CreateT<T>();
		auto randi =  qrand() % 1000;

		sig_gui([lay,obj]() {
			lay->addWidget(obj);
		});
		///emit sig_gui(lam);
		return randi;
	}

	QWidget* DrawEngineBase::CreateWidget(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QWidget(parent);
		};
		auto wid = (QWidget *)func_draw_ret(fun);
		return wid;
	}

	QPushButton* DrawEngineBase::CreateQPushButton(QWidget * parent) {
		auto fun = [parent]()->QObject* {
		    /*
            auto button = new QtMaterialFlatButton("I'm flat");
            button->setBackgroundMode(Qt::OpaqueMode);
            button->setFontSize(14);
            button->setRole(Material::Primary);
            return button;
            */
			return new QPushButton(parent);
		};
		auto wid = (QPushButton *)func_draw_ret(fun);
		return wid;
	}

	QLabel* DrawEngineBase::CreateQLabel(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QLabel(parent);
		};
		auto wid = (QLabel *)func_draw_ret(fun);
		return wid;
	}

	QLineEdit* DrawEngineBase::CreateQLineEdit(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QLineEdit(parent);
		};
		auto wid = (QLineEdit *)func_draw_ret(fun);
		return wid;
	}

	QSlider* DrawEngineBase::CreateQSlider(QWidget * parent, const Qt::Orientation &orentation) {
		auto fun = [parent, orentation]()->QSlider* {
			return new QSlider(orentation, parent);
		};
		auto wid = (QSlider *)func_draw_ret(fun);
		return wid;
	}

	QHBoxLayout* DrawEngineBase::CreateQHBoxLayout(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QHBoxLayout(parent);
		};
		auto wid = (QHBoxLayout *)func_draw_ret(fun);
		return wid;
	}

	QVBoxLayout* DrawEngineBase::CreateQVBoxLayout(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QVBoxLayout(parent);
		};
		auto wid = (QVBoxLayout *)func_draw_ret(fun);
		return wid;
	}

	QWidget* DrawEngineBase::CreateQWidget(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			return new QWidget(parent);
		};
		auto wid = (QWidget *)func_draw_ret(fun);
		return wid;
	}

	#ifndef __ANDROID__
	//QVideoWidget* DrawEngineBase::CreateQVideoWidget(QWidget * parent) {
	QObject* DrawEngineBase::CreateQVideoWidget(QWidget * parent) {
		auto fun = [parent]()->QObject* {
			auto vid = new QVideoWidget;//(parent);
			//vid->show();
			return vid;
		};
		//auto wid = (QVideoWidget *)func_draw_ret(fun);
		auto wid = func_draw_ret(fun);
		return wid;
	}
	#endif
}