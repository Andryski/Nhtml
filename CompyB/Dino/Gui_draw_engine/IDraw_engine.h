#pragma once
#include "../Api/IUAapi.h"
#include "Elements_enum.h"
#include <QObject>
#include <QString>
//#include <QWidget>
#include <functional>

namespace dino {

	/// @author Andry
	class IDraw_engine
	{
	public:
		virtual ~IDraw_engine() = default;
		virtual void getElementById() = 0;
        std::function<void(int)> pushMaper = 0;
		virtual void MapClicked(int id) = 0;
		QWidget *me;

	//signals:
	//	void sig_gui(std::function<void(void)>);

		std::function<void(std::function<void(void)>)> sig_gui = 0;
		std::function<QObject*(std::function<QObject*(void)>)> sig_gui_ret = 0;

		std::function<void(const QString &name, QWidget *widget)> CallNewMuzzle = 0;


		virtual IUAapiS* getMe() = 0;
		//**
		virtual int CreteElement(int parent, int type) = 0;
		virtual void setStyle(int id, QString style) = 0;
		virtual void setText(int id, QString str) = 0;
		virtual QString getText(int id) = 0;
		//****
		virtual void CreteMuzzle(int parent, const QString &name) = 0;
		virtual void CreteElementWithId(int parent, int type, int id) = 0;
		virtual void CreteElementWithId(int parent, Elements_enum type, int id) = 0;

	};

}
