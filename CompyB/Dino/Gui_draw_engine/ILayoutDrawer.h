//
// Created by Andry Z on 02.05.2018.
//

#ifndef COMPYB_CM_ILAYOUTDRAWER_H
#define COMPYB_CM_ILAYOUTDRAWER_H

namespace dino::Draw {

class ILayoutDrawer {
public:
	//ILayoutDrawer(IDraw_engine *dEngine) = 0;
	virtual ~ILayoutDrawer() = default;
	virtual void Load(const QString &layout) = 0;
};

}

#endif //COMPYB_CM_ILAYOUTDRAWER_H
