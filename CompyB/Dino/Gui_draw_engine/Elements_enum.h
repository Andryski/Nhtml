//
// Created by Andry Z on 05.04.2018.
//

#ifndef COMPYB_CM_ELEMENTS_ENUM_H
#define COMPYB_CM_ELEMENTS_ENUM_H


enum class Elements_enum :int {
	Button = 1,
	Lable,
	Picture,
	HSlider,
	VSlider, //5
	Widget,
	BoxLayout,
	VBoxLayout,
	HBoxLayout,
	LineEdit, //10
	VideoWidget,
	CheckBox
};


#endif //COMPYB_CM_ELEMENTS_ENUM_H
