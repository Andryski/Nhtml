//
// Created by Andry Z on 02.05.2018.
//

#include <QStack>
#include "LayoutDrawer.h"
#include "Elements_enum.h"

//on windows its not working((
//using namespace dino::Draw; 

namespace dino::Draw {
/**
	 * \brief
	 * load file gui
	 */
void LayoutDrawer::Load(const QString &layout)
{

	if (!layout.isNull())
	{

		QStringList sList = layout.split(QRegExp("[\r\n]"));

		QVector<int> test;
		QStack<int> tree;

		tree.push(-1);
		int now = -1;

		for (QString element : sList)
		{
			QStringList lObj = element.split(":");
			int elementID = lObj[0].toInt();
			if (lObj.length() == 2)
			{
				if (lObj[0] != "")
				{
					///DEngine->CreteElementWithId(now,Elements_enum::Widget,elementID);
//					QWidget *add = CreateWidget(now);
//					now->layout()->addWidget(add);
					tree.push(now);
					//					now = add;
					//					QBoxLayout *blay;
					if (lObj[1] == "V")
					{
						//						blay = CreateQVBoxLayout(add);
						DEngine->CreteElementWithId(now, Elements_enum::VBoxLayout, elementID);
					} else if (lObj[1] == "H")
					{
						DEngine->CreteElementWithId(now, Elements_enum::HBoxLayout, elementID);
						//						blay = CreateQHBoxLayout(add);
					}else if (lObj[1] == "VP")
					{
						DEngine->CreteElementWithId(now, Elements_enum::VideoWidget, elementID);
						//DEngine->setText(elementID, lObj[2]);

					}
					now = elementID;
				} else
				{
					//else
						{
						now = tree.pop();
					}
				}


			} else
			{
				if (lObj[1] == "B")
				{
					DEngine->CreteElementWithId(now, Elements_enum::Button, elementID);
					DEngine->setText(elementID, lObj[2]);
				} else if (lObj[1] == "L")
				{
					DEngine->CreteElementWithId(now, Elements_enum::Lable, elementID);
					DEngine->setText(elementID, lObj[2]);
				} else if (lObj[1] == "E")
				{
					DEngine->CreteElementWithId(now, Elements_enum::LineEdit, elementID);
					DEngine->setText(elementID, lObj[2]);

				} else if (lObj[1] == "SH")
				{
					DEngine->CreteElementWithId(now, Elements_enum::HSlider, elementID);
					if (lObj.count() == 3) {

					}
				}

			}
			//list.push_back(temp1);
			test.push_back(lObj.length());
		}
	}
}

LayoutDrawer::LayoutDrawer(IDraw_engine *dEngine) {//:ILayoutDrawer(dEngine) {
	DEngine = dEngine;
}

LayoutDrawer::~LayoutDrawer() {

}

}