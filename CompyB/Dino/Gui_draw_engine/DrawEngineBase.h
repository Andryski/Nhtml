#pragma once
#include <QObject>
#include <QWidget>
#include <functional>
#include <QPushButton>
#include <QLabel>
#include <QBoxLayout>
#include <QLineEdit>
#include <QSlider>
//#include <QVideoWidget>

#include "IDraw_engine.h"

namespace dino {
	class DrawEngineBase : public IDraw_engine
	{
	protected:
		bool is_it_thread();
		//void func_draw(const std::function<QObject*()> &tc, const std::function<void(QWidget *)> &st);
		//QObject * func_draw_ret(const std::function<QObject*()> &tc, const std::function<void(QWidget *)> &st);

		void func_draw(const std::function<void()> &tc);
		QObject * func_draw_ret(const std::function<QObject*()> &tc);

		template <typename T>
		T* CreateT(QWidget * parent = nullptr);

		template <typename T>
		unsigned long CreateDynamycT(QBoxLayout *lay);

		QWidget* CreateWidget(QWidget * parent);
		QPushButton* CreateQPushButton(QWidget * parent);
		QLabel* CreateQLabel(QWidget * parent);
		QLineEdit* CreateQLineEdit(QWidget * parent);
		QSlider* CreateQSlider(QWidget * parent, const Qt::Orientation &orentation);
		QHBoxLayout* CreateQHBoxLayout(QWidget * parent);
		QVBoxLayout* CreateQVBoxLayout(QWidget * parent);
		QWidget* CreateQWidget(QWidget * parent);
		//QVideoWidget* CreateQVideoWidget(QWidget * parent);

	#ifndef __ANDROID__
		QObject* CreateQVideoWidget(QWidget * parent);
	#endif
	};
}