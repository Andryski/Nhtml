
#if(APPLE)
#	set(CMAKE_PREFIX_PATH $ENV{HOME}/Qt/592/5.9.2/clang_64/)
#endif()

#find_package(Qt5Core)
#find_package(Qt5websockets)

set(CMAKE_AUTOMOC ON)

set(Dino_file
		${CMAKE_SOURCE_DIR}/Dino/Api/IUAapi.h
		${CMAKE_SOURCE_DIR}/Dino/Api/UAapi.h
		${CMAKE_SOURCE_DIR}/Dino/Gui_draw_engine/DrawEngine_Crutch.h
		${CMAKE_SOURCE_DIR}/Dino/Gui_draw_engine/DrawEngine_Crutch.cpp
		${CMAKE_SOURCE_DIR}/Dino/Gui_draw_engine/DrawEngineBase.h
		${CMAKE_SOURCE_DIR}/Dino/Gui_draw_engine/DrawEngineBase.cpp
		${CMAKE_SOURCE_DIR}/Dino/Gui_draw_engine/IDraw_engine.h
		${CMAKE_SOURCE_DIR}/Dino/ConsoleLog.h
		${CMAKE_SOURCE_DIR}/Dino/Dino.h
		${CMAKE_SOURCE_DIR}/Dino/Dino.cpp
		${CMAKE_SOURCE_DIR}/Dino/dino_global.h
		${CMAKE_SOURCE_DIR}/Dino/DinoApi.h
		${CMAKE_SOURCE_DIR}/Dino/Api/Sockets/IWebSocketAPI.h
		${CMAKE_SOURCE_DIR}/Dino/Api/Sockets/WebSocketAPI.cpp
		${CMAKE_SOURCE_DIR}/Dino/Api/Sockets/WebSocketAPI.h
		Api/Sockets/WebSocketFabricAPI.h)

set(Dino_file2
		Api/IConsoleLog.h
        Api/IDinoAPI.h
		Api/IRunerCallBack.h
		LayoutReader.cpp
		LayoutReader.h
		Api/IRAndC.h
		Api/IRuner.cpp
		Api/IRuner.h
		Api/Media/CoMediaPlayer.cpp
		Api/Media/CoMediaPlayer.h
		Api/Media/ICoMediaPlayer.h
		Gui_draw_engine/Elements_enum.h
		Gui_draw_engine/ILayoutDrawer.h
		Gui_draw_engine/LayoutDrawer.cpp
		Gui_draw_engine/LayoutDrawer.h
		Api/Media/IMediaFiles.h
		Api/Media/IMediaFile.cpp
		Api/Media/IMediaFile.h
		Api/UI/IUApiInner.h
		Api/Media/IMediaFiles.cpp
		Api/Media/MediaApp.cpp
		Api/Media/MediaApp.h
		Api/Network/IXMLHttpRequest.h
		Api/Network/XMLHttpRequest.cpp
		Api/Network/XMLHttpRequest.h)

#[[
set(material
		../../../ThirdParty/qt-material-widgets/components/qtmaterialflatbutton.h
		../../../ThirdParty/qt-material-widgets/components/qtmaterialflatbutton.cpp
		../../../ThirdParty/qt-material-widgets/components/qtmaterialflatbutton_internal.h
		../../../ThirdParty/qt-material-widgets/components/qtmaterialflatbutton_internal.cpp
		../../../ThirdParty/qt-material-widgets/components/lib/qtmaterialoverlaywidget.h
		../../../ThirdParty/qt-material-widgets/components/lib/qtmaterialoverlaywidget.cpp
		)
include_directories(./../../ThirdParty/qt-material-widgets/components/)
target_include_directories(test_material ./../../ThirdParty/qt-material-widgets/components/)
]]
add_subdirectory(../../ThirdParty subproject/qt-material_bin)

set(Dino_file3_mac
		Api/TouchBar/TouchBar.h
		Api/TouchBar/TouchBarItem.h)

add_definitions(-DLoad_Dinamic_Library_Def)


add_library(Dino_lib STATIC ${Dino_file2} ${Dino_file} ) #${material}


target_link_libraries(Dino_lib qt_material_widgets_lib)
if(APPLE)
	#add_library(Dino_lib STATIC ${Dino_file2} ${Dino_file} ${Dino_file3_mac} )
	##add_library(Dino_lib_mac_os STATIC ${Dino_file3_mac} )
	##qt5_use_modules(Dino_lib_mac_os Core)
	#set_property (TARGET Dino_lib_mac_os APPEND_STRING PROPERTY COMPILE_FLAGS "-fobjc-arc")
	##add_dependencies(Dino_lib Dino_lib_mac_os)

	#add_definitions("-x objective-c++") # as you already have
	#set(CMAKE_EXE_LINKER_FLAGS "-framework ServiceManagement  -framework AppKit -framework Foundation -framework Cocoa")
	##target_link_libraries(Dino_lib_mac_os stdc++ "-framework Foundation" "-framework Cocoa" "-framework AppKit" objc)
endif()

if(ANDROID)
	#qt5_use_modules(Dino_lib )#MultimediaWidgets
else()
	qt5_use_modules(Dino_lib MultimediaWidgets)#
endif()

if(APPLE)
	#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -framework AppKit -framework Foundation -w")
endif()

#add_compile_definitions(__ANDROID__1) //

#add_library(Dino_lib_dyn SHARED ${Dino_file})
#target_link_libraries(Dino_lib JSModule_lib)
target_link_libraries(Dino_lib Dino_Hearts_lib)
if(APPLE)
	#target_link_libraries(Dino_lib Dino_lib_mac_os)
endif()
qt5_use_modules(Dino_lib Core Network WebSockets Multimedia Widgets) #
#qt5_use_modules(Dino_lib MultimediaWidgets)
#qt5_use_modules(Dino_lib_dyn Core Widgets Websockets)