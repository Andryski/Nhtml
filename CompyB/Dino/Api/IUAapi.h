#pragma once
#include <string>
#include <functional>
#include <string>

class IUAapi
{
	virtual void SetStyle(int id, std::string style) = 0;
};

 struct IUAapiS
 {
	 std::function<void(int id, std::string style)> SetStyle = 0;
	 std::function<void(int id, std::string style)> SetText = 0;
	 std::function<std::string(int id)> GetText = 0;
	 std::function<int(int parent, int type)> CreteElement = 0;

	 std::function<void(int parent, const std::string &name)> CreteMuzzle = 0;
	 std::function<void(int parent, int type, int id)> CreteElementWithId = 0;


 };