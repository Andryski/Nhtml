//
// Created by Andry on 2019-04-19.
//
#pragma once
#include <string>

// fuck up windows:
//IXMLHttpRequest.h(7): error C2011: 'IXMLHttpRequest': 'struct' type redefinition
//c:\program files (x86)\windows kits\10\include\10.0.17763.0\um\msxml.h(8208): note: see declaration of 'IXMLHttpRequest'

class ICoXMLHttpRequest{
public:
    virtual ~ICoXMLHttpRequest() {};
    virtual void open(std::string method, std::string url, bool async) = 0;
    virtual void send(std::string data) = 0;
    virtual void send() = 0;
    virtual void setRequestHeader(std::string Name, std::string Value) = 0;
    int status;
    std::string statusText;
    std::string responseText;
};

class ICoXMLHttpRequestFabric{
public:
    virtual ICoXMLHttpRequest* Create() = 0;
    virtual ~ICoXMLHttpRequestFabric() {};
};