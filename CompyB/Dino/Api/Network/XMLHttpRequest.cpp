//
// Created by Andry on 2019-04-19.
//

#include "XMLHttpRequest.h"

#include <QEventLoop>

void XMLHttpRequest::open(std::string method, std::string url, bool async) {
    if(method == "GET"){
        Type = RequesType::Get;
    } else if(method == "POST") {
        Type = RequesType::Post;
    } else if(method == "DELETE") {
        Type = RequesType::Delet;
    }else if(method == "PATCH") {
        Type = RequesType::Patch;
    }

    url_address = url;

    isAsync = async;
}

void XMLHttpRequest::send(std::string data) {


}

void XMLHttpRequest::send() {
    QNetworkRequest request;
    QByteArray postDataByteArray;

    auto str = QString::fromStdString(url_address);
    request.setUrl(QUrl(str));

    if(isAsync){
        qWarning() << "Async - not implemented yet";
    }

    switch (Type){
        case RequesType::NotSet:{
            qWarning()<<"XMLHttpRequest type not set";
            Q_ASSERT(false);
            break;
        } case RequesType::Get: {
            reply = manager.get(request);
            break;
        } case RequesType::Post:{
            reply = manager.post(request, postDataByteArray);
            break;
        } case RequesType::Delet:{
            reply = manager.deleteResource(request);
            break;
        } case RequesType::Patch:{
            qWarning() << "not implemented yet";
            break;
        } default:
            qFatal("XMLHttpRequest WTF!1");
            Q_ASSERT(false);
    }

    QEventLoop waitLoop;
    QObject::connect(reply, &QNetworkReply::finished, &waitLoop, &QEventLoop::quit);
    waitLoop.exec();

    status = reply->error();
    statusText = reply->errorString().toStdString();
    responseText = reply->readAll().toStdString();
}
//https://habr.com/ru/post/314932/
//https://stackoverflow.com/questions/26180311/qhttp-in-not-available-in-qt5

void XMLHttpRequest::setRequestHeader(std::string Name, std::string Value) {

}
