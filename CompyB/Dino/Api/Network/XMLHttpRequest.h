//
// Created by Andry on 2019-04-19.
//
#pragma once

#include "IXMLHttpRequest.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>


enum class RequesType{
    NotSet,
    Get,
    Post,
    Delet,
    Patch
};

class XMLHttpRequest : public ICoXMLHttpRequest {
public:
    void open(std::string method, std::string url, bool async) override;

    void send(std::string data) override;
    void send() override;

    void setRequestHeader(std::string Name, std::string Value) override;


    bool isAsync;
    std::string url_address;
    RequesType Type;

private:
    QNetworkAccessManager manager;
    QNetworkReply *reply;
};

class XMLHttpRequestFabric : public ICoXMLHttpRequestFabric{
public:
    ICoXMLHttpRequest* Create() override{
        return new XMLHttpRequest();
    };
};