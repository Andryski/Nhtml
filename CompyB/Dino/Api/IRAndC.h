#pragma once
#include "IRuner.h"
#include "IRunerCallBack.h"
#include "IDinoAPI.h"

class IRAndC : public IRuner, public IRunerCallBack
{
public:
	virtual void Start() override = 0;
	virtual void Start(std::string) override = 0;
	virtual void Load() override = 0;
	virtual void Unload() override = 0;
	virtual void Translate() override = 0;
	virtual std::string gettest() override = 0;
	//virtual void SetCallBack() = 0;
	virtual void SetCallBack2(IDinoAPI *call) = 0;
	
	IRunerCallBack callBack;
    virtual ~IRAndC() {};
protected:
	IDinoAPI *DinoAPI;
};

