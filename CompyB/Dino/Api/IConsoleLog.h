﻿#pragma once
#include <string>
#include <functional>

/*class IConsoleLog {
	virtual void Debug(QString) = 0;
	virtual void Info(QString) = 0;
	virtual void Warning(QString) = 0;
	virtual void Critical(QString) = 0;
	virtual void Fatal(QString) = 0;
};*/


struct IConsoleLogS{
	std::function<void(std::string)> Debug = 0;
	std::function<void(std::string)> Info = 0;
	std::function<void(std::string)> Warning = 0;
	std::function<void(std::string)> Critical = 0;
	std::function<void(std::string)> Fatal = 0;
};