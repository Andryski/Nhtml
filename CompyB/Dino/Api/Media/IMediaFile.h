//
// Created by Andry on 15/11/2018.
//

#ifndef COMPYB_CM_IMEDIAFILE_H
#define COMPYB_CM_IMEDIAFILE_H

#include <string>
//#include <QUuid>

class IMediaFile {
public:
	std::string filePath;
};

class IMediaDescription{
public:
    //QUuid Guid;
    std::string Guid;
};

#endif //COMPYB_CM_IMEDIAFILE_H
