//
// Created by Andry on 2019-03-09.
//

#ifndef COMPYB_CM_MEDIAAPP_H
#define COMPYB_CM_MEDIAAPP_H

#include <QMap>
#include <QDirIterator>
#include <QtDebug>
#include <QUuid>

#include <boost/di.hpp>
namespace di = boost::di;

#include "IMediaFiles.h"

class MediaApp : public IMediaApp{
	QMap<QUuid, std::string> MediaMap;
	const QString &Path;
	std::string debug;
public:
	MediaApp(const QString &path): Path(path) {
		debug = Path.toStdString();
	};

	virtual ~MediaApp() {};

	void Refresh() override {
		MediaMap.clear();
		QDirIterator it(Path, QStringList() << "*", QDir::Files, QDirIterator::Subdirectories);
		while (it.hasNext()) {
			auto temp = it.next();
			temp  = temp.remove(Path + "/");
			qDebug() << temp;
			MediaMap.insert(QUuid::createUuid(), temp.toStdString());
		}
	}

	//QString GetPath(const IMediaDescription &description) override {
	//   if(!MediaMap.contains(description.Guid)){
	std::string GetPath(const std::string &uuid) override {
		if(!MediaMap.contains(QUuid(QString::fromStdString(uuid)))){
			return nullptr;
		}else{
			return Path.toStdString() + "/" + MediaMap[QUuid(QString::fromStdString(uuid))];
		}
	}

	std::string GetUuid(const std::string &name) override {
		if(MediaMap.values().contains(name)){
			return MediaMap.key(name).toString().toStdString();
		}
		return nullptr;
	}

	std::vector<std::string> GetListMediaFiles() override {
		//for (const auto &element: MediaMap)
		//  std::cout << element << ' ';
		return MediaMap.values().toVector().toStdVector();\
	};
};


#endif //COMPYB_CM_MEDIAAPP_H
