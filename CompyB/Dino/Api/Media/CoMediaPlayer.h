//
// Created by Andry Z on 06.11.2017.
//
#pragma once
#include <memory>
#include <QMediaPlayer>
#include "ICoMediaPlayer.h"
#include "IMediaFiles.h"
#include "../UI/IUApiInner.h"
#include "../../Gui_draw_engine/IDraw_engine.h"

//template <typename Shape>
class CoMediaPlayer {
    QMediaPlayer player;
////    QMediaPlaylist *playlist;
	QScopedPointer<IMediaApp> MediaAppController;
    std::shared_ptr<IMediaApp> MediaAppController_ptr;
    //IMediaApp *MediaAppController_ptr;
    //CoMediaPlayer(){};
public:
    //CoMediaPlayer() = delete;
    CoMediaPlayer(std::shared_ptr<IMediaApp> mediaAppController);
    ~CoMediaPlayer();
	void setMedia(const std::string &str_temp);
	void setMediaUrl(const QUrl url);
	void setMediaDescription (const IMediaDescription&);
	void setVolume(int volume);
    void play(const std::string &str_temp);
	void play();
    void pause();
    void stop();
    void setVideoOutput(int id);
	ICUApiInner* cuApiInner;
	dino::IDraw_engine* draw_engine;
	//IUApiInner* cUApiInner;
    ICoMediaPlayer* getMe();
};