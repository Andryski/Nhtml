//
// Created by Andry on 26/12/2018.
//
#include <gtest/gtest.h>

#include <QCoreApplication>

#include <vector>
using namespace std;

#include "../IMediaFiles.h"
#include "../CoMediaPlayer.h"
#include "../MediaApp.h"

#include <boost/di.hpp>
namespace di = boost::di;

template<class T>
bool IsContains(vector<T> ve, T item){
    return std::find(ve.begin(), ve.end(), item) != ve.end();
}

template<class T>
bool IsContains(vector<T> ve1, vector<T> ve2){
    for(auto &item: ve2){
        if(!IsContains<T>(ve1,item)){
            return false;
        }
    }
    return true;
}

static std::string testPath;


void common_test(IMediaApp *obj){
    obj->Refresh();

    auto list = obj->GetListMediaFiles();
    vector<string> test_list;
    for (auto item: list){
        test_list.push_back(item);
    }

    vector<string> etalon{
            "CTestTestfile.cmake",
    };
    EXPECT_TRUE(IsContains<string>(test_list, etalon));

    auto guid = obj->GetUuid("CTestTestfile.cmake");
    auto tem = guid;
    auto pathFile = obj->GetPath(guid);

    auto debug1 = pathFile;

    EXPECT_TRUE(debug1 == testPath + "/CTestTestfile.cmake");
}

TEST(DinoApi, MediaApp) {
    int arg = 0;
    QCoreApplication coreApplication(arg, nullptr);
    auto path = QCoreApplication::applicationDirPath();
    auto debug = path.toStdString();
    testPath = debug;

    IMediaApp *mApp = new MediaApp(path);

    common_test(mApp);
}

TEST(DinoApi, IMediaApp_DI){
    int arg = 0;
    QCoreApplication coreApplication(arg, nullptr);
    auto path = QCoreApplication::applicationDirPath();
    auto debug = path.toStdString();
    testPath = debug;

    const auto injector = di::make_injector(
            di::bind<QString>.to(path),
            di::bind<IMediaApp>.to<MediaApp>()
    );
    //std::unique_ptr<example>
    CoMediaPlayer *obj = injector.create<CoMediaPlayer*>();

    //common_test((IMediaApp*)obj);
}
