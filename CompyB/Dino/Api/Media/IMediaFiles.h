//
// Created by Andry Z on 03/09/2018.
//

#pragma once

#include <functional>
#include <string>

#include "IMediaFile.h"


struct IMediaFiles {
	//std::function<QArray<QString>()> GetMediaFiles = 0;

};

class IMediaApp{
public:
    // IRuner() {};
    virtual ~IMediaApp() {};
    //virtual QString GetPath(const IMediaDescription&) = 0;

    //virtual QUuid GetUuid(const std::string &) = 0;
    virtual std::string GetUuid(const std::string &) = 0;
    //virtual std::string GetPath(const QUuid&) = 0;
    virtual std::string GetPath(const std::string&) = 0;
    virtual std::vector<std::string> GetListMediaFiles() = 0;
    virtual void Refresh() = 0;
};
