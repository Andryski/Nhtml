//
// Created by Andry Z on 06.11.2017.
//

#ifndef COMPYB_CM_ICOMEDIAPLAYER_H
#define COMPYB_CM_ICOMEDIAPLAYER_H

#include <functional>
#include <string>
//#include <QUrl>
#include "IMediaFile.h"

struct ICoMediaPlayer
{
	std::function<void(const std::string&)> setMedia = 0;
	std::function<void(const IMediaDescription)> setMediaDescription = 0;
    std::function<void()> Pause = 0;
    std::function<void()> Stop = 0;
	std::function<void(const std::string&)> PalyName = 0;
	std::function<void()> Paly = 0;
	//std::function<void(const QUrl url)> setMediaUrl = 0;
	std::function<void(const std::string url)> setMediaUrl = 0;
	std::function<void(int)> setVolume = 0;
	std::function<void(int)> setVideoOutput = 0;
};

#endif //COMPYB_CM_ICOMEDIAPLAYER_H
