//
// Created by Andry Z on 06.11.2017.
//

#include "CoMediaPlayer.h"
#include <functional>
#include <QDir>

CoMediaPlayer::CoMediaPlayer(std::shared_ptr<IMediaApp> mediaAppController) {

   // player = new QMediaPlayer();
    //MediaAppController(mediaAppController);
    MediaAppController_ptr = mediaAppController;
}

CoMediaPlayer::~CoMediaPlayer() {

}

void CoMediaPlayer::setVolume(int volume){
    player.setVolume(volume);
}

void CoMediaPlayer::setMedia(const std::string &str_temp2) {
	auto str_temp = QString::fromStdString(str_temp2);
	auto temp1 = QUrl::fromLocalFile(str_temp);
	auto temp2 = QUrl(str_temp);
	auto temp3 = QUrl::fromUserInput(str_temp);

	auto temp11 = temp1.isLocalFile();
	auto temp22 = temp2.isLocalFile();
	auto temp33 = temp3.isLocalFile();

	auto temp111 = temp1.isValid();
	auto temp222 = temp2.isValid();
	auto temp333 = temp3.isValid();

	auto temp1111 = temp1.toString().toStdString();
	auto temp11112 = temp2.toString().toStdString();
	auto temp11113 = temp3.toString().toStdString();

	auto temp111111 = temp1.path().toStdString();
	auto temp111122 = temp2.path().toStdString();
	auto temp111133 = temp3.path().toStdString();

	auto abpath1 = QDir::isAbsolutePath(temp1.path());
	auto abpath2 = QDir::isAbsolutePath(temp2.path());
	auto abpath3 = QDir::isAbsolutePath(temp3.path());


    player.setMedia(temp3);
    //player.setMedia(QUrl(str_temp));
    //player.setMedia(QUrl::fromLocalFile(str_temp));
}

void CoMediaPlayer::setMediaUrl(const QUrl url) {
    player.setMedia(url);
}

void CoMediaPlayer::setMediaDescription(const IMediaDescription &description) {
//    auto path = MediaAppController_ptr->GetPath(description);
  //  play(path);
}

void CoMediaPlayer::play(const std::string &str_temp) {
    //playlist = new QMediaPlaylist(player);
    //playlist->addMedia(QUrl(str_temp));
    //player.setMedia(QUrl::fromLocalFile(str_temp));
    //player.setMedia(QUrl(str_temp));
    setMedia(str_temp);
    player.play();
}

void CoMediaPlayer::play() {
    player.play();
}

void CoMediaPlayer::stop() {
    player.stop();
}

void CoMediaPlayer::pause() {
    player.pause();
}

ICoMediaPlayer* CoMediaPlayer::getMe() {
    ICoMediaPlayer *temp = new ICoMediaPlayer();
    temp->setMedia = std::bind(&CoMediaPlayer::setMedia, this, std::placeholders::_1);
    //temp->PalyName = std::bind(&CoMediaPlayer::play, this, std::placeholders::_1);
    temp->PalyName = [&](const std::string str_temp){play(str_temp);};
    temp->Paly = [&](){play();};
	//temp->setVolume = &CoMediaPlayer::setVolume; //std::bind(&CoMediaPlayer::setVolume,this, std::placeholders::_1);
	temp->setVolume = std::bind(&CoMediaPlayer::setVolume, this, std::placeholders::_1);
    temp->Pause = [&](){pause();};
    temp->Stop = [&](){stop();};
    temp->setVideoOutput = std::bind(&CoMediaPlayer::setVideoOutput, this, std::placeholders::_1);
    return temp;
}

void CoMediaPlayer::setVideoOutput(int id) {
    //auto videoWidget = cUApiInner->GetWidget(id);

#ifndef __ANDROID__
    auto videoWidget = cuApiInner->GetWidget(id);
    auto fun = [ this, videoWidget]()->void {
        player.setVideoOutput((QVideoWidget*)videoWidget);
    };
    draw_engine->sig_gui(fun);
#else
	qWarning() << "VideoWidget not supportet on android";
	return;
#endif
}
