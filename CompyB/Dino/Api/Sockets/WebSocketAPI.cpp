﻿#include "WebSocketAPI.h"


#include <QtCore/QDebug>

WebSocket::WebSocket(const QUrl &url, bool debug, QObject *parent) :
	QObject(parent),
	m_url(url),
	m_debug(debug)
{
	callWebSocket = new IWebSocket;

	if (m_debug)
		qDebug() << "WebSocket server:" << url;
	connect(&m_webSocket, &QWebSocket::connected, this, &WebSocket::onConnected);
	connect(&m_webSocket, &QWebSocket::disconnected, this, &WebSocket::closed);
	connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &WebSocket::onTextMessageReceived);
	//callWebSocket.open = [&](const QString &messege) {open(messege); };
	callWebSocket->open = [&](const std::string &url1) {
		auto temp = QString::fromStdString(url1);
		open(temp);
	};
	callWebSocket->onclose = [&](const std::string &messege) {close(); };
	callWebSocket->send = [&](const std::string &messege) {sendText(QString::fromStdString(messege)); };
	callWebSocket->close = [&]() {close(); };
	QString test1 = url.toString();
	std::string	test2 = test1.toStdString();
	m_webSocket.open(url);
	auto test3 = m_webSocket.isValid();
	auto test4 = m_webSocket.state();
	auto test5 = m_webSocket.errorString();
}

WebSocket::WebSocket(bool debug, QObject *parent) :
	QObject(parent),
	m_debug(debug)
{
	callWebSocket = new IWebSocket;
	connect(&m_webSocket, &QWebSocket::connected, this, &WebSocket::onConnected);
	connect(&m_webSocket, &QWebSocket::disconnected, this, &WebSocket::closed);
	connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &WebSocket::onTextMessageReceived);
	//callWebSocket.open = [&](const QString &messege) {open(messege); };
	callWebSocket->open = [&](const std::string &url1) {
		auto temp = QString::fromStdString(url1);
		open(temp);
	};
	callWebSocket->onclose = [&](const std::string &messege) {close(); };
	callWebSocket->send = [&](const std::string &messege) {sendText(QString::fromStdString(messege)); };
	callWebSocket->close = [&]() {close(); };
}

void WebSocket::open(QString &url)
{
	qWarning() << "WebSocket::open now not work(( ";
	//i don't no wuy(

	auto t1 = QUrl(url);
	auto t2 = t1.toString();
	m_webSocket.open(QUrl(url));
	auto test = m_webSocket.isValid();
	//m_webSocket.open(QUrl(m_url));
}

void WebSocket::onConnected()
{
	if (m_debug)
		qDebug() << "WebSocket connected";
	callWebSocket->onopen();
}

void WebSocket::sendText(QString message)
{
	if (m_debug)
		qDebug() << "Message send:" << message;
	m_webSocket.sendTextMessage(message);
}

void WebSocket::onTextMessageReceived(QString message)
{
	if (m_debug)
		qDebug() << "Message received:" << message;
	callWebSocket->onmessage(message.toStdString());
}

void WebSocket::close()
{
	m_webSocket.close();
	callWebSocket->onclose(QString().toStdString());
}
