﻿#pragma once
#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <functional>
#include "IWebSocketAPI.h"

//namespace WebSocketAPI
//{

	class WebSocket : public QObject
	{
		Q_OBJECT
	public:
		//explicit
		WebSocket(const QUrl &url, bool debug = false, QObject *parent = Q_NULLPTR);
		//explicit
		WebSocket(bool debug = false, QObject *parent = Q_NULLPTR);

		void open(QString &url);
		void close();
		void sendText(QString message);

		IWebSocket *callWebSocket;

	Q_SIGNALS:
		void closed();

	private Q_SLOTS:
		void onConnected();
		void onTextMessageReceived(QString message);

	private:
		QWebSocket m_webSocket;
		QUrl m_url;
		bool m_debug;
	};
//}


