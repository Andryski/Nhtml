﻿#pragma once
#include <functional>
#include <string>

struct IWebSocket
{
	std::function<void(std::string)> open = 0;
	std::function<void()> onopen = 0;
	std::function<void(std::string)> onclose = 0;
	std::function<void(std::string)> onmessage = 0;
	std::function<void(std::string)> onerror = 0;
	std::function<void(std::string)> send = 0;
	std::function<void()> close = 0;
};


struct IWebSocketFabric
{
	std::function<IWebSocket*()> Create = 0;
	std::function<IWebSocket*(const std::string&)> CreateParam = 0;
};