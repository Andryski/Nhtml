//
// Created by Andry Z on 15/10/2017.
//

#ifndef HELLO_CMAKE_WEBSOCKETFABRICAPI_H
#define HELLO_CMAKE_WEBSOCKETFABRICAPI_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <functional>
#include "IWebSocketAPI.h"
#include "WebSocketAPI.h"

//todo mayby :IWebSocketFabric
//class WebSocketFabric :public IWebSocketFabric //public QObject, IWebSocketFabric
class WebSocketFabric: public QObject
{
    Q_OBJECT
public:
    WebSocketFabric(QObject *parent = Q_NULLPTR) : QObject(parent)
    {
        fabrik = new IWebSocketFabric();
        fabrik->Create = [&]()->IWebSocket* { return Create(); };
        fabrik->CreateParam = [&](const std::string &url)->IWebSocket* {
            auto temp = QString::fromStdString(url);
            return CreateParam(temp);
        };
    };

    IWebSocket* Create()
    {
        auto socket = new WebSocket(true, this);
        listWebSocet.append(socket);
        return socket->callWebSocket;
    }

    IWebSocket* CreateParam(const QString &url)
    {
        auto socet = new WebSocket(QUrl(url), true, this);
        listWebSocet.append(socet);
        return socet->callWebSocket;
    }

    IWebSocketFabric *fabrik;
    QVector<WebSocket*> listWebSocet;
    bool m_debug;
};
#endif //HELLO_CMAKE_WEBSOCKETFABRICAPI_H
