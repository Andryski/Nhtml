//
// Created by Andry on 2019-02-10.
//
#pragma once
#include <QObject>

/*
 * https://bugreports.qt.io/browse/QTBUG-56908
 * https://bugreports.qt.io/browse/QTBUG-56908Touch
 * https://github.com/andeplane/qt-touchbar
 *
 *
 * https://github.com/electron/electron/blob/edf063bae37e7f8a799bafa2e8c9cfed7ba8226e/lib/browser/api/touch-bar.js
 * https://github.com/electron/electron/blob/392458b2528fb7ab6592a47bf535419564ba74bc/lib/browser/api/browser-window.js
 * https://github.com/electron/electron/blob/6f91af93433df4e9c0e7fb592e5b2dcb0b1c7888/atom/browser/ui/cocoa/atom_touch_bar.h
 * https://github.com/electron/electron/blob/6f91af93433df4e9c0e7fb592e5b2dcb0b1c7888/atom/browser/ui/cocoa/atom_touch_bar.mm
 * https://github.com/electron/electron/blob/27bd47a3336ea176bb3f092fc428d7f1758e782f/atom/browser/native_window.cc
 * https://github.com/electron/electron/tree/6f91af93433df4e9c0e7fb592e5b2dcb0b1c7888/atom/browser/ui/cocoa
 */

class TouchBar : public QObject
{
	Q_OBJECT
public:
	//explicit TouchBar(QQuickItem *parent = nullptr);
	explicit TouchBar(QObject *parent = nullptr);
	signals:

public slots:

private:
	void* m_tbProvider;
	// QQuickItem interface
protected:
	//virtual void itemChange(ItemChange, const ItemChangeData &) override;
};
