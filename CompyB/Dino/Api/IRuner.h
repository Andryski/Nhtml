#pragma once

#include <string>

class IRuner
{
public:
	// IRuner() {};
    virtual ~IRuner() {};
    virtual void Start() = 0;
    virtual void Start(std::string) = 0;
    virtual void Load() = 0;
    virtual void Unload() = 0 ;
    virtual void Translate() = 0;
	virtual std::string gettest() = 0; // debug
	virtual void PushButton(int id) = 0;
};

