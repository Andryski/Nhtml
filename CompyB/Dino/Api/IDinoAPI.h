﻿#pragma once
#include "Sockets/IWebSocketAPI.h"
#include "IConsoleLog.h"
#include "IUAapi.h"
#include "Media/ICoMediaPlayer.h"
#include "Media/IMediaFiles.h"
#include "Network/IXMLHttpRequest.h"

struct IDinoAPI
{
	IWebSocketFabric *WebSocketFabric;
	IConsoleLogS *ConsoleLog;
	IUAapiS *UAapi;
	ICoMediaPlayer *CoMediaPlayer;
	IMediaApp *MediaApp;
    ICoXMLHttpRequestFabric *ICoXMLHttpRequestFabric;
};
