//
// Created by Andry on 2019-01-16.
//
#pragma once
//#include <QWidget>


struct IUApiInner
{
	//std::function<QWidget*(int id)> GetWidget = 0;
	std::function<QObject*(int id)> GetWidget = 0;
};

class ICUApiInner
{
public:
	virtual QWidget* GetWidget(int id) = 0;
};