#pragma once

#include <functional>
#include <string>

class IRunerCallBack
{
public:
	// IRunerCallBack() = 0;
    virtual ~IRunerCallBack(){};
	std::function<void(std::string)> setTitle = 0;
	std::function<void(std::string)> notification = 0;
	std::function<void(std::string, std::string)> dinoSet = 0;
	std::function<void(int, std::string)> setObjectText = 0;
};

struct ISRunerCallBack
{
	std::function<void(std::string)> setTitle = 0;
	std::function<void(std::string)> notification = 0;
	std::function<void(std::string, std::string)> dinoSet = 0;
	std::function<void(int, std::string)> setObjectText = 0;
	std::function<void(int, std::string)> setObjectStyleSheet = 0;
};

