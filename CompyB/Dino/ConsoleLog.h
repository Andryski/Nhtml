
#include <QString>
#include <QDebug>

#include "Api/IConsoleLog.h"

/// @brief class for logging in runner(js) api
class ConsoleLog// : public IConsoleLog
{
public:
	ConsoleLog(){};
	~ConsoleLog(){};
	static void Debug(QString messege)
	{
		qDebug() << messege;
	}
	static void Info(QString messege)
	{
		qInfo() << messege;
	}
	static void Warning(QString messege)
	{
		qWarning() << messege;
	}
	static void Critical(QString messege)
	{
		qCritical() << messege;
	}
	static void Fatal(QString messege)
	{
		qFatal("%s",messege.toStdString().data());
	}
};
