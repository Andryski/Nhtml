﻿#pragma once
//#include <QJSEngine>
#include "api/Network/JSXMLHttpRequest.h"
#include "api/JSWebSocket.h"
#include "../Dino/Api/IDinoAPI.h"
#include "api/JSConsoleLog.h"
#include "api/JSUAapi.h"
#include "api/Media/JsCoMediaPlayer.h"
#include "api/Media/JsMediaApp.h"

class JSMainApi
{
public:
	JSMainApi(QJSEngine* engineP, IDinoAPI *jsdinoAPIP) 
	: jsdinoAPI(jsdinoAPIP), engine(engineP)
	{
		loger = jsdinoAPI->ConsoleLog;
		auto jsconsol = new JSConsoleLog(loger);
		QJSValue valueJSConsoleLog = engine->newQObject(jsconsol);
		engine->globalObject().setProperty("Console", valueJSConsoleLog);
		engine->globalObject().setProperty("console", valueJSConsoleLog);

		UAapi = jsdinoAPI->UAapi;
		auto jsUAapi = new JSUAapi(UAapi);
		QJSValue valuejsUAapi = engine->newQObject(jsUAapi);
		engine->globalObject().setProperty("document", valuejsUAapi);

        jsWebSocketFabric = new JSWebSocketFactory(engine, jsdinoAPI->WebSocketFabric);
        jsWebSocketFabric->ConfigJsEngine(engine);

        player = new JsCoMediaPlayer(jsdinoAPI->CoMediaPlayer);
		player->ConfigJsEngine(engine);

		mediaApp = new JsMediaApp(jsdinoAPI->MediaApp);
		mediaApp->ConfigJsEngine(engine);

        jsXMLHttpRequestFabric = new JSXMLHttpRequestFabric(jsdinoAPI->ICoXMLHttpRequestFabric);
        jsXMLHttpRequestFabric->ConfigJsEngine(engine);
	};

	~JSMainApi() {
	};

	IConsoleLogS *loger;
	JSWebSocketFactory *jsWebSocketFabric;
	IDinoAPI *jsdinoAPI;
	QJSEngine* engine;
	IUAapiS *UAapi;

	JsCoMediaPlayer *player;
	JsMediaApp *mediaApp;
    JSXMLHttpRequestFabric *jsXMLHttpRequestFabric;
};
