//
// Created by Andry on 2019-04-20.
//
#pragma once

#include <QObject>
#include <QJSEngine>

#include "../../../Dino/Api/Network/IXMLHttpRequest.h"

class JSXMLHttpRequest : public QObject {
Q_OBJECT
    ICoXMLHttpRequest *XMLHttpRequest;
public:
    JSXMLHttpRequest(ICoXMLHttpRequest *xmlHttpRequest);

    void ConfigJsEngine(QJSEngine *engine);

    Q_INVOKABLE void open(QString method, QString url, bool async);
    Q_INVOKABLE void send();


    Q_PROPERTY(QJSValue status READ getStatus)
    QJSValue getStatus() {
        return XMLHttpRequest->status;
    }

    Q_PROPERTY(QJSValue responseText READ getResponseText)
    QJSValue getResponseText() {
     return QString::fromStdString(XMLHttpRequest->responseText);
    }
};

class JSXMLHttpRequestFabric : public QObject {
Q_OBJECT
    ICoXMLHttpRequestFabric *XMLHttpRequestFabric;
    QJSEngine *jsEngine;
public:
    JSXMLHttpRequestFabric(ICoXMLHttpRequestFabric *xMLHttpRequestFabricjsXMLHttpRequestFabric);
    Q_INVOKABLE QJSValue Create();
    void ConfigJsEngine(QJSEngine *engine);
};
