//
// Created by Andry on 2019-04-20.
//

#include "JSXMLHttpRequest.h"

JSXMLHttpRequest::JSXMLHttpRequest(ICoXMLHttpRequest *xmlHttpRequest) :
        XMLHttpRequest(xmlHttpRequest) {}

void JSXMLHttpRequest::open(QString method, QString url, bool async) {
    XMLHttpRequest->open(method.toStdString(), url.toStdString(), async);
}

void JSXMLHttpRequest::send() {
    XMLHttpRequest->send();
}

void JSXMLHttpRequest::ConfigJsEngine(QJSEngine *engine) {
    //QJSValue valueJsObj = engine->newQObject(this);
    //engine->globalObject().setProperty("Dino_media2", valueJsObj);
}


JSXMLHttpRequestFabric::JSXMLHttpRequestFabric(ICoXMLHttpRequestFabric *xMLHttpRequestFabric) :
        XMLHttpRequestFabric(xMLHttpRequestFabric) {}

QJSValue JSXMLHttpRequestFabric::Create() {
    auto instance = XMLHttpRequestFabric->Create();
    auto jsInstance = new JSXMLHttpRequest(instance);
    auto ret = jsEngine->newQObject(jsInstance);
    return ret;
}

void JSXMLHttpRequestFabric::ConfigJsEngine(QJSEngine *engine) {
    jsEngine = engine;
    QJSValue valueJsObj = engine->newQObject(this);
    engine->globalObject().setProperty("Dino_network_XMLHttpRequestFabric", valueJsObj);
    engine->evaluate(
            "function XMLHttpRequest() {"
            "    return Dino_network_XMLHttpRequestFabric.Create()"
            "}");
}

