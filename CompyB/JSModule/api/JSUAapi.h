#pragma once
#include <QJSEngine>

#include "../../Dino/Api/UAapi.h"

class JSUAapi : public QObject
{
	Q_OBJECT
public:
	JSUAapi(IUAapiS *UAapi, QObject *parent = Q_NULLPTR): QObject(parent), caller(UAapi)
	{	
	}
	~JSUAapi(){};
	Q_INVOKABLE void SetStyle(int id, QString style)
	{
		caller->SetStyle(id, style.toStdString());
	};
	Q_INVOKABLE void SetText(int id, QString style)
	{
		caller->SetText(id, style.toStdString());
	};
	Q_INVOKABLE QJSValue GetText(int id)
	{
		auto ret = QString::fromStdString(caller->GetText(id));
		return QJSValue(ret);
	};
	Q_INVOKABLE QJSValue CreteElement(int parent, int type)
	{
		return caller->CreteElement(parent, type);
	}
	Q_INVOKABLE void CreteMuzzle(int parent, QString name)
	{
		caller->CreteMuzzle(parent, name.toStdString());
	}
	Q_INVOKABLE void CreteElementWithId(int parent, int type, int id)
	{
		caller->CreteElementWithId(parent, type, id);
	}

	IUAapiS *caller;
};
