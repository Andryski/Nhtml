//
// Created by Andry on 31/12/2018.
//

#ifndef COMPYB_CM_JSMEDIAAPP_H
#define COMPYB_CM_JSMEDIAAPP_H

#include <QObject>
#include <QJSEngine>

#include "../../../Dino/Api/Media/IMediaFiles.h"

class JsMediaApp : public QObject
{
Q_OBJECT
    IMediaApp *MediaApp;
public:
    JsMediaApp(IMediaApp *MediaApp);
    ~JsMediaApp();
    void ConfigJsEngine(QJSEngine *engine);

    Q_INVOKABLE void Refresh();
   // Q_INVOKABLE QUuid GetUuid(const std::string &);
    Q_INVOKABLE QString GetUuid(const QString &);
    Q_INVOKABLE QString GetPath(const QString&); //delete later
    Q_INVOKABLE QList<QString> GetListMediaFiles();
};


#endif //COMPYB_CM_JSMEDIAAPP_H
