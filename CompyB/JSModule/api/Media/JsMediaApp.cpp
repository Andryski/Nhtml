//
// Created by Andry on 31/12/2018.
//

#include "JsMediaApp.h"

JsMediaApp::JsMediaApp(IMediaApp *MediaApp) :
    MediaApp(MediaApp)
{
}

JsMediaApp::~JsMediaApp() {

}

void JsMediaApp::ConfigJsEngine(QJSEngine *engine) {
    QJSValue valueJsObj = engine->newQObject(this);
    engine->globalObject().setProperty("Dino_media2", valueJsObj);
}

void JsMediaApp::Refresh() {
    MediaApp->Refresh();
}

QString JsMediaApp::GetUuid(const QString &name) {
    auto temp = name;
    return QString::fromStdString(MediaApp->GetUuid(name.toStdString()));
}

QString JsMediaApp::GetPath(const QString &uuid) {
    return QString::fromStdString(MediaApp->GetPath(uuid.toStdString()));
}

QList<QString> JsMediaApp::GetListMediaFiles() {
    auto list = MediaApp->GetListMediaFiles();
    QList<QString> test_list;
    for (auto item: list){
        test_list.push_back(QString::fromStdString(item));
    }
    return test_list;
}
