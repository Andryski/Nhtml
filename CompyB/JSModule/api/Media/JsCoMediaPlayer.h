//
// Created by Andry Z on 06.11.2017.
//

#ifndef COMPYB_CM_JSCOMEDIAPLAYER_H
#define COMPYB_CM_JSCOMEDIAPLAYER_H

#include <QJSEngine>

#include "../../../Dino/Api/Media/ICoMediaPlayer.h"

class JsCoMediaPlayer : public QObject
{
Q_OBJECT
    ICoMediaPlayer *pplayer;
public:
    JsCoMediaPlayer(ICoMediaPlayer  *player);
    ~JsCoMediaPlayer();
    void ConfigJsEngine(QJSEngine *engine);

    Q_INVOKABLE void Stop();
    Q_INVOKABLE void Pause();
    Q_INVOKABLE void Play(QString str);
	Q_INVOKABLE void Play();
	Q_INVOKABLE void setVolume(int volume);
	Q_INVOKABLE void setMedia(const QString& path);
	Q_INVOKABLE void setVideoOutput(int id);
};


#endif //COMPYB_CM_JSCOMEDIAPLAYER_H
