//
// Created by Andry Z on 06.11.2017.
//

#include "JsCoMediaPlayer.h"

JsCoMediaPlayer::JsCoMediaPlayer(ICoMediaPlayer *player){
    pplayer = player;
}

JsCoMediaPlayer::~JsCoMediaPlayer() {

}

void JsCoMediaPlayer::ConfigJsEngine(QJSEngine *engine) {
    QJSValue valueJsObj = engine->newQObject(this);
    engine->globalObject().setProperty("Dino_audio", valueJsObj);
}

void JsCoMediaPlayer::Stop() {
    pplayer->Stop();
}

void JsCoMediaPlayer::Pause() {
    pplayer->Pause();
}

void JsCoMediaPlayer::Play(QString str) {
    pplayer->PalyName(str.toStdString());
}

void JsCoMediaPlayer::Play() {
	pplayer->Paly();
}

void JsCoMediaPlayer::setVolume(int volume){
	pplayer->setVolume(volume);
}

void JsCoMediaPlayer::setMedia(const QString &path) {
	pplayer->setMedia(path.toStdString());
}

void JsCoMediaPlayer::setVideoOutput(int id) {
	pplayer->setVideoOutput(id);
}

