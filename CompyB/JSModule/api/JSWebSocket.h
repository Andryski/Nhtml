﻿#pragma once
#include <QJSEngine>

#include "../../Dino/Api/Sockets/IWebSocketAPI.h"

class JSWebSocket : public QObject
{
	Q_OBJECT
public:
	JSWebSocket(IWebSocket *WebSocket, QObject *parent = nullptr) : QObject(parent), webSocket(WebSocket)
	{
		webSocket->onopen = [&]() { _onopen(); };
		webSocket->onclose = [&](std::string str) { _onclose(QString::fromStdString(str)); };
		webSocket->onmessage = [&](std::string str) { _onmessage(QString::fromStdString(str)); };
		webSocket->onerror = [&](std::string str) { _onerror(QString::fromStdString(str)); };
	}
	~JSWebSocket() {};
	Q_INVOKABLE QJSValue _open(QJSValue url) {
		auto y1 = onopen.toString();
		auto y2 = onopen.isCallable();

		auto y3 = fun1.toString();
		auto y4 = fun1.isCallable();

		auto test1 = this->property("onopen");
		auto test2 = test1.toString();

		webSocket->open(url.toString().toStdString());
		return QJSValue();
	}
	Q_INVOKABLE QJSValue _onopen() {
		QJSValue result;
		QJSValue test = onopen.toString();
		if (onopen.isCallable())
			result = onopen.call();

		return QJSValue();
	}

	Q_INVOKABLE QJSValue _onclose(QJSValue event) {
		QJSValueList list;
		list.append(event);
		if (onclose.isCallable())
			onclose.call(list);
		return QJSValue();
	}
	Q_INVOKABLE QJSValue _onmessage(QJSValue event) {
		QJSValueList list;
		list.append(event);
		if (onmessage.isCallable())
			onmessage.call(list);
		return QJSValue();
	}
	Q_INVOKABLE QJSValue _onerror(QJSValue error) {
		QJSValueList list;
		list.append(error);
		if (onerror.isCallable())
			onerror.call(list);
		return QJSValue();
	}
	Q_INVOKABLE QJSValue send(QJSValue message) {
		webSocket->send(message.toString().toStdString());
		return QJSValue();
	}
	Q_INVOKABLE QJSValue Close() {
		webSocket->close();
		return QJSValue();
	}

	QJSValue onopen;
	QJSValue onclose;
	QJSValue onmessage;
	QJSValue onerror;

	Q_PROPERTY(QJSValue onopen WRITE setOnopen)
	void setOnopen(const QJSValue &a) {
		onopen = a;
	}

	Q_PROPERTY(QJSValue onclose WRITE setOnclose)
	void setOnclose(const QJSValue &a) {
		onclose = a;
	}

	Q_PROPERTY(QJSValue onmessage WRITE setOnmessage)
	void setOnmessage(const QJSValue &a) {
		onmessage = a;
	}

	Q_INVOKABLE void setFun(QJSValue a) {
		fun1 = a;
	}
	QJSValue fun1;

private:
	QJSEngine* m_engine;
	IWebSocket *webSocket;
};

class JSWebSocketFactory : public QObject
{
	Q_OBJECT
public:
	JSWebSocketFactory(QJSEngine* engine, IWebSocketFabric *WebSocketFabric)
	: m_engine(engine),webSocketFabric(WebSocketFabric) {}
	Q_INVOKABLE QJSValue createSocer() {
		auto soc = webSocketFabric->Create();
		auto jssoc = new JSWebSocket(soc);
		auto ret = m_engine->newQObject(jssoc);
		/*ret.setProperty("onopen", jssoc->onopen);
		ret.setProperty("onclose", jssoc->onclose);
		ret.setProperty("onmessage", jssoc->onmessage);
		ret.setProperty("onerror", jssoc->onerror);*/
		return ret;
	}
	Q_INVOKABLE QJSValue createSocerParem(QJSValue url) {
		auto soc = webSocketFabric->CreateParam(url.toString().toStdString());
		auto jssoc = new JSWebSocket(soc);
		auto ret = m_engine->newQObject(jssoc);/*
		ret.setProperty("onopen", jssoc->onopen);
		ret.setProperty("onclose", jssoc->onclose);
		ret.setProperty("onmessage", jssoc->onmessage);
		ret.setProperty("onerror", jssoc->onerror);*/
		return ret;
	}


    void ConfigJsEngine(QJSEngine *engine) {
        QJSValue varjswebsocet = engine->newQObject(this);
        auto test11 = varjswebsocet.isError();
        auto test21 = varjswebsocet.toString();
        // todo через точку _Dino._WebSocket почемуто не работает
        engine->globalObject().setProperty("_Dino_WebSocket", varjswebsocet);
        QJSValue isret = engine->evaluate("_Dino_WebSocket");
        test11 = isret.isError();
        test21 = isret.toString();
        //fixme: createSocer
        engine->evaluate(
                "function WebSocket() {"
                "    return _Dino_WebSocket.createSocer()"
                "}");
        auto result = engine->evaluate(
                "function WebSocket(vars) {"
                "    return _Dino_WebSocket.createSocerParem(vars)"
                "}");
        auto test1 = result.isError();
    }

private:
	QJSEngine* m_engine;
	IWebSocketFabric *webSocketFabric;
};

