#pragma once
#include <QJSEngine>

#include "../Dino/Api/Sockets/IWebSocketAPI.h"
#include "../Dino/Api/IConsoleLog.h"

class JSConsoleLog : public QObject
{
	Q_OBJECT
public:
	JSConsoleLog(IConsoleLogS *loger, QObject *parent = nullptr)
		: QObject(parent), loger(loger)
	{
	}
	~JSConsoleLog() {};
	Q_INVOKABLE void Debug(QJSValue str) {
		loger->Debug(str.toString().toStdString());
	}
	Q_INVOKABLE void Log(QJSValue str) {
		this->Info(str.toString());
	}
	Q_INVOKABLE void Info(QJSValue str) {
		loger->Info(str.toString().toStdString());
	}
	Q_INVOKABLE void Warning(QJSValue str) {
		loger->Warning(str.toString().toStdString());
	}
	Q_INVOKABLE void Critical(QJSValue str) {
		loger->Critical(str.toString().toStdString());
	}
	Q_INVOKABLE void Fatal (QJSValue str) {
		loger->Fatal(str.toString().toStdString());
	}
	IConsoleLogS *loger;
};
