#pragma once

//#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(JSMODULE_LIB)
#  define JSMODULE_EXPORT Q_DECL_EXPORT
# else
#  define JSMODULE_EXPORT //Q_DECL_IMPORT
# endif
#else
# define JSMODULE_EXPORT
#endif
