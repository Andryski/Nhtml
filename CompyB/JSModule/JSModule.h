#pragma once

#include <QString>
#include <QJSEngine>

#include <functional>

#include "jsmodule_global.h"
#include "JSMainApi.h"
#include "../Dino/Api/IRAndC.h"
#include "../Dino/Api/IRuner.h"


//https://stackoverflow.com/questions/23661437/how-to-make-a-c-code-public-headers-internal
//https://stackoverflow.com/questions/26234327/qlibrary-import-a-class
//https://stackoverflow.com/questions/19171139/how-to-build-an-api-with-qjsengine/37575936#37575936

class dino_js;

class JSMODULE_EXPORT JSModule : public IRAndC //IRuner
{

public:
    JSModule();
    virtual ~JSModule();

	void Start() override;
	void Load() override;
	void Unload() override;
	void Translate() override;
	void Start(std::string text) override;
	//void SetCallBack() override;
	void SetCallBack2(IDinoAPI *call) override;

	QString getres();

	std::string gettest() override;

	void PushButton(int id) override;

	//IRunerCallBack callBack;

	JSMainApi *jsMainApi;

private:
	QJSEngine myEngine;
	QJSValue value;
	dino_js *js_calbacks;
};
//https://stackoverflow.com/questions/32040558/passing-a-qobject-to-an-script-function-with-qjsengine


extern "C" inline JSMODULE_EXPORT IRAndC *getNewClass(){
	return new JSModule();
}

//extern "C" JSMODULE_EXPORT int glopfild = 0;


// https://stackoverflow.com/questions/19171139/how-to-build-an-api-with-qjsengine/37575936#37575936
// https://stackoverflow.com/search?q=+Q_INVOKABLE+QJSValue
