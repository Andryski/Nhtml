﻿#include <QFile>
#include <QTextStream>
#include <iostream>

/// @brief Logging
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QByteArray localMsg = msg.toLocal8Bit();
	QString messegeOut;
	QString typeS;
	QString contextSFie = context.file;
	QString contextSLine = QString::number(context.line);
	QString contextSFunc = context.function;
	switch (type) {
	case QtDebugMsg:
		typeS ="Debug:";
		break;
	case QtInfoMsg:
		typeS = "Info:";
		break;
	case QtWarningMsg:
		typeS = "Warning:";
		break;
	case QtCriticalMsg:
		typeS = "Critical:";
		break;
	case QtFatalMsg:
		typeS = "Fatal:";
		break;
		abort();
	}

	messegeOut = QString("%0: %1 (%2:%3, %4), %5, %6")
	.arg(typeS).arg(msg)
	.arg(contextSFie).arg(contextSLine)
	.arg(contextSFunc).arg(context.category)
	.arg(QString::number(context.version));

	if(QCoreApplication::instance()->property("debugOption").toBool()){
		std::cout << messegeOut.toStdString() << std::endl;
	}

	QFile file("log.txt");
	if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
		QTextStream outf(&file);
		outf << messegeOut << endl;
		//std::cout << messegeOut.toStdString() << std::endl;
	}
	file.close();
}

void Removelog()
{
	QFile file("log.txt");
	file.remove();
}