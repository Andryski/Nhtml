//
// Created by Andry on 2019-03-30.
//
#pragma once
#include <QSysInfo>

namespace SysInfo {
void WriteInfoToLog() {
	qDebug() << "QT version =" << QT_VERSION_STR;
	qDebug() << "buildAbi =" << QSysInfo::buildAbi();
	qDebug() << "kernelType =" << QSysInfo::kernelType();
#ifndef linux
	qDebug() << "machineUniqueId =" << QSysInfo::machineUniqueId();
#endif
	qDebug() << "prettyProductName =" << QSysInfo::prettyProductName();
	qDebug() << "productType =" << QSysInfo::productType();
	qDebug() << "productVersion =" << QSysInfo::productVersion();
};
}