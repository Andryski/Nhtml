#pragma once
#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QTabBar>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "CTabBar.h"

class Navigate_ui : public QWidget
{
	Q_OBJECT

public:
	Navigate_ui(QWidget *parent = Q_NULLPTR) : QWidget(parent)
	{
		//tabBar = new QTabBar();
		cTabBar = new CTabBar();
		h_layout_tabs = new QHBoxLayout();
		buttonAdd = new QPushButton("+");
		buttonAdd->setMaximumWidth(37);
		//h_layout_tabs->addWidget(tabBar);
		h_layout_tabs->addWidget(cTabBar);
		h_layout_tabs->addWidget(buttonAdd);

		lineEdit = new QLineEdit();
		lineEdit->setText("test");
		buttonGo = new QPushButton("Go");
		h_layout_addres = new QHBoxLayout();
		h_layout_addres->addWidget(lineEdit);
		h_layout_addres->addWidget(buttonGo);

		v_layout = new QVBoxLayout(this);
		v_layout->addLayout(h_layout_tabs);
		v_layout->addLayout(h_layout_addres);

		//tabBar->setTabsClosable(true);
		connect(cTabBar, &CTabBar::currentChanged, this, &Navigate_ui::signal_navig_on_tab);

		connect(buttonAdd, &QPushButton::clicked, this, &Navigate_ui::slotAddTabButton);
		//connect(tabBar, &QTabBar::currentChanged, this, &Navigate_ui::on_tabWidget_currentChanged);
		//connect(tabBar, &QTabBar::tabCloseRequested, this, &Navigate_ui::on_tabBar_tabCloseRequested);

		connect(buttonGo, &QPushButton::clicked, this, &Navigate_ui::on_GoToAdress_clicked);
		connect(lineEdit, &QLineEdit::returnPressed, this, &Navigate_ui::on_GoToAdress_clicked);

		cTabBar->setTabsClosable(true);
		connect(cTabBar, &CTabBar::tabCloseRequested, this, &Navigate_ui::on_tabBar_tabCloseRequested);


		slotAddTabButton();
	};

	~Navigate_ui()
	{
		delete cTabBar;
		delete lineEdit;
		delete buttonAdd;
		delete buttonGo;
		delete h_layout_tabs;
		delete h_layout_addres;
		delete v_layout;
	};

signals:
	void signal_navig_to(int id, QString str);
	void signal_navig_on_tab(int id);
	void signal_add_tab(int id);
	void dino_load();
	void dino_close();
	void dino_active();
	void dino_unactive();

public slots:
	void on_GoToAdress_clicked();
	void on_tabBar_tabCloseRequested(int index);
	void on_tabWidget_currentChanged(int index);
	void slotAddTabButton();


private:
	//QTabBar *tabBar;
	QLineEdit *lineEdit;
	QPushButton *buttonGo;
	QPushButton *buttonAdd;
	QHBoxLayout *h_layout_tabs;
	QHBoxLayout *h_layout_addres;
	QVBoxLayout *v_layout;

    void on_tabBar_tabCloseRequested2(int index){
        emit on_tabBar_tabCloseRequested(index);
    };

public:
	CTabBar *cTabBar;
};

