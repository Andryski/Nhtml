#pragma once
#include <QObject>
#include <QTabBar>
#include <QTabWidget>
#include <QPushButton>

struct CompyTabIdent;

class CTabBar : public QTabBar
{
	Q_OBJECT

public:
	CTabBar(QWidget *parent = Q_NULLPTR);
	~CTabBar();
	//void tabCloseRequested(QString name);
	//void addTab(CompyTabIdent ident);
	//void addTab(QString name);
	void addEmptyTab();

    void tabCloseRequested2(int index);
//protected:
signals:
	//void tabCloseRequested(int index);// override;

private:
	QPushButton *addButton;
};

