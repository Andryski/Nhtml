#pragma once

//#include <QtWidgets/QMainWindow>

//todo исправить чтобы та можено было собирать через cmake
//#include "ui_CompyB.h"
//#include "GeneratedFiles/ui_CompyB.h"
//#include "ui_CompyB.h"
//#include <ui_CompyB.h>
#include <QMainWindow>
#include "../MasterDino/MasterDino.h"
#include "Navigate_ui.h"

struct STabs
{
	STabs() :empty(true) {};
	//url
	QString name;
	QWidget tab;
	int id;
	bool empty;
};

struct STabsNmae
{
	QString name;
	int id;
};

class CompyB : public QMainWindow
{
	Q_OBJECT

public:
	CompyB(QWidget *parent = Q_NULLPTR);
	~CompyB();
	void loadDino(QString);
	//QTabBar *tabBar;
    //QPushButton *addTabButton;
	
	QMap<int, QWidget*> mapidTabs;

	QMap<int, QBoxLayout*> mapidTabsL;

//	QTextStream *in;
//	QTextStream *err;
//	QTextStream *out;

public slots:
	void signal_navig_to(int id, QString str);
	void signal_navig_on_tab(int id);
	void signal_add_tab(int id);

    void on_tabBar_tabCloseRequested(int index);
	void about();

protected:
	MasterDino *MDino;
	int later;
	QVector<QWidget *> arrayTabs;
	QVector<STabs> sArrayTabs;
	QHash<STabsNmae, QWidget> sMapTabs;

	//UI
	//Ui::CompyBClass *ui;
	void *uip; //thank you cmake with auto generation qt gui
	Navigate_ui *navigate_;

	QMenu *helpMenu;
	QAction *aboutAct;
	QAction *aboutQtAct;
};
