#include "Navigate_ui.h"
#include "CompyTabIdent.h"
#include <QString>
#include <QDebug>


void Navigate_ui::on_tabBar_tabCloseRequested(int index) {
	qDebug() << "Navigate_ui on_tabBar_tabCloseRequested index=" << index;
	int count = cTabBar->count();
	if (count != 0) {
		cTabBar->removeTab(index);
	}
}

void Navigate_ui::on_GoToAdress_clicked()
{
	QString str = lineEdit->text();
	int con = cTabBar->currentIndex();
	if(con == -1){
		slotAddTabButton();
		con+=1;
	}
	cTabBar->setTabText(con, str);
	emit signal_navig_to(con, str);
}

void Navigate_ui::on_tabWidget_currentChanged(int index)
{
	emit signal_navig_on_tab(index);

	//int test = tabBar->currentIndex();

	//ui.scrollArea->setWidget(arrayTabs[index]);


	//ui.scrollArea->widget()->hide();
	///arrayTabs[later]->hide();

	//ui.scrollArea->setWidget(arrayTabs[index]);
	///arrayTabs[index]->show();
	////later = index;
	//ui->tabWidget->setCurrentIndex(lockedTab);
}

void Navigate_ui::slotAddTabButton() {
	//CompyTabIdent idet1;
	//idet1.name = "new tab";
	//cTabBar->addTab(QString("New tab"));
	cTabBar->addEmptyTab();
	int con = cTabBar->currentIndex();
	emit signal_add_tab(con);

	//	QBoxLayout *lay = new QVBoxLayout(ui.scrollArea);
	//	//todo: fixme: fix this
	//	QWidget *realtab = new QWidget(ui.scrollArea); //scrollArea);
	//	lay->addWidget(realtab);
	//	QPushButton *test = new QPushButton("new tab", realtab);
	//	//realtab->
	//	arrayTabs.append(realtab);
}
