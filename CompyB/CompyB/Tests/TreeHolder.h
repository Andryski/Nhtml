//
// Created by Andry Z on 05/10/2018.
//

#ifndef COMPYB_CM_TREEHOLDER_H
#define COMPYB_CM_TREEHOLDER_H


#include <QObject>
#include <QList>

class TreeHolder {

public:

	QObject *element;
	QString name;
	QString classname;
	QList<TreeHolder *> childrens;
	TreeHolder(QObject *ele){
		element = ele;
		name = element->objectName();
		classname = element->metaObject()->className();
		//foreach(children : element->children()){

		//}
		QList<QObject*> list = element->findChildren<QObject*>();
		//QList<QObject*> list = element->children();
		foreach (auto obj, list) {
				auto temp = new TreeHolder(obj);
				childrens.append(temp);
			}
	}
};


#endif //COMPYB_CM_TREEHOLDER_H
