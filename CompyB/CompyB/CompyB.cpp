﻿#include "CompyB.h"

#include <qpushbutton.h>
#include <QLineEdit>
#include <qshortcut.h>
#include <qapplication.h>

//#include "GeneratedFiles/ui_CompyB.h"
#include "ui_CompyB.h"
#include "myMessageOutput.h"
#include "SysInfo.h"
#include "Eula.h"

//private CompyB::CompyB Ui::CompyBClass ui;

//#define ui() dynamic_cast<Ui::CompyBClass *>(uip)

CompyB::CompyB(QWidget *parent)
	: QMainWindow(parent)
{
	/*in = new QTextStream(stdin);
	err = new QTextStream(stderr);
	out = new QTextStream(stdout);
	qDebug() << "dfff";
	*/
	Removelog();
	qInstallMessageHandler(myMessageOutput);

	uip=new Ui::CompyBClass();
	//Ui::CompyBClass ui;
	//auto test1 = ui();
	Ui::CompyBClass *ui = static_cast<Ui::CompyBClass *>(uip);
	ui->setupUi(this);

	MDino = new MasterDino(this);
	navigate_ = new Navigate_ui();
	ui->tabLayout->addWidget(navigate_);
	connect(navigate_, &Navigate_ui::signal_navig_to, this, &CompyB::signal_navig_to);
	connect(navigate_, &Navigate_ui::signal_navig_on_tab, this, &CompyB::signal_navig_on_tab);
    connect(navigate_, &Navigate_ui::signal_add_tab, this, &CompyB::signal_add_tab);
    connect(navigate_->cTabBar, &CTabBar::tabCloseRequested, this, &CompyB::on_tabBar_tabCloseRequested);


	//verticalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

	//horizontalLayout->addItem(verticalSpacer);

	SysInfo::WriteInfoToLog();
	auto accepted = Eula::IsAcceptedAndTryAccept(this);
	if (!accepted) {
		qInfo() << "License not accepted - exit";
		exit(0);
	}
	qInfo() << "License is accepted";

	aboutAct = new QAction(tr("&About"), this);
	connect(aboutAct, &QAction::triggered, this, &CompyB::about);

	aboutQtAct = new QAction(tr("About &Qt"), this);
	aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
	connect(aboutQtAct, &QAction::triggered, qApp, &QApplication::aboutQt);
	//connect(aboutQtAct, &QAction::triggered, this, &CompyB::aboutQt);

	helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
	helpMenu->addAction(aboutQtAct);
}

void CompyB::about()
{
	QMessageBox::about(this, "About Compy",
		"The Compy browser \n"
		"vers 0.2\n"
		"thid party:\n"
		"QT\n"
		"[Boost].DI(experimental)\n"
		"GoogleTests\n"
		"qt-android-cmake\n"
		"CMakeHelpers\n"
	);
}

CompyB::~CompyB(){
	delete MDino;
	delete navigate_;
}

void CompyB::loadDino(QString)
{
}

void CompyB::signal_navig_to(int id, QString str)
{
	Ui::CompyBClass *ui = static_cast<Ui::CompyBClass *>(uip);
	loadDino(str);
	
	auto *wid = mapidTabs[id];
	if (wid == nullptr)
	{
		wid = new QWidget();
		mapidTabs[id] = wid;

		MDino->LoadDino(str, wid);
	}
	if (wid->objectName() == ("null widget"))
	{
		int wtf = 0;
		wtf += 1;
	}
	ui->scrollArea->takeWidget();
	ui->scrollArea->setWidget(wid);
	//auto test
	//auto test = wid->parent();
	/*
	auto lodWid = ui.scrollArea->takeWidget();
	int num = mapidTabs.key(lodWid);
	if(lodWid != nullptr) {
		lodWid->setParent(nullptr);
	}
	ui->scrollArea->setWidget(wid);
	mapidTabs[num] = lodWid;*/
}

void CompyB::signal_navig_on_tab(int id)
{
	Ui::CompyBClass *ui = static_cast<Ui::CompyBClass *>(uip);
	qDebug() << "CompyB signal_navig_on_tab id=" << id;
	if(id >= 0){
		auto *wid = mapidTabs[id];
		//qDebug() << "CompyB signal_navig_on_tab mapidTabs id=" << wid;
		if (wid != nullptr)
		{
			qDebug() << "CompyB signal_navig_on_tab true";
			auto temp = ui->scrollArea->takeWidget();
			if (temp->objectName() == "null widget") {
				//temp = ui.scrollArea->();
				delete temp;
			}
			/*auto lodWid = ui.scrollArea->takeWidget();
			int num = mapidTabs.key(lodWid);
			if(lodWid != nullptr) {
				lodWid->setParent(nullptr);
			}
			ui.scrollArea->setWidget(wid);
			mapidTabs[num] = lodWid;*/
			//qDebug() << "CompyB signal_navig_on_tab takeWidget=" << ui.scrollArea->takeWidget();
			//if(ui.scrollArea->takeWidget() != nullptr)
			//qDebug() << "CompyB signal_navig_on_tab takeWidget parent=" << ui.scrollArea->takeWidget()->parent();
			ui->scrollArea->setWidget(wid);
			//wid->setParent(ui.scrollArea);
			//qDebug() << "CompyB signal_navig_on_tab after takeWidget=" << ui.scrollArea->takeWidget();
			//if(ui.scrollArea->takeWidget() != nullptr)
			//qDebug() << "CompyB signal_navig_on_tab after takeWidget parent=" << ui.scrollArea->takeWidget()->parent();


		}else{
			qDebug() << "CompyB signal_navig_on_tab false";
			//qDebug() << "CompyB signal_navig_on_tab takeWidget=" << ui.scrollArea->takeWidget();
			//if(ui.scrollArea->takeWidget() != nullptr)
			//qDebug() << "CompyB signal_navig_on_tab takeWidget parent=" << ui.scrollArea->takeWidget()->parent();
			//ui.scrollArea->takeWidget()->setParent(nullptr);
			ui->scrollArea->takeWidget();
			auto temp = new QWidget();
			temp->setObjectName("null widget");
			ui->scrollArea->setWidget(temp);
			//qDebug() << "CompyB signal_navig_on_tab after takeWidget=" << ui.scrollArea->takeWidget();
			//if(ui.scrollArea->takeWidget() != nullptr)
			//qDebug() << "CompyB signal_navig_on_tab after takeWidget parent=" << ui.scrollArea->takeWidget()->parent();

		}
	}
}

void CompyB::signal_add_tab(int id)
{
	if (mapidTabs[id] == nullptr) {
		mapidTabs.insert(id, nullptr);
	}
}

void CompyB::on_tabBar_tabCloseRequested(int id)
{
	qDebug() << "CompyB on_tabBar_tabCloseRequested id=" << id;
    if (mapidTabs[id] != nullptr) {
        delete mapidTabs[id];
        mapidTabs.remove(id);
    }
}

