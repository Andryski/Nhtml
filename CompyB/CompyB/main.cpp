#include "CompyB.h"
#include <QApplication>
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication app(argc, argv);

	QCoreApplication::setApplicationName("Compy Browser");
	QCoreApplication::setApplicationVersion("0.1");

	QCommandLineParser parser;
	parser.setApplicationDescription("Test helper");
	parser.addHelpOption();
	parser.addVersionOption();

	QCommandLineOption debugOption(QStringList() << "d" << "debug","Debug options");
	parser.addOption(debugOption);

	parser.process(app);
	const QStringList args = parser.positionalArguments();

	bool debug = parser.isSet(debugOption);
	app.setProperty("debugOption",debug);

	QMainWindow *window = new CompyB();//CompyB browser;
	window->show();
	return app.exec();
}
