/********************************************************************************
** Form generated from reading UI file 'CompyB.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COMPYB_H
#define UI_COMPYB_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CompyBClass
{
public:
    QAction *actionLanguage;
    QAction *actionLayout;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *urllineEdit;
    QPushButton *GoToAdress;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QHBoxLayout *tabLayout;
    QMenuBar *menuBar;
    QMenu *menuConfig;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *CompyBClass)
    {
        if (CompyBClass->objectName().isEmpty())
            CompyBClass->setObjectName(QStringLiteral("CompyBClass"));
        CompyBClass->resize(632, 500);
        actionLanguage = new QAction(CompyBClass);
        actionLanguage->setObjectName(QStringLiteral("actionLanguage"));
        actionLayout = new QAction(CompyBClass);
        actionLayout->setObjectName(QStringLiteral("actionLayout"));
        centralWidget = new QWidget(CompyBClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, 0, -1);
        urllineEdit = new QLineEdit(centralWidget);
        urllineEdit->setObjectName(QStringLiteral("urllineEdit"));
        urllineEdit->setEnabled(true);
        urllineEdit->setMaximumSize(QSize(16777215, 1));

        horizontalLayout_2->addWidget(urllineEdit);

        GoToAdress = new QPushButton(centralWidget);
        GoToAdress->setObjectName(QStringLiteral("GoToAdress"));
        GoToAdress->setMaximumSize(QSize(16777215, 1));
        GoToAdress->setProperty("C_id", QVariant(12));

        horizontalLayout_2->addWidget(GoToAdress);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 612, 410));
        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 2, 0, 1, 1);

        tabLayout = new QHBoxLayout();
        tabLayout->setSpacing(6);
        tabLayout->setObjectName(QStringLiteral("tabLayout"));

        gridLayout->addLayout(tabLayout, 0, 0, 1, 1);

        CompyBClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(CompyBClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 632, 21));
        menuConfig = new QMenu(menuBar);
        menuConfig->setObjectName(QStringLiteral("menuConfig"));
        CompyBClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(CompyBClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        CompyBClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(CompyBClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        CompyBClass->setStatusBar(statusBar);

        menuBar->addAction(menuConfig->menuAction());
        menuConfig->addAction(actionLanguage);
        menuConfig->addAction(actionLayout);

        retranslateUi(CompyBClass);

        QMetaObject::connectSlotsByName(CompyBClass);
    } // setupUi

    void retranslateUi(QMainWindow *CompyBClass)
    {
        CompyBClass->setWindowTitle(QApplication::translate("CompyBClass", "CompyB", Q_NULLPTR));
        actionLanguage->setText(QApplication::translate("CompyBClass", "Language", Q_NULLPTR));
        actionLayout->setText(QApplication::translate("CompyBClass", "Layout", Q_NULLPTR));
        GoToAdress->setText(QApplication::translate("CompyBClass", "Go", Q_NULLPTR));
        menuConfig->setTitle(QApplication::translate("CompyBClass", "Config", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CompyBClass: public Ui_CompyBClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COMPYB_H
