#pragma once

#include "qtcptalker_global.h"

#include <QTcpSocket>
#include <QQueue>
#include <QHostAddress>
#include <QObject>
#include <QAbstractSocket>
#include <QMutex>
#include <QMutexLocker>
#include <QSignalMapper>
#include <QThread>

#include "MyQTCPWrapper.h"

class QTCPTALKER_EXPORT QTCPTalk : public QObject
{
	Q_OBJECT

public:
	QTCPTalk(QObject *parent = Q_NULLPTR, bool create_socket = true, bool use_multi_thread_reader = true);
	~QTCPTalk();
	void connectTo(QHostAddress addr, quint16 port);
	void connectTo(QString addr, quint16 port);
	void connectTo(QPair<QString, quint16> addr);
	void setSocet(QTcpSocket *soc);

	void send(QString str);
	void send(QByteArray array);
	QByteArray read();
	QString read(int nozing);

	void onSokConnected();
	void onSokDisconnected();

	QTcpSocket *tcpSocket;

	bool WaitConnect();
	void waitForBytesWritten();

	Compy_Network::MyQTCPWrapper test;

public slots:
	void displayError(QAbstractSocket::SocketError socketError);
	void ReadyRead();
	
private slots:
	void Psend(QByteArray);
	void SlotConnect(QString addr, quint16 port);
	
signals:
	void SigWrite(QByteArray arr);

//protected signals:
	//void SigConnectToHost(QString addr, quint16 port, QPrivateSignal);
	void SigConnectToHost(QString addr, quint16 port);

private:

	QThread *thread;
	QSignalMapper *mapper;
	// QObject::sender()
	QMutex has_messege;
	QMutex lock_messege_queue;
	QQueue<QByteArray> messegepool;

	QString peerAddress;
	int peerPort;
	QString peerName;



	//QDataStream in;
	QString currentFortune;

	bool cnow_len = false;
	unsigned long len = 0;
	//QNetworkSession *networkSession;

	bool multi_thread_reader;
	void Create_Socket();
};
