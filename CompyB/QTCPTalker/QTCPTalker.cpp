#include "QTCPTalker.h"
#include <QThread>

QTCPTalk::QTCPTalk(QObject *parent, bool create_socket, bool use_multi_thread_reader):
		QObject(parent), multi_thread_reader(use_multi_thread_reader)
{
	tcpSocket = nullptr;
	has_messege.lock();
	if(create_socket){
		tcpSocket = new QTcpSocket();// this);
		Create_Socket();
	}
}

QTCPTalk::~QTCPTalk() {
	tcpSocket->abort();
	tcpSocket->close();
//	delete tcpSocket;

};

void QTCPTalk::Create_Socket() {

	if(multi_thread_reader){

		tcpSocket->setParent(nullptr);

		//qWarning() << "Create_Socket = multi_thread_reader";
		thread = new QThread();
		connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::DirectConnection);



		QMutex *start_finish = new QMutex();

		connect(thread, &QThread::started, [=]() {
			//connect(tcpSocket, &QTcpSocket::readyRead, this, &QTCPTalk::ReadyRead);
			connect(tcpSocket, &QTcpSocket::readyRead, [this]() {
				this->ReadyRead();
			});
			connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), [this](QAbstractSocket::SocketError socketError) { 
				this->displayError(socketError);
			});
			

			connect(tcpSocket, &QTcpSocket::connected, [this]() {
				this->onSokConnected();
			});
			connect(tcpSocket, &QTcpSocket::disconnected, [this]() {
				this->onSokDisconnected();
			});
			//,Qt::BlockingQueuedConnection
			//SlotConnect
			
			connect(this, &QTCPTalk::SigConnectToHost, thread, [this](QString addr, quint16 port)
			{
				tcpSocket->connectToHost(addr, port);
			}, Qt::BlockingQueuedConnection
			);


			start_finish->unlock();
			/*connect(this, &QTCPTalk::SigWrite, this, &QTCPTalk::Psend,Qt::ConnectionType::QueuedConnection);*
			connect(this, &QTCPTalk::SigWrite, thread,[this](QByteArray arr) {
				tcpSocket->write(arr); 
			},Qt::ConnectionType::QueuedConnection);
			/*
			while (true)
			{
				QCoreApplication::processEvents();
			}*/
		});

		tcpSocket->moveToThread(thread);
		start_finish->lock();
		thread->start();
		start_finish->lock();
		start_finish->unlock();
		delete start_finish;

		//QCoreApplication::processEvents();
		connect(this, &QTCPTalk::SigWrite, thread, [this](QByteArray arr) {
			tcpSocket->write(arr);
		}, Qt::ConnectionType::QueuedConnection);
		//QThread::currentThread()->sleep(3);

		
		//connect(this, &QTCPTalk::SigConnectToHost, this, &QTCPTalk::SlotConnect, Qt::BlockingQueuedConnection);
		
	}
}

void QTCPTalk::SlotConnect(QString addr, quint16 port) {
	tcpSocket->connectToHost(addr, port);
}

void QTCPTalk::connectTo(QHostAddress addr, quint16 port)
{
	//tcpSocket->connectToHost(addr, port);
	auto connected = tcpSocket->isOpen();
	auto connected2 = tcpSocket->state();
}

void QTCPTalk::connectTo(QString addr, quint16 port)
{
	emit SigConnectToHost(addr, port);
	//tcpSocket->connectToHost(addr, port);
	auto connected = tcpSocket->isOpen();
	auto connected2 = tcpSocket->state();
	auto adr = tcpSocket->peerAddress().toString().toStdString();
}

void QTCPTalk::connectTo(QPair<QString, quint16> addr)
{
	tcpSocket->connectToHost(addr.first, addr.second);
}

bool QTCPTalk::WaitConnect() {
	return tcpSocket->waitForConnected();
}

void QTCPTalk::setSocet(QTcpSocket * soc)
{
	if(tcpSocket != nullptr)
		delete tcpSocket;
	tcpSocket = soc;

	peerAddress = tcpSocket->peerAddress().toString();
	peerPort = tcpSocket->peerPort();
	peerName = tcpSocket->peerName();

	Create_Socket();
}

void QTCPTalk::send(QString str)
{
	QByteArray temp;
	temp.append(str);
	auto s = temp.size();
	send(temp);
}

void QTCPTalk::send(QByteArray array)
{
	int size = array.size();
	QString textsize(QString::number(size)+"#");
	QByteArray temp;
	temp.append(textsize);

	//emit Psend(temp);
	//emit Psend(array);
	//tcpSocket->write(temp);
	//tcpSocket->write(array);
	
	emit SigWrite(temp);
	emit SigWrite(array);
}


void QTCPTalk::Psend(QByteArray arr) {
	tcpSocket->write(arr);
}

void QTCPTalk::waitForBytesWritten() {
	tcpSocket->waitForBytesWritten();
}

void QTCPTalk::displayError(QAbstractSocket::SocketError socketError)
{

	switch (socketError) {
	case QAbstractSocket::RemoteHostClosedError:
		qWarning() << "Remote Host Closed Error";
		break;
	case QAbstractSocket::HostNotFoundError:
		qWarning() << "The host was not found.";
		break;
	case QAbstractSocket::ConnectionRefusedError:
		qWarning() << "The connection was refused by the peer.";
		break;
	default:
		qWarning() << QString("The following error occurred: %1.").arg(tcpSocket->errorString());
		break;
	}
}

//этот код писался вечером и отлаживался ночью на хакатоне AH
void QTCPTalk::ReadyRead()
{
	//QTcpSocket* readSocket = qobject_cast<QTcpSocket*>(sender());
	//bool test1 = tcpSocket == readSocket;

	auto addr = tcpSocket->peerAddress().toString().toStdString();

	while (tcpSocket->bytesAvailable())
	{
		if (!cnow_len) {
			QByteArray temp;
			temp = tcpSocket->read(1);
			QString lenCh(temp);
			if (lenCh == "#")
			{
				cnow_len = true;
			} else {
				len *= 10;
				len += lenCh.toInt();
			}
			if (len > 1000000) {
				qWarning() << "len more 1000000, abort conection " + peerAddress + " " + peerName;
				tcpSocket->write("don't try hack pleas and eat my shorts");
				tcpSocket->disconnectFromHost();

			}
		} else {
			if (tcpSocket->bytesAvailable() >= len) {
				lock_messege_queue.lock();
				messegepool.append(tcpSocket->read(len));
				has_messege.unlock();
				lock_messege_queue.unlock();
				cnow_len = false;
				len = 0;
			}
		}
	}
}

QByteArray QTCPTalk::read() {
	QTcpSocket* readSocket = qobject_cast<QTcpSocket*>(sender());
	bool test1 = tcpSocket == readSocket;

	auto connect = tcpSocket->waitForConnected();

	//QCoreApplication::processEvents();

	has_messege.lock();
	lock_messege_queue.lock();
	auto ret =  messegepool.takeFirst();
	if(messegepool.count() > 0) {
		has_messege.unlock();
	}
	lock_messege_queue.unlock();
	return ret;
}

/*
 void Server::readyRead(){

    QByteArray buffer;
    QTcpSocket* readSocket = qobject_cast<QTcpSocket*>(sender());
    buffer = readSocket->readAll();

    QString mytext = QString::fromStdString(buffer);
    qDebug() << mytext;
}
 */
///https://stackoverflow.com/questions/36265902/c-multi-client-tcp-server-with-qlist
/*

RootWindow::RootWindow(QWidget *parent): QMainWindow(parent)
{
    server = new QLocalServer;
    list = new QList<QLocalSocket*>;

    mapper = new QSignalMapper(this);

    connect(server, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
    connect(mapper, SIGNAL(mapped(int)), this, SLOT(slotReadyRead(int)));

    server->listen("TestServer");
}

RootWindow::~RootWindow()
{
    delete list;
}

void RootWindow::slotNewConnection()
{
    qWarning() << "newConnection";
    list->append(server->nextPendingConnection());

    //here you map each client to its number in the list
    mapper->setMapping(list->last(), list->length()-1);

    //here we say, that when ever a client from the QList sends readyRead() the mapper should be used
    //with the property (list->length()-1) defined in the line above
    connect(list->last(), SIGNAL(readyRead()), mapper, SLOT(map()));
}

void RootWindow::slotReadyRead(int index)
{
    qWarning() << "Client " << index << " has written: " << list->at(index)->readAll();
}
 */

QString QTCPTalk::read(int nozing) {

	QByteArray temp = read();
	return QString(temp);
}

void QTCPTalk::onSokConnected()
{
	peerAddress = tcpSocket->peerAddress().toString();
	peerName = tcpSocket->peerName();
	qDebug() << "Connect" + peerAddress + " " + peerName;
	//auto test1 = QObject::sender();
	//auto test2 = (QTcpSocket *)test1;
	//auto test3 = test2->peerAddress();
	//auto test4 = test2->peerPort();
	//auto test5 = test2->peerName();
}

void QTCPTalk::onSokDisconnected()
{
	//auto test11 = (QTCPTalk*)QThread::currentThread()->parent();
	//auto test5 = test11->peerAddress.toStdString();
	//int test6 = test11->peerPort;

	//qDebug() << "connect";
	//auto test1 = sender();
	//auto test2 = (QTcpSocket *)test1;
	//auto test3 = tcpSocket->peerAddress().toString().toStdString();
	//auto test4 = test2->peerPort();
	qDebug() << "Disconnected " + peerAddress + " " + peerName;
}


