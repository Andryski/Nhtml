#include "MyQTCPWrapper.h"

namespace Compy_Network {

MyQTCPWrapper::MyQTCPWrapper(QTcpSocket *socket, QObject *parent): QObject(parent)
{
	//has_messege.lock();
	if (socket==nullptr)
	{
		tcpSocket = new QTcpSocket(this);
	} else {
		tcpSocket = socket;
		//tcpSocket->setParent(this);
	}
	//connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &MyQTCPWrapper::displayError);
	connect(tcpSocket, &QTcpSocket::readyRead, this, &MyQTCPWrapper::ReadyRead);
	connect(tcpSocket, &QTcpSocket::connected, this, &MyQTCPWrapper::mySokConnected);
	connect(tcpSocket, &QTcpSocket::disconnected, this, &MyQTCPWrapper::mySokDisconnected);
}

MyQTCPWrapper::~MyQTCPWrapper() {

}

	void MyQTCPWrapper::Init()
	{
		//tcpSocket->moveToThread(QThread::currentThread());
		connect(tcpSocket, &QTcpSocket::readyRead, this, &MyQTCPWrapper::ReadyRead);
		connect(tcpSocket, &QTcpSocket::connected, this, &MyQTCPWrapper::mySokConnected);
		connect(tcpSocket, &QTcpSocket::disconnected, this, &MyQTCPWrapper::mySokDisconnected);
	}

	/*void MyQTCPWrapper::ConnectTo(const QHostAddress &addr, const quint16 &port) const
{
	tcpSocket->connectToHost(addr, port);
};
void MyQTCPWrapper::ConnectTo(const QString &addr, const quint16 &port) const {
	tcpSocket->connectToHost(addr, port);
};*/
void MyQTCPWrapper::ConnectTo(const QPair<QString, quint16> &addr) const {
	tcpSocket->connectToHost(addr.first, addr.second);
}

bool MyQTCPWrapper::WaitConnect()
{
	return tcpSocket->waitForConnected();
}

bool MyQTCPWrapper::waitForBytesWritten()
{
	return tcpSocket->waitForBytesWritten();
}

void MyQTCPWrapper::SendM(const QString &str) const
{
	QByteArray temp;
	temp.append(str);
	auto s = temp.size();
	Send(temp);
}

void MyQTCPWrapper::Send(const QByteArray &array) const
{
	int size = array.size();
	QString textsize(QString::number(size) + "#");
	QByteArray temp;
	temp.append(textsize);

	tcpSocket->write(temp);
	tcpSocket->write(array);

	//emit SigWrite(temp);
	//emit SigWrite(array);
}

void MyQTCPWrapper::ReadyRead()
{
	while (tcpSocket->bytesAvailable())
	{
		if (!cnow_len) {
			QByteArray temp;
			temp = tcpSocket->read(1);
			QString lenCh(temp);
			if (lenCh == "#")
			{
				cnow_len = true;
			} else {
				len *= 10;
				len += lenCh.toInt();
			}
			if (len > 1000000000) {
				qWarning() << "len more 1000000000, abort conection " + peerAddress + " " + peerName;
				tcpSocket->write("don't try hack pleas and eat my shorts");
				tcpSocket->disconnectFromHost();

			}
		} else {
			qDebug() << "len="<< len;
			qDebug() << "bytesAvailable="<< tcpSocket->bytesAvailable();
			qDebug() << "reciveLen="<< reciveLen;
			//if (tcpSocket->bytesAvailable() >= len) {

			auto bytesAvailable = tcpSocket->bytesAvailable();
			if(bytesAvailable + reciveLen <= len) {
				reciveArray.append(tcpSocket->read(bytesAvailable));
				reciveLen += bytesAvailable;
			}else{
				if(bytesAvailable + reciveLen > len) {
					auto templen = len - reciveLen;
					reciveArray.append(tcpSocket->read(templen));
					reciveLen += templen;
				}
			}


			if (reciveLen == len)
			{
				lock_messege_queue.lock();
				messegepool.append(reciveArray);

				reciveArray.clear();
				reciveLen=0;

				//messegepool.append(tcpSocket->read(len));
				waitMessege.wakeAll();
				//if(messegepool.count()==1)
					//has_messege.unlock();
				lock_messege_queue.unlock();
				cnow_len = false;
				len = 0;
			}
		}
	}
}

QByteArray MyQTCPWrapper::readArray() {
	//QTcpSocket* readSocket = qobject_cast<QTcpSocket*>(sender());
	//bool test1 = tcpSocket == readSocket;

	//auto connect = tcpSocket->waitForConnected();

	//QCoreApplication::processEvents();
	//QString result = "";
//while (true) {
	//has_messege.lock();
		lock_messege_queue.lock();
		//auto ret = messegepool.takeFirst();
	if (messegepool.count() == 0) {
		waitMessege.wait(&lock_messege_queue);
	}
		//if (messegepool.count() > 0) {
			//has_messege.unlock();
			auto ret = messegepool.takeFirst();
			lock_messege_queue.unlock();

			return ret;
	//	}else
	//	{

	//	}
		//lock_messege_queue.unlock();
	//	QThread::currentThread()->sleep(1);
		//return ret;
//	}
}

QString MyQTCPWrapper::readString() {

	QByteArray temp = readArray();
	return QString(temp);
}

	bool MyQTCPWrapper::waitForConnected()
	{
		return tcpSocket->waitForConnected();
	}

	void MyQTCPWrapper::displayError(QAbstractSocket::SocketError& error)
{
	switch (error) {
	case QAbstractSocket::RemoteHostClosedError:
		qWarning() << "Remote Host Closed Error";
		break;
	case QAbstractSocket::HostNotFoundError:
		qWarning() << "The host was not found.";
		break;
	case QAbstractSocket::ConnectionRefusedError:
		qWarning() << "The connection was refused by the peer.";
		break;
	default:
		qWarning() << QString("The following error occurred: %1.").arg(tcpSocket->errorString());
		break;
	}
}

//void MyQTCPWrapper::mySokConnected() {};
//void MyQTCPWrapper::mySokDisconnected(){};
}