#include "QTCPTalker2.h"

namespace Compy_Network {

//QTCPTalker2::QTCPTalker2(QObject *parent, MyQTCPWrapper *tcpsocket, bool use_multi_thread_reader)
QTCPTalker2::QTCPTalker2(QObject *parent, QTcpSocket *tcpsocket)
{
	thread = new QThread();
	if (tcpsocket == nullptr) {
		socket = new MyQTCPWrapper();
	}else
	{
		tcpsocket->moveToThread(thread);
		socket = new MyQTCPWrapper(tcpsocket);
	}

	connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::DirectConnection);


	connect(socket, &MyQTCPWrapper::mySokConnected, [this]() {
		this->onSokConnected();
	});
	connect(socket, &MyQTCPWrapper::mySokDisconnected, [this]() {
		this->onSokDisconnected();
	});
	//,Qt::BlockingQueuedConnection
	//SlotConnect

	connect(this, &QTCPTalker2::ConnectToS, socket, &MyQTCPWrapper::ConnectTo, Qt::BlockingQueuedConnection);

	connect(this, &QTCPTalker2::WaitConnectS, socket, &MyQTCPWrapper::waitForConnected, Qt::BlockingQueuedConnection);

	connect(this, &QTCPTalker2::waitForBytesWrittenS, socket, &MyQTCPWrapper::waitForBytesWritten, Qt::BlockingQueuedConnection);

	connect(this, &QTCPTalker2::SendS, socket, &MyQTCPWrapper::SendM, Qt::BlockingQueuedConnection);
	connect(this, &QTCPTalker2::SendB, socket, &MyQTCPWrapper::Send, Qt::BlockingQueuedConnection);

	QMutex *start_finish = new QMutex();

	connect(thread, &QThread::started, [=]()
	{
		//socket->Init();
		start_finish->unlock();
	});

	socket->moveToThread(thread);

	start_finish->lock();
	thread->start();
	start_finish->lock();
	start_finish->unlock();
	delete start_finish;

}

QTCPTalker2::~QTCPTalker2() {
}

void QTCPTalker2::send(const QString& messege)
{
	emit SendS(messege);
}

void QTCPTalker2::send(const QByteArray& messege)
{
	emit SendB(messege);
}



QByteArray QTCPTalker2::readArray()
{
	return socket->readArray();
}

QString QTCPTalker2::readString()
{
	return socket->readString();
}

void QTCPTalker2::connectTo(const QPair<QString, quint16>& addr)
{
	emit ConnectToS(addr);
}

bool QTCPTalker2::WaitConnect()
{
	return emit WaitConnectS();
	//return socket->waitForConnected();
}

void QTCPTalker2::waitForBytesWritten()
{
	emit waitForBytesWrittenS();
}


void QTCPTalker2::onSokConnected()
{
}

void QTCPTalker2::onSokDisconnected()
{
}

	
}