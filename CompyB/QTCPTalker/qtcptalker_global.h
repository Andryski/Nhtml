#pragma once

//#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QTCPTALKER_LIB)
#  define QTCPTALKER_EXPORT Q_DECL_EXPORT
# else
#  define QTCPTALKER_EXPORT //Q_DECL_IMPORT
# endif
#else
# define QTCPTALKER_EXPORT
#endif
