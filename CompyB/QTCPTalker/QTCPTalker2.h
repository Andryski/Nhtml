#pragma once
#include <QObject>

#include <QHostAddress>
#include "MyQTCPWrapper.h"

namespace Compy_Network {
class QTCPTalker2: public QObject
{
	Q_OBJECT
public:
	//QTCPTalker2(QObject *parent = Q_NULLPTR, MyQTCPWrapper *tcpsocket = nullptr, bool use_multi_thread_reader = true);
	//QTCPTalker2(QObject *parent = Q_NULLPTR);
	QTCPTalker2(QObject *parent = Q_NULLPTR, QTcpSocket *tcpsocket = nullptr);
	~QTCPTalker2();

	QByteArray readArray();
	QString readString();

	void connectTo(const QPair<QString, quint16> &addr);
	void send(const QString &messege);
	void send(const QByteArray &messege);
	bool WaitConnect();
	void waitForBytesWritten();

signals:
	//void SigConnectToHost(QString addr, quint16 port);
	//void connectTo1(QHostAddress addr, quint16 port);
	//void connectTo2(QString addr, quint16 port);
	//void connectTo3(QPair<QString, quint16> addr);
	//void ConnectTo(const QHostAddress &addr, const quint16 &port) const;
	//void ConnectTo(const QString &addr, const quint16 &port) const;
	void ConnectToS(const QPair<QString, quint16> &addr) const;
	void SendS(const QString &messege) const;
	void SendB(const QByteArray &messege) const;
	bool WaitConnectS();
	bool waitForBytesWrittenS();

public slots:
	void onSokConnected();
	void onSokDisconnected();

private:
	QThread * thread;
	MyQTCPWrapper *socket;
};

}
