#pragma once
#include <QTcpSocket>
#include <QHostAddress>
#include <QObject>
#include <QAbstractSocket>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <QSignalMapper>
#include <QQueue>
#include <QHostAddress>
#include <QWaitCondition>

namespace Compy_Network{

class MyQTCPWrapper : public QObject
{
	Q_OBJECT

public:
	MyQTCPWrapper(QTcpSocket *socet = nullptr, QObject *parent = Q_NULLPTR);
	~MyQTCPWrapper();

	void Init();

	QByteArray readArray();
	QString readString();

	bool waitForConnected();

signals:
	void mySokConnected();
	void mySokDisconnected();

public slots:
	//void ConnectTo(const QHostAddress &addr, const quint16 &port) const;
	//void ConnectTo(const QString &addr, const quint16 &port) const;
	void ConnectTo(const QPair<QString, quint16> &addr) const;
	bool WaitConnect();
	bool waitForBytesWritten();
	void SendM(const QString &messege) const;
	void Send(const QByteArray &array) const;

private slots:
	void ReadyRead();
	void displayError(QAbstractSocket::SocketError &error);


private:

	QTcpSocket * tcpSocket;
	QSignalMapper *mapper;
	//QMutex has_messege;
	QWaitCondition waitMessege;
	QMutex lock_messege_queue;
	QQueue<QByteArray> messegepool;

	QString peerAddress;
	int peerPort;
	QString peerName;

	bool cnow_len = false;
	unsigned long len = 0;

	QByteArray reciveArray;
	unsigned long reciveLen = 0;
};

}