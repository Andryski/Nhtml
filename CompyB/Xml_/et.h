#pragma once

#include <QString>
#include <QPair>
#include <QMap>
#include <functional>
#include "Property.h"
#include "Element_worker.h"


class RElement
{
public:
	RElement() {};
	RElement(RElement *parent);
	~RElement();

	RElement *parent = nullptr;

	QVector<RElement *> vect;
	bool is_root;

	// et_root *root_ptr = nullptr;
	QString state = QString::null;
	
	QString name;

	bool is_lay = false;
	LayoutWork *lay = nullptr;
	
	//QMap<QString, QString> attributes;

	QString type;

	QVector<Property_Base*> prop;
	Property_Base *prop_curent;
private:
};
