#pragma once
#include <QString>
#include <functional>


//todo: ���������� �� name ��� ���
class Property_Base
{
public:
	Property_Base(const QString &Name) :name(Name) {}
	QString name;

	std::function<void(const QString &value)> worker_value = [&](const QString &Wvalue)
	{
		return;
	};
	std::function<void(const QString &name, const QString &value)> worker2 = nullptr;

	std::function<void(const QString &name)> worker_name = [&](const QString &Wname)
	{
		worker_value = [&](const QString &Wvalue)
		{
			worker2(Wname, Wvalue);
		};
	};

};

class Property_default : public Property_Base
{
public:
	Property_default(const QString &Name) :Property_Base(Name) {
		worker2 = [&](const QString &name, const QString &value)
		{
		};
	}
	QString text;
};


class Property_text : public Property_Base
{
public:
	Property_text(const QString &Name) :Property_Base(Name) {
		worker2 = [&](const QString &name, const QString &value)
		{
			if (name == QString("string"))
			{
				text = value;
			}
		};
	}
	QString text;
};

class Property_C_id : public Property_Base
{
public:
	Property_C_id(const QString &Name) :Property_Base(Name) {
		worker2 = [&](const QString &name, const QString &value)
		{
			if (name == QString("string"))
			{
				C_id = value.toInt();
			}
		};
	}
	int C_id;
};

class Property_rect : public Property_Base
{
public:
	Property_rect(const QString &Name) :Property_Base(Name) {
		worker2 = [&](const QString &name, const QString &value)
		{
			if (name == QString("x"))
			{
				x = value.toInt();
			}
			else if (name == QString("y"))
			{
				y = value.toInt();
			}
			else if (name == QString("width"))
			{
				width = value.toInt();
			}
			else if (name == QString("height"))
			{
				height = value.toInt();
			}
		};
	}
	int x;
	int y;
	int width;
	int height;
};
