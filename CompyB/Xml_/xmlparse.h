#ifndef XMLPARSE_H
#define XMLPARSE_H


#include <QXmlSimpleReader>
#include <QList>
#include "et.h"



class et_handler : public QXmlDefaultHandler
{
public:
  et_handler();
  ~et_handler();
  bool startDocument() override;
  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attribs) override;
  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName) override;
  bool characters(const QString &str) override;
  bool fatalError(const QXmlParseException &exception) override;

  RElement* get_tree();

private:
	RElement *ElemTreeRoot = nullptr;
	RElement *current_root = nullptr;
};

#endif // XMLPARSE_H
