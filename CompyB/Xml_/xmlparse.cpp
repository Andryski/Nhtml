#include "xmlparse.h"

et_handler::et_handler()
{
    ElemTreeRoot = new RElement(nullptr);
}

et_handler::~et_handler()
{
    delete ElemTreeRoot;
}

bool et_handler::startDocument()
{
    current_root = ElemTreeRoot;
    return true;
}


Property_Base* resolv(const QString &type_name)
{
	if (type_name == "geometry")
	{
		return new Property_rect(type_name);
	}
	else if (type_name == "text")
	{
		return new Property_text(type_name);
	}
	else if (type_name == "C_id")
	{
		return new Property_C_id(type_name);
	}
	else
	{
		return new Property_default(type_name);
	}
}


bool et_handler::startElement(const QString &namespaceURI,
                         const QString &localName,
                         const QString &qName,
                         const QXmlAttributes &attribs)
{
	if(current_root->state == QString("property"))
	{
		current_root->prop_curent->worker_name(localName);
	}else
	if (localName == "widget" || localName == "layout")
	{
		QString xml_class = attribs.value("class");
		QString xml_name = attribs.value("name");

		RElement *element = new RElement(current_root);
		element->type = xml_class;
		element->name = xml_name;
		element->state = localName;
		current_root->vect.push_back(element);
		//element->is_root = true;
		current_root = element;
	}else if (localName == "property")
	{
		current_root->state = "property";

		//ToDO: ��������� ��� � ��� �������� ����
		//Property_Base::resolv(attribs.qName(0));

		current_root->prop_curent = resolv(attribs.value(0));
		current_root->prop.push_back(current_root->prop_curent);
	}else if (localName == "property")
	{
		current_root->state = "property";
	}

    return true;
}

bool et_handler::endElement(const QString &namespaceURI,
                            const QString &localName,
                            const QString &qName)
{
	if (current_root->state == QString("property"))
	{
		if(localName == QString("property"))
		{
			current_root->state = "";
			current_root->state == QString::null;
		}
	}else if(localName == "widget" || localName == "layout")
	{
		current_root = current_root->parent;
	}
	
	return true;
}

bool et_handler::characters(const QString &str)
{
	//todo: ���������� �� 2 �� 3 ����� ������� 2 ���������
	if (!(str == "\r\n " || str == "\r\n" || str == "\r\n   " || str == "\r\n  " || str == "\r\n    ")) {
		if (current_root->state == QString("property"))
		{
			current_root->prop_curent->worker_value(str);
		}
	}
    return true;
}

bool et_handler::fatalError(const QXmlParseException &exception)
{
    QTextStream qt(stdout);
    qt << "ERROR " << exception.message() << "\n";

    return true;
}

RElement* et_handler::get_tree()
{
	return ElemTreeRoot;
}
