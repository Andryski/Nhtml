#include <QXmlSimpleReader>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "xmlparse.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_makeButton_clicked()
{
    QTextStream qt(stdout);
    QFile* file = new QFile("mainwindow.ui");

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
        qt << "!!!!!!!!!";

    QXmlSimpleReader xmlReader;
    QXmlInputSource *source = new QXmlInputSource(file);

    et_handler *handler = new et_handler;
    xmlReader.setContentHandler(handler);
    xmlReader.setErrorHandler(handler);

    bool ok = xmlReader.parse(source);

	RElement *element_tree = handler->get_tree();

	int a = 0;
	a+=1;

}
