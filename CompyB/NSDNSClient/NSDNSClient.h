#pragma once

#include "nsdnsclient_global.h"

#include <QPair>
#include <QString>
#include <QObject>

class NSDNSCLIENT_EXPORT NSDNSClient : public QObject
{
	Q_OBJECT
public:
	NSDNSClient(QObject *parent = Q_NULLPTR);
	~NSDNSClient();
	static QPair<QString, int> getAddr(const QString&);
};
