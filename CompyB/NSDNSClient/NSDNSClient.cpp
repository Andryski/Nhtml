#include <QCoreApplication>
#include <QHostInfo>
#include "NSDNSClient.h"

//#include "../QTCPTalker/QTCPTalker.h"
#include "../QTCPTalker/QTCPTalker2.h"

NSDNSClient::NSDNSClient(QObject *parent) : QObject(parent)
{
}

NSDNSClient::~NSDNSClient()
{
}

QPair<QString, int> NSDNSClient::getAddr(const QString &addr) {
	//todo: to Qdns
	QHostInfo info = QHostInfo::fromName("nsdns.compy.io");
	//QHostInfo info = QHostInfo::fromName("home.andry.top");
	auto temp1 = info.errorString().toStdString();
	//"Host not found"
    auto temp11 = info.error();

	auto temp2 = info.hostName().toStdString();
	auto temp3 = info.addresses();
	auto temp4 = temp3[0];
	auto temp5 = temp4.toString();

	Compy_Network::QTCPTalker2 socket;

	socket.connectTo(qMakePair<QString, quint16>(QString("nsdns.compy.io"), 6544));
	
	socket.WaitConnect();
	
	socket.send(addr);

	auto addres = socket.readString();
	auto port = socket.readString().toInt();

	return qMakePair(addres, port);

	///QTCPTalk *net = new QTCPTalk();

	if(QCoreApplication::instance()->property("debugOption").toBool()){

	}
}