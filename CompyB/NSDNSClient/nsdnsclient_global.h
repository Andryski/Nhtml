#pragma once

//#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(NSDNSCLIENT_LIB)
#  define NSDNSCLIENT_EXPORT Q_DECL_EXPORT
# else
#  define NSDNSCLIENT_EXPORT //Q_DECL_IMPORT
# endif
#else
# define NSDNSCLIENT_EXPORT
#endif
