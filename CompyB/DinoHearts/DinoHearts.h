//
// Created by Andry on 2019-02-26.
//
#pragma once
#include <string>
#include <iostream>
#include <memory>

#include "../Dino/Api/IRAndC.h"

class DinoHearts {
public:
	DinoHearts(const std::string &path);

	std::shared_ptr<IRAndC> GetInstance(const std::string &name);

private:
	std::string Path;
	IRAndC* GetJs();
	std::shared_ptr<IRAndC> GetNative();
};
