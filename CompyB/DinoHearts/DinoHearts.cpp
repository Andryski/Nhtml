//
// Created by Andry on 2019-02-26.
//

#include <QLibrary>
#include <QDebug>

#include "DinoHearts.h"

#ifdef Q_OS_ANDROID
#define Load_Dinamic_Library_Def
#endif

#ifdef Load_Dinamic_Library_Def
#include "../JSModule/JSModule.h"
#include "../JSModule/GetJSModule.h"
#endif

DinoHearts::DinoHearts(const std::string &path) :Path(path) {}

std::shared_ptr<IRAndC> DinoHearts::GetInstance(const std::string &name) {
	// todo: use https://github.com/Efrit/str_switch

	if(name == "js"){
		//todo: rename to qt_js
		return std::shared_ptr<IRAndC>(GetJs());
	}else if(name == "c#"){
		//todo: c#
		throw new std::runtime_error("c# not implemented yet");
	}else if(name == "node.js"){
		//todo: node.js
		throw new std::runtime_error("node.js not implemented yet, wait https://github.com/nodejs/node/issues/23265 "
							   "or use https://github.com/nodejs/node/issues/23265#issuecomment-452690239");
	}else if(name == "v8_js"){
		//todo: v8_js
		throw new std::runtime_error("v8_js not implemented yet");
	}else if(name == "native"){
		return GetNative();
		//todo: native
		throw new std::runtime_error("native not implemented yet");
	}

	throw new std::runtime_error("script typr=" + name + " not find");
}

IRAndC* DinoHearts::GetJs() {
	IRAndC* runer1 = nullptr;
	//std::shared_ptr<IRAndC> Point_runer;
#ifdef Load_Dinamic_Library_Def
	runer1 = getNewClass();//GetJSModule();
#else
	//auto temp2 = QCoreApplication::applicationFilePath();
		//auto temp2 = QCoreApplication::applicationDirPath();

		QLibrary myLib("JSModule");
		if (myLib.load())
		{
			typedef IRAndC* (*create_Runer_from_DLL)();
			create_Runer_from_DLL create_TestDLL_lib = (create_Runer_from_DLL)myLib.resolve("getNewClass");

			if (create_TestDLL_lib)
				runer1 = create_TestDLL_lib();

			//runer1->SetCallBack2(dApi->API);

		}else{
			auto testloadL = myLib.errorString();
			qWarning() << testloadL;
		}
#endif
	return runer1;
}

std::shared_ptr<IRAndC> DinoHearts::GetNative()
{
	IRAndC* runer = nullptr;
	QString path = QString::fromStdString(Path) + "Script/native_app";
	auto test = path.toStdString();
	QLibrary myLib(path);
	if (myLib.load())
	{
		typedef IRAndC* (*create_Runer_from_DLL)();
		create_Runer_from_DLL create_TestDLL_lib = (create_Runer_from_DLL)myLib.resolve("getNewClass");

		typedef void (*test_fun)();
		test_fun test1 = (test_fun)myLib.resolve("test1");
		test_fun test2 = (test_fun)myLib.resolve("test2");
		test_fun test3 = (test_fun)myLib.resolve("test3");
		test_fun test4 = (test_fun)myLib.resolve("test4");

		if (create_TestDLL_lib){
			runer = create_TestDLL_lib();
		}else{
			auto testloadL = myLib.errorString();
			qWarning() << testloadL;
			qWarning() << "lib not loaded";
		}

		//runer1->SetCallBack2(dApi->API);

	}else{
		auto testloadL = myLib.errorString();
		qWarning() << testloadL;
	}
	return std::shared_ptr<IRAndC>(runer);
}
