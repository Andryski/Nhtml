#include <QtCore/QCoreApplication>

#include "../QTCPTalker/QTCPTalker.h"

#include <QEventLoop>

#include <iostream>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	QTCPTalk test(&a);
	//test.connectTo("192.168.88.2", 543);
	//test.connectTo("localhost", 654);
	test.connectTo("::1", 654);
	test.send(QString("vk.ru"));
	QString test2 = test.read(2);
	std::cout << test2.toStdString() << std::endl;
	//a.processEvents(QEventLoop::AllEvents, 600000);
	std::cout << "even" << std::endl;
	a.processEvents();
	//a.exec();
	int b = 0;
	//return a.exec();
	return 0;
}
