// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� PROTOBWORKER_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� PROTOBWORKER_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.
#ifdef PROTOBWORKER_EXPORTS
#define PROTOBWORKER_API __declspec(dllexport)
#else
#define PROTOBWORKER_API __declspec(dllimport)
#endif

#include <string>
using namespace std;


//#include <>


class ProtoBWorker_impl;
// ���� ����� ������������� �� ProtoBWorker.dll
class PROTOBWORKER_API CProtoBWorker {
public:
	CProtoBWorker();
	~CProtoBWorker();
	
	string
private:
	ProtoBWorker_impl* _impl;
};

extern PROTOBWORKER_API int nProtoBWorker;

PROTOBWORKER_API int fnProtoBWorker(void);
