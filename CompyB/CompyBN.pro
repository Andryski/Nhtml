#-------------------------------------------------
#
# Project created by QtCreator 2017-07-01T20:32:30
#
#-------------------------------------------------

QT       += core gui network widgets qml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Compy
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    CompyB/CompyB.cpp \
    CompyB/main.cpp \
    Dino/Dino.cpp \
    Dino/IRuner.cpp \
    Dino/LayoutReader.cpp \
    JSModule/JSModule.cpp \
    MasterDino/AccesResolver.cpp \
    MasterDino/Dino_loader.cpp \
    MasterDino/MasterDino.cpp \
    NSDNSClient/NSDNSClient.cpp \
    QTCPTalker/QTCPTalker.cpp

HEADERS += \
    CompyB/CompyB.h \
    Dino/Dino.h \
    Dino/dino_global.h \
    Dino/IRAndC.h \
    Dino/IRuner.h \
    Dino/IRunerCallBack.h \
    Dino/LayoutReader.h \
    JSModule/JSModule.h \
    JSModule/jsmodule_global.h \
    MasterDino/AccesResolver.h \
    MasterDino/Dino_loader.h \
    MasterDino/MasterDino.h \
    MasterDino/masterdino_global.h \
    MasterDino/resource.h \
    NSDNSClient/NSDNSClient.h \
    NSDNSClient/nsdnsclient_global.h \
    QTCPTalker/QTCPTalker.h \
    QTCPTalker/qtcptalker_global.h

FORMS += \
    CompyB/CompyB.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    CompyB/CompyB.qrc

DISTFILES += \
    CompyB/CompyB.ico \
    CompyB/CompyB.rc

