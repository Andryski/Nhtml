#pragma once
#include <QThread>
#include <QString>

//protected
//public
class MyQThread : public QThread
{
	Q_OBJECT
	
		QString ThreadName;
public:
	MyQThread(const QString &name, QObject *parent = Q_NULLPTR);
	~MyQThread();
	//using QThread::QPrivateSignal;

//public slots:
	/*
	private	slots:
	void myStartedp();
	protected	slots:
	void myStartedp2();
	public	slots:
	void myStartedp3();

	void started();
	*/

signals:
	//void started(QPrivateSignal);
	void myStarted();

protected:
	void run() override;

	//friend class QThread;

};