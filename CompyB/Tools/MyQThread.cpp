#include "MyQThread.h"
#include "ThreadNameHelp.h"
#include <QMetaMethod>

MyQThread::MyQThread(const QString &name, QObject *parent):QThread(parent){
	ThreadName = name;

	//not work(
	const QMetaObject *moTest = QThread::metaObject();

	//QThread::connect(&QThread::started, this, &MyQThread::started);
	//connect(this, &QThread::started, this, &MyQThread::started);

	QList<QString> slotSignatures;
	QList<QString> signalSignatures;

	// Start from MyClass members
	for (int methodIdx = moTest->methodOffset(); methodIdx < moTest->methodCount(); ++methodIdx) {
		QMetaMethod mmTest = moTest->method(methodIdx);
		switch ((int)mmTest.methodType()) {
		case QMetaMethod::Signal:
			signalSignatures.append(QString(mmTest.methodSignature())); // Requires Qt 5.0 or newer
			break;
		case QMetaMethod::Slot:
			slotSignatures.append(QString(mmTest.methodSignature())); // Requires Qt 5.0 or newer
			break;
		}
	}
}

MyQThread::~MyQThread() {

}

//todo: move rename thread befor started signal
/*
void MyQThread::myStartedp() {
	int a = 0;
}
void MyQThread::myStartedp2() {
	int a = 0;
}
void MyQThread::myStartedp3() {
	int a = 0;
}

void MyQThread::started() {

}*/

void MyQThread::run() {
#ifdef myThread
	tool_thread::SetThreadName(ThreadName.toStdString().data());
#endif
	emit myStarted();
	QThread::run();
}

