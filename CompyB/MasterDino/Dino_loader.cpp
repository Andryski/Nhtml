#include "Dino_loader.h"

#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "MasterDino.h"
#include "../QTCPTalker/QTCPTalker2.h"

Dino_loader::Dino_loader()
{
}

Dino_loader::~Dino_loader()
{
}

void Dino_loader::Load(const QString & name)
{
	//Q_ASSERT(MasterDino::path == "");
	QString path = MasterDino::path + "/Apps/";
	std::string debug1 = path.toStdString();
	QDir folder = path + name;

	//todo: надо подумать должен ли путь быть абсолютным
/*
	if (!folder.isAbsolute()) {
		return;
	}
*/
	//int temp_version = get_version(name);
	auto absolutePath = folder.absolutePath();


	//auto addr2 = NSDNSClient::getAddr("test");
	//QThread::currentThread()->sleep(10);
	if (!folder.exists()) {
		DownloadDino(name);
		//return;
	}else {
		Update();
	}

	QString pathFolder = folder.path() + "/";
	QFile file(pathFolder + "Layaut/PC.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QByteArray in = file.readAll();
	file_layaut = QString(in);
	file.close();
	in.clear();

	file.setFileName(pathFolder + "vers.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	in = file.readAll();
	vers = QString(in);
	file.close();
	in.clear();

	file.setFileName(pathFolder + "Script_type.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		//return;
	} else {
		in = file.readAll();
		type_script = QString(in);
		file.close();
		in.clear();
		if(type_script == QString("js"))
		{
			file.setFileName(pathFolder + "Script/main.js");
			if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
				//return;
			}else {
				in = file.readAll();
				file_script = QString(in);
				file.close();
				in.clear();
			}
		}
	}
}

void Dino_loader::DownloadDino(const QString & str_addr)
{
	auto Dino_app_addres = NSDNSClient::getAddr(str_addr);

	Compy_Network::QTCPTalker2 tcp_net;
	
	//test_net.connectTo(qMakePair<QString, quint16>(QString("localhost"), 6544));
	tcp_net.connectTo(Dino_app_addres);
	tcp_net.WaitConnect();


	auto vers = tcp_net.readString();
	tcp_net.send(QString("get"));
	//tcp_net.waitForBytesWrittenS();
	//int a = 0;

	auto runnertypr = tcp_net.readString();
	auto jsscrypt = tcp_net.readString();
	auto layaut = tcp_net.readString();

	auto mediacount = tcp_net.readString().toInt();

	QVector<QPair<QString,QByteArray>> medias;
	if(mediacount > 0)
	{
		for (int i = 0; i < mediacount; ++i)
		{
			auto name = tcp_net.readString();
			auto data = tcp_net.readArray();
			auto temp = data.size();
			medias.append(qMakePair(name,data));
		}
	}

	QString path = MasterDino::path + "/Apps/";
	std::string debug1 = path.toStdString();
	QDir folder = path + str_addr;

	QString pathFolder = folder.path();

	auto test = folder.mkpath(pathFolder);

	QFile file(pathFolder + "/" + "vers.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream stream(&file);
	stream << vers;
	file.close();

	folder.mkdir(folder.path() + "/" + "Layaut/");
	folder.mkdir(folder.path() + "/" + "Script/");
	folder.mkdir(folder.path() + "/" + "Lang/");


	file.setFileName(pathFolder + "/" + "Layaut/PC.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << layaut;
	file.close();

	file.setFileName(pathFolder + "/" + "Script/main.js");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << jsscrypt;
	file.close();

	file.setFileName(pathFolder + "/" + "Script_type.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << runnertypr;
	file.close();

	if(mediacount > 0)
	{
		folder.mkdir(folder.path() + "/" + "Media/");
		for (int i = 0; i < mediacount; ++i)
		{
			auto item = medias[i];
			file.setFileName(pathFolder + "/" + "Media/" + item.first);
			if (!file.open(QIODevice::WriteOnly))
				return;
			//stream.setDevice(&file);
			//stream << item.second;
			file.write(item.second);
			file.close();
		}
	}

    //auto addr = qMakePair(QString("::1"), 799);
   /* auto addr = qMakePair(QString("10.18.11.15"), 799);
	QTCPTalk net;
	net.connectTo(addr);
	//net.send(QString("get"));
	
	auto test_temp = net.read();
	net.send(str_addr);
	QByteArray info_ver = net.read();
	//name_title = net.read(2);
	//QString info_lan = net.read(2);
	QByteArray info_lay = net.read();
	//QString info_css = net.read(2);
	QByteArray info_script_type = net.read();
	QByteArray info_script = net.read();

	QString path = MasterDino::path + "/Apps/";
	QDir folder = path + str_addr + "/";
	folder.mkdir(folder.path());


	QString pathFolder = folder.path();

	QFile file(pathFolder + "/" + "vers.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream stream(&file);
	stream << info_ver;
	file.close();

	folder.mkdir(folder.path() + "/" + "Layaut/");
	folder.mkdir(folder.path() + "/" + "Script/");
	folder.mkdir(folder.path() + "/" + "Lang/");


	file.setFileName(pathFolder + "/" + "Layaut/PC.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << info_lay;
	file.close();

	file.setFileName(pathFolder + "/" + "Script/main.js");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << info_script;
	file.close();

	file.setFileName(pathFolder + "/" + "Script_type.txt");
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	stream.setDevice(&file);
	stream << info_script_type;
	file.close();
	*/
}

void Dino_loader::Update()
{
}

int Dino_loader::get_version(const QString & str_addr)
{
	auto addr = qMakePair(QString("::1"), 799);
	QTCPTalk net;
	net.connectTo(addr);
	net.send(QString("version"));
	net.send(str_addr);
	QByteArray info_ver = net.read();

	return info_ver.toInt();
}
