#pragma once

//#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(MASTERDINO_LIB)
#  define MASTERDINO_EXPORT Q_DECL_EXPORT
# else
#  define MASTERDINO_EXPORT //Q_DECL_IMPORT
# endif
#else
# define MASTERDINO_EXPORT
#endif
