//
// Created by Andry Z on 07.04.2018.
//

#ifndef COMPYB_CM_DINO_CONTENT_INFO_H
#define COMPYB_CM_DINO_CONTENT_INFO_H


#include <QString>

namespace DinoNames {

	struct Dino_content_info {
		QString name_title;
		QString vers;
		QString file_layaut;
		QString file_lang;
		QString type_script;
		QString file_script;
		QString Path_folder;
	};
}


#endif //COMPYB_CM_DINO_CONTENT_INFO_H
