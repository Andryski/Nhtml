#include <gtest/gtest.h>
#include <string>

#include <QCoreApplication>

#include <MasterDino/MasterDino.h>
#include "../Dino_loader.h"

using std::string;

const char *actualValTrue  = "hello gtest";
const char *actualValFalse = "hello world";
const char *expectVal      = "hello gtest";

TEST(Dino_loader, Load) {
	int arg = 0;
	QCoreApplication coreApplication(arg, nullptr);
	coreApplication.setProperty("debugOption", true);
	MasterDino::path = "../../CompyApps";
	Dino_loader dinoLoader;
	dinoLoader.Load("test2/");
	//EXPECT_STREQ(dinoLoader.type_script.toStdString(),"jstype");
	EXPECT_TRUE(dinoLoader.type_script.toStdString() == "js");
	QString file = "0:V\n2:B:setTitle\n3:B:trol house\n:\n77:H\n4:L:Compy_app\n5:E:\n6:B:neb\n:\n:\n7:H\n8:B:tree1\n9:B:tree2\n:\n10:H";
	//EXPECT_TRUE(dinoLoader.file_layaut == file);
}

TEST(StrCompare, CStrNotEqual) {
	EXPECT_STRNE(expectVal, actualValFalse);
}

