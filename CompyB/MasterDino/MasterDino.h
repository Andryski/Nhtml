#pragma once

#include "masterdino_global.h"

#include <QObject>
#include <QVector>
#include <QMap>
#include <QString>
//#include <QSignalSpy>

#include <functional>

#include "../Dino/Dino.h"

/// @author Andry
class MASTERDINO_EXPORT MasterDino : public QObject
{
	Q_OBJECT
public:
	MasterDino(QObject *parent = Q_NULLPTR);
	~MasterDino();
	
	//QSignalSpy spy;

	/// @brief
	/// @param [in] name - name of (now tab) wen load
	void LoadDino(const QString&, QObject *widget);
	void CloseDino(const QString&);

	void ChangedDino(const QString&);
	void UnChangedDino(const QString&);

	void DownloadDino(const QString&);

	static QString path;

	//bool event(QEvent* event);

public slots:
	void slot_gui(std::function<void(void)>);
	QObject* slot_gui_ret(std::function<QObject* (void)> fun);

private:
	void FindAppsFolder();
	QMap<QString, Dino*> sdinos;
	QVector<Dino> dinos;
};
