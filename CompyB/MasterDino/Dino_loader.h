#pragma once

#include <QString>

#include "../NSDNSClient/NSDNSClient.h"
#include "../QTCPTalker/QTCPTalker.h"
#include "Dino_content_info.h"
//using namespace DinoNames;

/**
 * @brief class for load dino
 * load from folder or internet
 * can update dino
 */
class Dino_loader
{
public:
	Dino_loader();
	~Dino_loader();
	
	void Load(const QString &name);
	void DownloadDino(const QString &str_addr);
	void Update();
	int get_version(const QString &);


	//Dino_content_info info;
	QString name_title;
	QString vers;
	QString file_layaut;
	QString file_lang;
	QString type_script;
	QString file_script;
	QString Path_folder;
};

