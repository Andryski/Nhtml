#include "MasterDino.h"

#include <QFile>
#include <QStandardPaths>
#include <QDir>
#include <QTextStream>

#ifdef _MSC_VER
#include <ciso646> // resolve problem with keywords or\and\not
#endif // _MSC_VER

#include "Dino_loader.h"
#include "../Tools/MyQThread.h"
using namespace DinoNames;

MasterDino::MasterDino(QObject *parent) : QObject(parent)
{
	FindAppsFolder();
}

MasterDino::~MasterDino()
{
	foreach (const QString &str, sdinos.keys()) {
			delete sdinos.value(str);
			sdinos.remove(str);
		}
}

void MasterDino::FindAppsFolder(){
	/*
	auto path1 = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
	auto path2 = QStandardPaths::standardLocations(QStandardPaths::AppConfigLocation);
	auto path3 = QStandardPaths::standardLocations(QStandardPaths::CacheLocation);
	auto path33 = QStandardPaths::standardLocations(QStandardPaths::TempLocation);


	auto path5 = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
	auto path6 = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
	auto path66 = QStandardPaths::writableLocation(QStandardPaths::TempLocation);

	qDebug() << path1;
	qDebug() << path2;
	qDebug() << path3;
	qDebug() << path33;

	qDebug() << path5;
	qDebug() << path6;
	qDebug() << path66;
	 */

	auto appDataLocation = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
	qDebug() << "appDataLocation = " << appDataLocation;

	

/// find CompyApps folder
#ifdef Q_OS_ANDROID
	auto path3 = QStandardPaths::standardLocations(QStandardPaths::CacheLocation);
	QDir loc_path(path3[1]);

	loc_path.cd("CompyApps");
    auto debug_str_path = loc_path.path();
    path = loc_path.path();
#else
    auto loc_path = QDir::current();
	qDebug() << "QDir::current = " << loc_path;

	if(!QCoreApplication::instance()->property("debugOption").toBool()){
		path = appDataLocation + "/CompyApps";
	}else {
		qDebug() << "Debug location on.";
		QDir app_path_test(loc_path.path() + "/CompyApps");
		//std::string test1 = app_path_test.path().toStdString();

		while (!app_path_test.exists() and !loc_path.isRoot()) {
			loc_path.cdUp();
			app_path_test = QDir(loc_path.path() + "/CompyApps");
			//test1 = app_path_test.path().toStdString();
		}
		if(loc_path.isRoot()){
			qWarning() << "Debug Option is ON, but local CompyApps folder not found";
			path = appDataLocation + "/CompyApps";
		}
		path = app_path_test.path();
	}
#endif
	qDebug() << "Path with apps = " << path;
	Q_ASSERT(!path.isEmpty());
}

void MasterDino::LoadDino(const QString &name, QObject *widget_me)
{

	MyQThread *thread = new MyQThread("Dino_loader_thread");
	//QThread *thread = new QThread(this);

	Dino *dino = new Dino(thread);
	sdinos[name] = dino;
	
	//connect(dino, &Dino::sig_gui, this, &MasterDino::slot_gui);
	//connect(dino, &Dino::sig_gui_ret, this, &MasterDino::slot_gui_ret);// , Qt::BlockingQueuedConnection);

	connect(dino, &Dino::sig_gui, this, &MasterDino::slot_gui, Qt::BlockingQueuedConnection);
	connect(dino, &Dino::sig_gui_ret, this, &MasterDino::slot_gui_ret, Qt::BlockingQueuedConnection);

	//dino->sig_gui = [=](std::function<void(void)> fun) { this->slot_gui(fun); };

	/*connect(thread, &MyQThread::started, [=]()
	{
	}
	);*/

	connect(thread, &MyQThread::myStarted, [dino, name, widget_me, this]() {

		Dino_loader *dinol = new Dino_loader();
		dinol->Load(name);

		Dino_content_info info;
		info.file_layaut = dinol->file_layaut;
		info.file_script = dinol->file_script;
		info.type_script = dinol->type_script;
		info.vers = dinol->vers;
		info.Path_folder = path + "/Apps/" + name ;
		dino->dino_name = name;

		dino->CallNewMuzzle = std::bind(&MasterDino::LoadDino,this,std::placeholders::_1,std::placeholders::_2);

		dino->SetDinoLoaderInfo(info);
		dino->me = (QWidget *)widget_me;
		dino->LoadDino();

	});

	sdinos.insert(name, dino);
	//dino->moveToThread(thread);
	thread->start();

}

void MasterDino::CloseDino(const QString &)
{

}

void MasterDino::ChangedDino(const QString &)
{
}

void MasterDino::UnChangedDino(const QString &)
{
}

void MasterDino::DownloadDino(const QString &str_addr)
{
	


}

void MasterDino::slot_gui(std::function<void(void)> fun) {
	fun();
}

QObject* MasterDino::slot_gui_ret(std::function<QObject* (void)> fun) {
	return fun();
}

//bool MasterDino::event(QEvent* event) {
	//if (event->type() == QEvent::User)
	/*
	if (event->type() == MyEvent::GetType())//MyEvent::type())
	{
	//char const * deb = typeid(event).name();

	MyEvent* postedEvent = static_cast<MyEvent*>(event);
	//QString str = postedEvent->t();
	//this->setWindowTitle(str);
	//this->setWindowTitle(postedEvent->message());

	return true;
	}
	*/

	//return QCoreApplication::event(event);
//}

QString MasterDino::path;



