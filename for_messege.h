#pragma once


//#include <PB\Light_Element.pb.h>
#include <PB\Layout.pb.h>

#include <string>
#include <map>
using namespace std;

/*
nothtml::Light_Element get_LE(const nothtml::Element &element,const int &size) {
	if (size == 0) {
		assert(true);
		assert(false);
	}
	nothtml::Light_Element light_element;
	light_element.set_type(element);
	light_element.set_size(size);
	return light_element;
}
*/

#ifndef QT_VERSION
template <typename ProtoT>
string convert(const ProtoT &mes) {
	string ret_arr;

	string *std_str = new string;

	//string Mtype = mes.GetTypeName();

	//nothtml::Element type;
	//type = get_type(Mtype);

	//nothtml::Light_Element element;
	//element = get_LE(type, mes.ByteSize());
	//element.SerializeToString(std_str);
	//ret_arr.append(std_str->data(), std_str->length());
	//*std_str = "";

	mes.SerializeToString(std_str);
	ret_arr.append(std_str->data(), std_str->length());
	//*std_str = "";

	delete std_str;
	return ret_arr;
}

/*
nothtml::Element get_type(const string &stype)
{
	map<string, nothtml::Element> light_elemwnts{
		{ "nothtml.Layout", nothtml::Element::LAYOUT },
		{ "nothtml.Button", nothtml::Element::BUTTON },
		{ "nothtml.Label", nothtml::Element::LABEL },
		{ "nothtml.Picture", nothtml::Element::PICTURE },
		{ "nothtml.StyleSheet", nothtml::Element::STYLE },
		{ "nothtml.JavaScript", nothtml::Element::SCRIPT },
		{ "nothtml.style", nothtml::Element::MY_STYLE },
		{ "nothtml.Head", nothtml::Element::HEAD },
		{ "nothtml.Tabel", nothtml::Element::TABLE }
	};

	nothtml::Element elem;
	elem = light_elemwnts.at(stype);
	return elem;
}
*/

#endif // Not QT_VERSION

#ifdef QT_VERSION
template <typename ProtoT>
QByteArray convert(const ProtoT &mes) {
	QByteArray ret_arr;

	std::string *std_str = new std::string;

	std::string std_Mtypr = mes.GetTypeName();
	QString Mtype = QString::fromStdString(std_Mtypr);

	nothtml::Element type;
	type = get_type(Mtype);

	nothtml::Light_Element element;
	element = get_LE(type, mes.ByteSize());
	element.SerializeToString(std_str);
	ret_arr.append(std_str->data(), std_str->length());
	*std_str = "";

	mes.SerializeToString(std_str);
	ret_arr.append(std_str->data(), std_str->length());
	*std_str = "";

	delete std_str;
	return ret_arr;
}

nothtml::Element get_type(const QString &stype)
{
	nothtml::Element elem;
	if (stype.toStdString() == "nothtml.Layout") {
		elem = nothtml::Element::LAYOUT;
	}
	if (stype.toStdString() == "nothtml.Button") {
		elem = nothtml::Element::BUTTON;
	}
	if (stype.toStdString() == "nothtml.Label") {
		elem = nothtml::Element::LABEL;
	}
	if (stype.toStdString() == "nothtml.Picture") {
		elem = nothtml::Element::PICTURE;
	}
	if (stype.toStdString() == "nothtml.StyleSheet") {
		elem = nothtml::Element::STYLE;
	}
	if (stype.toStdString() == "nothtml.JavaScript") {
		elem = nothtml::Element::SCRIPT;
	}
	return elem;
}
#endif // QT_VERSION


