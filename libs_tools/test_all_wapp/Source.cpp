#include "../Win32Project2/Win32Project2.h"

#include <string>
#include <functional>
using namespace std;
//https://github.com/Microsoft/Windows-universal-samples/blob/master/Samples/WebView/js/WinRTComponent/WinRTToastNotificationWrapper.cpp
//https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/07/08/quickstart-sending-a-local-toast-notification-and-handling-activations-from-it-windows-10/
//https://blogs.msdn.microsoft.com/tiles_and_toasts/2015/07/08/toast-notification-and-action-center-overview-for-windows-10/

//https://github.com/microsoft/cppwinrt
//Embracing%20Standard%20C++%20for%20the%20Windows%20Runtime%20-%20Kenny%20Kerr%20and%20James%20McNellis%20-%20CppCon%202016.pdf
//https://cppcon.org/
//https://moderncpp.com/windows-runtime/
//https://kennykerr.ca/
//https://moderncpp.com/

//Windows.?Media.?Playback 
//https://docs.microsoft.com/en-us/windows/uwp/audio-video-camera/system-media-transport-controls
//http://stackoverflow.com/questions/43953779/toast-notifications-windows-10-with-cppwinrt-library


void main() {
	testmain("myapp","test_a");
	auto fun1 = fungen();
	string test1_str_l1 = fun1();
}