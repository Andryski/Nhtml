// Win32Project2.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Win32Project2.h"
#include <functional>

// This is an example of an exported variable
WIN32PROJECT2_API int nWin32Project2=0;

// This is an example of an exported function.
WIN32PROJECT2_API int fnWin32Project2(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see Win32Project2.h for the class definition
CWin32Project2::CWin32Project2()
{
    return;
}


#include <iostream>
using namespace std;

#pragma comment(lib, "windowsapp") 

//#include "winrt/Windows.UI.Core.h"
//#include "winrt/ppl.h"
#include "../winrt/Windows.Data.Xml.Dom.h"
#include "../winrt/Windows.UI.Notifications.h"
#include "../winrt/Windows.UI.Notifications.Management.h"
//#include "winrt/Windows.Foundation.h"
using namespace winrt::Windows::UI::Notifications;
//using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::UI::Notifications::Management;
//using namespace winrt::Windows::Data::Xml;
using namespace winrt;
using winrt::Windows::UI::Notifications::ToastNotification;
using winrt::Windows::UI::Notifications::ToastNotificationManager;
//using winrt::Windows::Data::Xml::Dom::XmlDocument;


#include <locale>
#include <codecvt>
#include <string>

void testmain(std::string appName, std::string messedge1) {
	//setup converter
	//typedef std::codecvt_utf8<wchar_t> convert_type;
	//std::wstring_convert<convert_type, wchar_t> converter;

	//use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
	//return converter.from_bytes(t_str);

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	//std::string narrow = converter.to_bytes(wide_utf16_source_string);
	std::wstring wappName = converter.from_bytes(appName);
	std::wstring wmessedge1 = converter.from_bytes(messedge1);
	Wtestmain(wappName, wmessedge1);

}

void Wtestmain(std::wstring appName, std::wstring messedge1) {

	init_apartment();

	//auto t5 = winrt::Windows::UI::Notifications::Notification();
	//auto t4 = ToastNotificationManager::GetDefault();
	//t4.CreateToastNotifier(L"appid").Show(L"test");

	auto notificationXml = ToastNotificationManager::GetTemplateContent(ToastTemplateType::ToastText04);
	//ToastNotificationManager notificationXml = ToastNotificationManager::ToastNotificationManager();
	//notificationXml.GetTemplateContent(ToastTemplateType::ToastText04);
	//auto t1 = ToastTemplateType::ToastText04;

	auto toeastElement = notificationXml.GetElementsByTagName(L"text");
	//toeastElement.Item(0).AppendChild(notificationXml.CreateTextNode(L"This is Notification Message"));
	toeastElement.Item(0).AppendChild(notificationXml.CreateTextNode(messedge1));
	//toeastElement.Item(1).AppendChild(notificationXml.CreateTextNode(L"This is second line Notification"));
	//toeastElement.Item(2).AppendChild(notificationXml.CreateTextNode(L"This is Third line Notification"));

	auto toastNotification = ToastNotification(notificationXml);
	//ToastNotificationManager::CreateToastNotifier(L"appid").Show(toastNotification);
	ToastNotificationManager::CreateToastNotifier(appName).Show(toastNotification);
	//ToastNotificationManager::CreateToastNotifier().Show(toastNotification);

	// XmlDocument doc;
	// doc.LoadXml(L"<toast...>...</toast>");
	// ToastNotification toast(doc);
	// ToastNotificationManager::CreateToastNotifier(L"appid").Show(toast);

}


std::function<std::string()> fungen() {
	return []() {return std::string("lambda test"); };

}