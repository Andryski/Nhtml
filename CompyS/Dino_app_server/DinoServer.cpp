#include "DinoServer.h"

#include "DinoThread.h"

DinoServer::DinoServer(QObject *parent)
	: QTcpServer(parent)
{
}

bool DinoServer::listen(quint16 port)
{
	return QTcpServer::listen(QHostAddress::Any, port);
}

DinoServer::~DinoServer()
{
}

void DinoServer::incomingConnection(qintptr socketDescriptor)
{
	QString fortune = "test";
	DinoThread *thread = new DinoThread(socketDescriptor, fortune); // , this);
	connect(thread, &DinoThread::finished, thread, &DinoThread::deleteLater);
	thread->start();
}