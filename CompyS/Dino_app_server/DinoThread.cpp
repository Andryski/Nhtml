#include "DinoThread.h"

#include <QtNetwork>

#include "../../CompyB/QTCPTalker/QTCPTalker.h"

DinoThread::DinoThread(int socketDescriptor, const QString &fortune, QObject *parent)
	: QThread(parent), socketDescriptor(socketDescriptor), text(fortune)
{
}

DinoThread::~DinoThread()
{
}

void DinoThread::run()
{
	QTcpSocket tcpSocket;
	if (!tcpSocket.setSocketDescriptor(socketDescriptor)) {
		emit error(tcpSocket.error());
		return;
	}
	QTCPTalk soc;
	soc.setSocet(&tcpSocket);

	QString action = soc.read(1);
	QDir loc_path = QDir::current();
	loc_path.cdUp();
	loc_path.cd("Apps");

	QDir folder = loc_path;

	QByteArray file_layaut, vers, type_script, file_script;

	QString pathFolder = folder.path() + "/" + action + "/";
	QFile file(pathFolder + "Layaut/PC.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QByteArray in = file.readAll();
	file_layaut = in;
	file.close();
	in.clear();

	file.setFileName(pathFolder + "vers.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	in = file.readAll();
	vers = in;
	file.close();
	in.clear();

	file.setFileName(pathFolder + "Script_type.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		//return;
	}
	else {
		in = file.readAll();
		type_script = in;
		file.close();
		in.clear();
		if (type_script == "js")
		{
			file.setFileName(pathFolder + "Script/main.js");
			if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
				//return;
			}
			else {
				in = file.readAll();
				file_script = in;
				file.close();
				in.clear();
			}
		}
	}

	soc.send(vers);
	soc.send(file_layaut);
	soc.send(type_script);
	soc.send(file_script);


	/*
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_4_0);
	out << text;
	tcpSocket.write(block);
	*/

	tcpSocket.disconnectFromHost();
	tcpSocket.waitForDisconnected();
}