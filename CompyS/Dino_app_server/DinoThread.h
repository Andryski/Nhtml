#pragma once

#include <QThread>
#include <QTcpSocket>

class DinoThread : public QThread
{
	Q_OBJECT

public:
	DinoThread(int socketDescriptor, const QString &fortune, QObject *parent = Q_NULLPTR);
	~DinoThread();

	void run() override;

signals:
	void error(QTcpSocket::SocketError socketError);

private:
	int socketDescriptor;
	QString text;
};
