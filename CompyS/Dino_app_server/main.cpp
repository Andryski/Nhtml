#include <QtCore/QCoreApplication>
#include "DinoServer.h"
#include "../QTCPServer/QTCPServer.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	DinoServer server(&a);
	//QTCPServer server(&a);
	if (!server.listen(799)) {
		return -1;
	}
	return a.exec();
}
