#pragma once

#include <QTcpServer>
#include <QStringList>

class DinoServer : public QTcpServer
{
	Q_OBJECT

public:
	DinoServer(QObject *parent = 0);

	bool listen(quint16 port);
	~DinoServer();

protected:
	void incomingConnection(qintptr socketDescriptor) ;

private:
	QStringList clients;
};
