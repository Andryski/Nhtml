#include <QtCore/QCoreApplication>

#include "NSDNS.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	NSDNS dns(&a);
	return a.exec();
}
