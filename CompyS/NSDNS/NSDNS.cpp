#include "NSDNS.h"



#include <functional>

NSDNS::NSDNS(QObject *obj)
{
	table["vk.ru"] = { qMakePair(QString("192.168.88.2"), 765) };
	table["reader.ru"] = qMakePair(QString("192.168.88.2"), 765);
	table["google.com"] = qMakePair(QString("192.168.88.2"), 765);
	serv = new QTCPServer(obj);
	using std::placeholders::_1;
	//serv.fun = std::bind(&NSDNS::resolve, &resolve, _1); ;
	serv->fun = [&](const QString &strAdrr) {return resolve(strAdrr); };
}




NSDNS::~NSDNS()
{
	delete serv;
}

QPair<QString, int> NSDNS::resolve(const QString &strAdrr) {
	return table[strAdrr];
}