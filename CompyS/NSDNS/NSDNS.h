#pragma once

#include <QMap>
#include <QString>
#include <QObject>

#include "../QTCPServer/QTCPServer.h"

class NSDNS
{
public:
	NSDNS(QObject *);

	~NSDNS();
	QPair<QString, int> resolve(const QString &strAdrr);
	QMap<QString, QPair<QString, int>> table;
	QTCPServer *serv;
};

