#pragma once

#include "qtcpserver_global.h"
#include <QObject>

#include <QtNetwork>
#include <QAbstractSocket>
#include <QTcpServer>
#include <QTcpSocket>

#include <functional>

class QTCPSERVER_EXPORT QTCPServer : public QObject
{
	Q_OBJECT
public:
	QTCPServer(QObject *parent = Q_NULLPTR);
	~QTCPServer();
	std::function<QPair<QString, int>(const QString&)> fun;


public slots:
	void newConnection();
private:
	QTcpServer *tcpServer;
};
