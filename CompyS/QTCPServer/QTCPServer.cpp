#include "QTCPServer.h"

#include "../../CompyB/QTCPTalker/QTCPTalker.h"
#include <QThread>

QTCPServer::QTCPServer(QObject *parent ) : QObject(parent)
{
	tcpServer = new QTcpServer(this);
	//tcpServer->connect(&QTcpServer::newConnection, this, &QTCPServer::newConnection);
	connect(tcpServer, &QTcpServer::newConnection, this, &QTCPServer::newConnection);
	if (!tcpServer->listen(QHostAddress::Any, 654)) {
		return;
	}
}

QTCPServer::~QTCPServer()
{
}

void QTCPServer::newConnection() {
	QTcpSocket* clientSocket = tcpServer->nextPendingConnection();
	
	QThread *thread = new QThread(this);
	
	QTCPTalk *nexttalc = new QTCPTalk();//thread
	clientSocket->setParent(nexttalc);
	nexttalc->setSocet(clientSocket);

	//connect(thread, &QThread::started, nexttalc, &draw_engine::thread_draw);
	connect(thread, &QThread::started, nexttalc, [nexttalc]() {
		connect(nexttalc->tcpSocket, &QTcpSocket::readyRead, nexttalc, &QTCPTalk::ReadyRead);
		connect(nexttalc->tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), nexttalc, &QTCPTalk::displayError);
		connect(nexttalc->tcpSocket, &QTcpSocket::connected, nexttalc, &QTCPTalk::onSokConnected);
		connect(nexttalc->tcpSocket, &QTcpSocket::disconnected, nexttalc, &QTCPTalk::onSokDisconnected);
		auto read = nexttalc->read();
		//auto ans = fun(QString(read));
		auto ans = qMakePair(QString("::1"),789);
		nexttalc->send(ans.first);
		nexttalc->send(QString::number(ans.second));
	});

	connect(thread, &QThread::finished, thread, &QThread::deleteLater);

	nexttalc->moveToThread(thread);
	clientSocket->moveToThread(thread);

	thread->start();
}

