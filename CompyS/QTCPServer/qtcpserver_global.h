#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(QTCPSERVER_LIB)
#  define QTCPSERVER_EXPORT Q_DECL_EXPORT
# else
#  define QTCPSERVER_EXPORT Q_DECL_IMPORT
# endif
#else
# define QTCPSERVER_EXPORT
#endif
