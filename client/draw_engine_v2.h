#pragma once

#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
#include <QGridLayout>
#include <QtWidgets>

#include <QMap>
#include <QObject>
#include <QThread>

#include <QRunnable>

#include <QPair>
#include <qvariant.h>

#include <PB/Page.pb.h>

#include <PB/Layout.pb.h>

#include <PB/Head.pb.h>
#include <PB/Tabel.pb.h>
#include <PB\Layout_content.pb.h>
#include <PB\Light_Element.pb.h>

#include <functional>
#include <utility> //pair
#include <string>
using namespace std;

#include "css_style_keep.h"

struct head_struct
{
	QString title;
	QString icon;
};

struct head_and_body_struct
{
	head_struct head_s;
	string body;
};

class draw_engine_v2 : public QObject//, QRunnable
{
	Q_OBJECT
public:
	draw_engine_v2();
	~draw_engine_v2();

	bool debag;

	void drow_v3(const nothtml::Layout_content &messege, QLayout * ParenntLayout);
	void drow_for_v3(const google::protobuf::RepeatedPtrField< ::nothtml::Layout_content > &messege, QLayout * ParenntLayout);
	void init_drow(const string &content, QLayout * ParenntLayout);
	void draw_one(QLayout * ParenntLayout);

	//void draw()

	static pair<pair<QString, QString>,string> pars_head(const QByteArray &messege);


	css_style_keep style_keep;

private:
	bool is_it_thread();

	void init_maps();

	//lambda type
	typedef function< function<QObject*()>(pair<QWidget *, QLayout *> Parent_adr, QString str) > lam_ret;
	typedef function< function<void()>(pair<QWidget *, QLayout *> Parent_adr, QString str) > lam_nr;

	//function < QString(pair<QWidget *, QLayout *>, std::string str) >

	//void func_drow(const QAction &atc);

	

	void func_drow(const function<QObject*()> &tc,const function<void(QWidget *)> &st = nullptr);
	QObject *func_drow_ret(const function<QObject*()> &tc, const function<void(QWidget *)> &st = nullptr);

	// ������ ��� �������� ��������
	QMap < nothtml::Layout_Element, lam_ret> map_Layout;
	// �� ������������?(
	QMap < nothtml::Element, lam_ret> map_elements_ret;
	// ������ ��� �������� ���������
	QMap < nothtml::Element, lam_ret> map_elements;//lam_nr> map_elements_nr;
	// ��������?(
	QMap < nothtml::Element, function < QString(std::string sstr) > > map_messege_pars;

signals:
	void sig_create_element(QEvent *t);
	QObject *sig_create_element_with_ret(QEvent *t);

};

