#pragma once
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
#include <QGraphicsAnchorLayout>
#include <QtWidgets>
#include <QObject>


#include <PB/Page.pb.h>
#include <PB/Light_Element.pb.h>
#include <PB/Button.pb.h>
#include <PB/Layout.pb.h>
#include <PB/Label.pb.h>
#include <PB/Picture.pb.h>
#include <PB/StyleSheet.pb.h>
#include <PB/JavaScript.pb.h>
#include <PB/Head.pb.h>
#include <PB/Tabel.pb.h>

//#include <PB/Light_Element.pb.h>

#include <functional>
using namespace std;

#include "MyEvent.h"


class draw_engine : public QObject
{
	//
	Q_OBJECT
public:
	QString title;

	draw_engine();
	~draw_engine();
	void start(const QByteArray &messege);
	//void start(String messege);

	int draw(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt);

	int new_draw2(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt);

	void set_layout(QBoxLayout * QBLayout) {
		get_QBLayoutParennt = QBLayout;

	}
	
	QMap < nothtml::Element, function < QString(std::string sstr) > > meta_get{
		{ nothtml::Element::BUTTON,
			[=](std::string sstr) {
				QString ret_str;
				nothtml::Button EButton;
				EButton.ParseFromString(sstr);

				ret_str = QString::fromStdString(EButton.text());

				return ret_str;
			}
		},
		{ nothtml::Element::LABEL,
			[=](std::string sstr) {
				QString ret_str;

				nothtml::Label label_PB;
				label_PB.ParseFromString(sstr);

				ret_str = QString::fromStdString(label_PB.text());

				return ret_str;
			}
		}
	};

	QMap<nothtml::Element, function< function<QObject*()>(QBoxLayout *layout, QString str) > > meta_widgets{
		{ nothtml::Element::BUTTON,  
			[=](QBoxLayout *layout, QString str) {
				return [=]() {
					QPushButton *pb = new QPushButton(str);
					layout->addWidget(pb);
					return (QObject *)pb;
				};
			}
		},
		{ nothtml::Element::LABEL,
				[=](QBoxLayout *layout, QString str) {
				return [=]() {
					QLabel *la = new QLabel(str);
					la->setWordWrap(true);
					layout->addWidget(la);
					return (QObject *)la;
				};
			}
		}
	};

	void thread_draw() {
				//QThread::ex
		//QApplication::processEvents();
		
		//MyEvent* myEvent2 = new MyEvent();
		/*myEvent2->t = [&]() {
			QBoxLayout *box_layout = new QVBoxLayout;
			QFrame *fram = new QFrame;
			fram->setLayout(box_layout);
			fram->setFrameShape(QFrame::StyledPanel);
			fram->setFrameShadow(QFrame::Plain);

			get_QBLayoutParennt->addWidget(fram);
			return (QObject *)box_layout;
		};*/
		//QObject *temp_obj = emit sig_create_new_Layout(myEvent2);
		//QBoxLayout *tempw = (QBoxLayout *)temp_obj;
		//QBoxLayout *tempw = (QBoxLayout *)emit sig_create_new_Layout(myEvent2);

		//MyEvent* myEvent = new MyEvent("Hello GUI! This is another thread.");


		/*myEvent->t = [=]() {
			QPushButton *pb = new QPushButton("test lam but");
			tempw->addWidget(pb);
			return (QObject *)pb;
		};*/

		//myEvent->t = meta_widgets.value(nothtml::Element::BUTTON)(tempw);

		//emit sig_create_new_element(myEvent);

		//MyEvent* myEvent3 = new MyEvent();
		//myEvent3->t = meta_widgets.value(nothtml::Element::LABEL)(tempw);
		//emit sig_create_new_element(myEvent3);
		
		new_draw2(get_messege, get_num, get_QBLayoutParennt);
		QThread *thread = QThread::currentThread();
		thread->quit();

	}
	void set_messege(const std::string &messege) {
		get_messege = messege;
	}
	void set_num(const int &num) {
		get_num = num;
	}

	void set_get(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt) {
		get_messege = messege;
		get_num = num;
		get_QBLayoutParennt = QBLayoutParennt;
	};

	QObject *parent;

public slots:
void draw2(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt) {
	draw(messege, num, QBLayoutParennt);
};
void draw3() {
	draw(get_messege, get_num, get_QBLayoutParennt);
};


signals:
	void sig_create_new_element(QEvent *t);
	QObject *sig_create_new_Layout(QEvent *t);

private:
	QString incom_messege;
	std::string get_messege;
	int get_num;
	QBoxLayout * get_QBLayoutParennt;
};


