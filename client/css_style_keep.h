#pragma once

#include <map>
#include <string>
#include <vector>
using namespace std;

#include <QtWidgets>

#include "keep_style.h"
#include <PB\CSS_style.pb.h>


class css_style_keep
{
public:
	css_style_keep();
	~css_style_keep();
	void SetCssKeep(nothtml::CSS_Colection colection);
	void SetCssKeep_array(const QByteArray & messege);
	void AplayCSS(int id, QWidget *obj);
private:
	map<int, string> id_to_name;
	//id to id element
	map<int, vector<int>> id_map;
	map<int, keep_style> id_element;
};

