#include "css_style_keep.h"



css_style_keep::css_style_keep()
{
}


css_style_keep::~css_style_keep()
{
}

void css_style_keep::SetCssKeep(nothtml::CSS_Colection colection)
{
	int n = colection.rule_size();
	for (size_t i = 0; i < n; i++)
	{
		nothtml::Rule node;
		node = colection.rule(i);
		keep_style temp(node);
		id_element[node.id_rule()] = temp;
	}
	n = colection.rules_size();
	for (size_t i = 0; i < n; i++)
	{
		nothtml::Rules node;
		node = colection.rules(i);

		vector<int> temp;
		for (size_t j = 0; j < node.rule_id_size(); j++)
		{
			//temp.push_back(node.rule_id[j]);
			temp.push_back(node.rule_id(j));
		}

		id_map[node.id_rules()] = temp;
	}
}


void css_style_keep::SetCssKeep_array(const QByteArray &css_messege)
{
	nothtml::CSS_Colection temp;
	string str_css_messege(css_messege.toStdString());
	temp.ParseFromString(str_css_messege);
	SetCssKeep(temp);
}

void css_style_keep::AplayCSS(int id, QWidget * obj)
{
	vector<int> ids = id_map[id];
	for each (auto i in ids)
	{
		keep_style temp = id_element[i];
		temp.apply(obj);
	}
}
