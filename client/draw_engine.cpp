#include "draw_engine.h"

#include "mainwindow.h"

#include <QGraphicsAnchorLayout>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsWidget>

#include "Messege_geter.h"

draw_engine::draw_engine()
{
}


draw_engine::~draw_engine()
{
}

void draw_engine::start(const QByteArray &messege)
{
	//incom_messege = messege;

	const int fixed_size_page = 10;
	const int fixed_size_page_v2 = 7;
	//const int fixed_size_page = 7;
	//std::string all_messege(incom_messege.toStdString());
	std::string all_messege(messege.toStdString());
	int lenst = all_messege.size();

	/// brief      parsing header
	std::string page1_messege = all_messege.substr(0, fixed_size_page_v2);//fixed_size_page);
	nothtml::Page page1;
	page1.ParseFromString(page1_messege);
	//int nsd1 = page1.ByteSize();
	//int temp_n = page1.num();
	if (page1.vers() != 2) {
		exit(2);
	}
	else {
		Messege_geter mes_get(all_messege.substr(fixed_size_page_v2, lenst - fixed_size_page_v2));
		if (page1.head()) {
			//Messege_geter mes_get(all_messege.substr(fixed_size_page_v2, lenst - fixed_size_page));
			std::pair<std::string, nothtml::Element> me = mes_get.get_info();
			if(me.second != nothtml::Element::HEAD) exit(3);
			nothtml::Head head1;
			//head1.ParseFromString(mes_get.get_messege());
			head1.ParseFromString(me.first);
			title = QString::fromStdString(head1.title());
			get_messege = mes_get.get_last();
		}
		else {
			get_messege = all_messege.substr(fixed_size_page_v2, lenst - fixed_size_page_v2);//all_messege;
		}
	}

	get_num = 1;//temp_n;
	//get_messege = all_messege.substr(fixed_size_page -, lenst - fixed_size_page);//all_messege;


}

int draw_engine::new_draw2(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt) {
	/*int point = 0;
	for (int i = 1; i <= num; ++i) {
		std::string LE_messege = messege.substr(point, 7);
		nothtml::Light_Element lightE;
		lightE.ParseFromString(LE_messege);
		point += lightE.ByteSize();

		int size = lightE.size();
		nothtml::Element element = lightE.type();
		std::string messege_chil = messege.substr(point, size);


		if (element == nothtml::Element::ERROR_READ_PB) {
			assert(true);
			assert(false);
			break;
		}
		if (element == nothtml::Element::TABLE) {
			nothtml::Tabel tabel1;
			tabel1.ParseFromString(messege_chil);

			MyEvent* myEvent = new MyEvent();

			myEvent->t = [&]() {
				QWidget *gridLayoutWidget = new QWidget();
				QGridLayout *gridLayout_1 = new QGridLayout(gridLayoutWidget);


				//QPushButton *pushButton_5 = new QPushButton(gridLayoutWidget);
				//pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

				//gridLayout_1->addWidget(pushButton_5, 0, 4, 2, 1);


				QBLayoutParennt->addWidget(gridLayoutWidget);
				return (QObject *)gridLayoutWidget;
			};

			QWidget *qbj_tab = (QWidget *)emit sig_create_new_Layout(myEvent);

			

			for (size_t j = 0; j < tabel1.elements_size(); j++)
			{
				nothtml::Tabel_Tabel_elem telem = tabel1.elements(j);
			
				myEvent->t = [&]() {
					QWidget *gridCellWidget = new QWidget();
					QBoxLayout *box_layout2 = new QHBoxLayout(gridCellWidget);

					QBLayoutParennt->addWidget(gridCellWidget);
					return (QObject *)gridCellWidget;
				};

				QWidget *qbj_cell = (QWidget *)emit sig_create_new_Layout(myEvent);

				
				draw_engine tabl_gr_en;
				tabl_gr_en.parent = parent;
				string temp_str = telem.obj();
				tabl_gr_en.new_draw2(temp_str, 1, (QBoxLayout *)(QLayout *)qbj_cell->layout());


				myEvent->t = [&]() {
					QWidget *gridCellWidget = new QWidget();
					((QGridLayout *)qbj_tab->layout())->addWidget(qbj_cell, telem.x() + 1, telem.y() + 1, telem.x_spawn() + 1, telem.y_spawn() + 1);
					return (QObject *)gridCellWidget;
				};

				//((QGridLayout *)qbj_tab->layout())->addWidget(qbj_cell, telem.x()+1, telem.y()+1, telem.x_spawn()+1, telem.y_spawn() + 1);
				emit sig_create_new_Layout(myEvent);


			}
			
		}else
		if (element == nothtml::Element::LAYOUT) {

			nothtml::Layout layout;
			layout.ParseFromString(messege_chil);

			if (layout.type() == nothtml::Layout_Element::ERROR_READ_PB_LE) {
				assert(true);
				assert(false);
			}
			if (layout.type() == nothtml::Layout_Element::META_L) {
				int temp_point = 0;
				temp_point += new_draw2(messege.substr(point + size, messege.size() - point - size), layout.num(), QBLayoutParennt);
				point += temp_point;
				if (temp_point != layout.next_le()) {
				//	assert(true);
				//	assert(false);
				}
				point += size;
				//break;
				continue;
			}

			MyEvent* myEvent = new MyEvent();

			QBoxLayout *box_layout;
			if (layout.type() == nothtml::Layout_Element::Horizontal) {
				//box_layout = new QHBoxLayout;
				myEvent->t = [&]() {
					QBoxLayout *box_layout2 = new QHBoxLayout;
					QFrame *fram = new QFrame;
					fram->setLayout(box_layout2);
					fram->setFrameShape(QFrame::StyledPanel);
					fram->setFrameShadow(QFrame::Plain);

					QBLayoutParennt->addWidget(fram);
					//QBLayoutParennt->addLayout(box_layout2);
					return (QObject *)box_layout2;
				};
				box_layout = (QBoxLayout *)emit sig_create_new_Layout(myEvent);
			}else
				if (layout.type() == nothtml::Layout_Element::Vertical) {
					//box_layout = new QVBoxLayout;
					myEvent->t = [=](int numidtread){
						return [=]() {
						int tread_num = numidtread;
						int a = tread_num;
						QBoxLayout *box_layout2 = new QVBoxLayout;
						QFrame *fram = new QFrame;
						fram->setLayout(box_layout2);
						fram->setFrameShape(QFrame::StyledPanel);
						fram->setFrameShadow(QFrame::Plain);

						QBLayoutParennt->addWidget(fram);
						//QBLayoutParennt->addLayout(box_layout2);
						return (QObject *)box_layout2;
						};
				}((int)QThread::currentThreadId());
				myEvent->tread_num = (int)QThread::currentThreadId();
				box_layout = (QBoxLayout *)emit sig_create_new_Layout(myEvent);
			}


			
			/*myEvent->t = [&]() {
				QFrame *fram = new QFrame;
				fram->setLayout(box_layout);
				fram->setFrameShape(QFrame::StyledPanel);
				fram->setFrameShadow(QFrame::Plain);

				QBLayoutParennt->addWidget(fram);
				return (QObject *)box_layout;
			};*/
			//QBoxLayout *tempw = (QBoxLayout *)emit sig_create_new_Layout(myEvent);
			//QBoxLayout *tempw = (QBoxLayout *)emit sig_create_new_Layout(myEvent);

	/*/
			
			int temp_point = 0;
			//temp_point += new_draw2(messege.substr(point + size, messege.size() - point - size), layout.num(), box_layout);
			//point += temp_point;

			
			
			QThread *thread = new QThread();
			draw_engine *dr_en = new draw_engine();
			dr_en->set_messege(messege.substr(point + size, layout.next_le()));
			dr_en->set_layout(box_layout);
			dr_en->set_num(layout.num());
			dr_en->parent = parent;
			dr_en->moveToThread(thread);
			dr_en->thread_draw();
			/*
			connect(thread, &QThread::started, dr_en, &draw_engine::thread_draw);

			connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::DirectConnection);

			connect(dr_en, &draw_engine::sig_create_new_element, (MainWindow *)parent, &MainWindow::create_new_element, \
				Qt::QueuedConnection);

			connect(dr_en, &draw_engine::sig_create_new_Layout, (MainWindow *)parent, &MainWindow::crete_new_Layout, \
				Qt::BlockingQueuedConnection);

			thread->start();
			*
			
			//if (temp_point != layout.next_le()) {
				//assert(true);
			//	assert(false);
			//}
//			last_widget = fram;
			point += layout.next_le();

		}
		else {
			//auto fun = meta_get.value(element);
			//QMap<QString, int>::const_iterator
			auto i = meta_get.find(element);
//			while (i != meta_get.end() && i.key() == element) { //XDDD
			if(i != meta_get.end() && i.key() == element) {
				QString str = i.value()(messege_chil);
				MyEvent* myEvent = new MyEvent();
				myEvent->t = meta_widgets.value(element)(QBLayoutParennt, str);
				emit sig_create_new_element(myEvent);
			}	
		}

		

		point += size;
	}
	return point;
	*/
}

/**
* @brief      parsing and draw on window
* @details    [long description]
*
* @param[in]  messege          The messege
* @param[in]  num              The number of elements
* @param      QBLayoutParennt  The layout parent
*
*/
int draw_engine::draw(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt) {
	int point = 0;

	QWidget *last_widget;
	last_widget = nullptr;
	for (int i = 1; i <= num; ++i) {
		std::string LE_messege = messege.substr(point, 7); //history XD messege.substr(0, 4);
		nothtml::Light_Element lightE;
		lightE.ParseFromString(LE_messege);
		point += lightE.ByteSize();

		int size = lightE.size();
		nothtml::Element element = lightE.type();
		std::string messege_chil = messege.substr(point, size);


		switch (element) {
		case nothtml::Element::ERROR_READ_PB: {
			assert(true);
			assert(false);
			break;
		}
		case nothtml::Element::BUTTON: {
			nothtml::Button EButton;
			EButton.ParseFromString(messege_chil);
			nothtml::Rect Brect = EButton.position();


			QPushButton *QButton;
			QButton = new QPushButton;
			QButton->setObjectName(QString::fromStdString(EButton.text()));
			QButton->setText(QString::fromStdString(EButton.text()));

			QBLayoutParennt->addWidget(QButton);

			int x, y, w, h;
			x = Brect.x();
			y = Brect.y();
			w = Brect.width();
			h = Brect.height();
			QButton->setGeometry(x, y, w, h);
			QButton->show();
			last_widget = QButton;
			break;
		}
		case nothtml::Element::LABEL: {
			nothtml::Label label_PB;
			label_PB.ParseFromString(messege_chil);

			QLabel *labe_e = new QLabel;
			labe_e->setText(QString::fromStdString(label_PB.text()));
			labe_e->setWordWrap(true);
			QBLayoutParennt->addWidget(labe_e);
			last_widget = labe_e;
			break;
		}
		case nothtml::Element::PICTURE: {
			nothtml::Picture picture_pb;
			picture_pb.ParseFromString(messege_chil);

			//QImage picture_file = StrToImeg(QString::fromStdString(picture_pb.pict_array()));

			QString path = QString::fromStdString(picture_pb.pict_array());
			QImage picture_file(path);

			QImage picture_file_final = picture_file;// .scaled(200, 200, Qt::IgnoreAspectRatio);

			QLabel *picture_elem = new QLabel;
			picture_elem->setPixmap(QPixmap::fromImage(picture_file_final));
			QBLayoutParennt->addWidget(picture_elem);

			last_widget = picture_elem;
			break;
		}
		case nothtml::Element::LAYOUT: {

			
			nothtml::Layout layout;
			layout.ParseFromString(messege_chil);

			if (layout.type() == nothtml::Layout_Element::ERROR_READ_PB_LE) {
				assert(true);
				assert(false);
			}
			if (layout.type() == nothtml::Layout_Element::META_L) {
				point += draw(messege.substr(point + size, messege.size() - point - size), layout.num(), QBLayoutParennt);
				break;
			}
			QBoxLayout *box_layout;
			if (layout.type() == nothtml::Layout_Element::Horizontal) {
				box_layout = new QHBoxLayout;
				//point += draw(messege.substr(point + size, messege.size() - point - size), layout.num(), box_layout);
				//break;
			}
			if (layout.type() == nothtml::Layout_Element::Vertical) {
				box_layout = new QVBoxLayout;
			}
			
			QFrame *fram = new QFrame;
			fram->setLayout(box_layout);
			fram->setFrameShape(QFrame::StyledPanel);
			fram->setFrameShadow(QFrame::Plain);

			QBLayoutParennt->addWidget(fram);
			//QBLayoutParennt->addLayout(box_layout);

			point += draw(messege.substr(point + size, messege.size() - point - size), layout.num(), box_layout);
			last_widget = fram;
			break;
		}
		case nothtml::Element::SCRIPT: {
			nothtml::JavaScript jvscipt1;
			QString jv_script;
			jvscipt1.ParseFromString(messege_chil);

			jv_script = QString::fromStdString(jvscipt1.script());
			/*
			QScriptValue script_val = engine.newQObject(last_widget);
			//engine.globalObject().setProperty(last_widget->objectName(), script_val);
			engine.globalObject().setProperty("button", script_val);

			QScriptValue Code = engine.evaluate(jv_script);// , file2.fileName());

			QScriptValue ctor = engine.evaluate("Calculator"); //"event1");

			QScriptValue result = ctor.construct();

			if (result.isError())
				qDebug()
				<< "Uncaught exception at line"
				<< result.property("lineNumber").toInt32()
				<< ":" << result.toString();

			if (result.isError()) {
				QMessageBox::critical(0, "Hello Script",
					QString::fromLatin1("%0:%1: %2")
					.arg(last_widget->objectName())
					.arg(result.property("lineNumber").toInt32())
					.arg(result.toString()));
			}
			*/

		}
		case nothtml::Element::STYLE: {
			nothtml::StyleSheet style1;
			QString css_style;
			style1.ParseFromString(messege_chil);
			std::string debugw = style1.sheet();
			//const char* debugw2 = debugw.c_str();
			std::string debug;
			//debug = "background-color: #0f0";//style1.sheet();
			debug = debugw;
			css_style = QString::fromStdString(debug);

			try {
				if (last_widget != NULL)
					last_widget->setStyleSheet(css_style);
			}
//			catch (char *e) {
				//printf("Exception Caught: %s\n", e);
	//		}
			catch (...) {

			}

		}
		}
		point += size;
	}
	return point;
}