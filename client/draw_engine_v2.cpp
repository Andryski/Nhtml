#include "draw_engine_v2.h"

#include <QSpacerItem>



draw_engine_v2::draw_engine_v2()
{
	init_maps();
}


draw_engine_v2::~draw_engine_v2()
{
}

bool draw_engine_v2::is_it_thread()
{
	return QThread::currentThread() != QApplication::instance()->thread();
}

void draw_engine_v2::init_maps()
{
	map_messege_pars.insert(nothtml::Element::LABEL,
		[=](std::string sstr) {
			QString ret_str;

			//nothtml::Label label_PB;
			//label_PB.ParseFromString(sstr);
			//ret_str = QString::fromStdString(label_PB.text());

			ret_str = QString::fromStdString(sstr);
			return ret_str;
			//return sstr;
		}
	);

	map_messege_pars.insert(nothtml::Element::BUTTON,
		[=](std::string sstr) {
			QString ret_str;

			//nothtml::Button EButton;
			//EButton.ParseFromString(sstr);
			//ret_str = QString::fromStdString(EButton.text());

			return ret_str;
		}
	);

	map_elements.insert(nothtml::Element::LABEL,
		[=](pair<QWidget *, QLayout *> Parent_adr, QString str) {
			QLayout *layout = Parent_adr.second;
			QWidget *WidgetP = Parent_adr.first;
			if (WidgetP == nullptr) WidgetP = new QWidget;
			return [=]() {
				QLabel *la = new QLabel(str);//, WidgetP);
				la->setWordWrap(true);
				//layout->addWidget(la);
				Parent_adr.second->addWidget(la);
				return (QObject *)la;
			};
		}	
	);

	map_elements.insert(nothtml::Element::BUTTON,
		[=](pair<QWidget *, QLayout *> Parent_adr, QString str) {
			QLayout *layout = Parent_adr.second;
			QWidget *WidgetP = Parent_adr.first;
			if (WidgetP == nullptr) WidgetP = new QWidget;
			return [=]() {
				QPushButton *pb = new QPushButton(str, WidgetP);
				layout->addWidget(pb);
				return (QObject *)pb;
			};
		}
	);
	debag = true;
	if (debag) {
		map_Layout.insert(nothtml::Layout_Element::Horizontal,
			[=](pair<QWidget *, QLayout *> Parent_adr, QString str) {
			QLayout *layout = Parent_adr.second;
			QWidget *WidgetP = Parent_adr.first;
			if (WidgetP == nullptr) WidgetP = new QWidget;
			return [=]() {
				
				QBoxLayout *box_layout = new QHBoxLayout;
				QFrame *fram = new QFrame;
				fram->setFrameShape(QFrame::StyledPanel);
				fram->setFrameShadow(QFrame::Plain);
				fram->setLayout(box_layout);
				layout->addWidget(fram);

				return (QObject *)fram;
			};
		}
		);

		map_Layout.insert(nothtml::Layout_Element::Vertical,
			[=](pair<QWidget *, QLayout *> Parent_adr, QString str) {
			QLayout *layout = Parent_adr.second;
			QWidget *WidgetP = Parent_adr.first;
			if (WidgetP == nullptr) WidgetP = new QWidget;
			return [=]() {
				
				QBoxLayout *box_layout = new QVBoxLayout;
				QFrame *fram = new QFrame;
				fram->setFrameShape(QFrame::StyledPanel);
				fram->setFrameShadow(QFrame::Plain);
				fram->setLayout(box_layout);
				layout->addWidget(fram);

				return (QObject *)fram;
			};
		}
		);
	}


	map_elements.insert(nothtml::Element::VSPASER,
		[=](pair<QWidget *, QLayout *> Parent_adr, QString str) {
		QLayout *layout = Parent_adr.second;
		QWidget *WidgetP = Parent_adr.first;
		if (WidgetP == nullptr) WidgetP = new QWidget;
		return [=]() {
			QSpacerItem *spase = new QSpacerItem(10,10);
			layout->addItem(spase);
			return (QObject *)spase;
		};
	}
	);

	//QPair *test;
}


void draw_engine_v2::drow_v3(const nothtml::Layout_content & messege, QLayout * ParenntLayout)
{
	nothtml::Element element = messege.type_string();

	if (element == nothtml::Element::TABLE) {
		nothtml::Tabel tabel1;
		tabel1.ParseFromString(messege.string());

		QWidget *gridLayoutWidget = new QWidget;
		QGridLayout *gridLayout_1 = new QGridLayout(gridLayoutWidget);



		int i_size = tabel1.elements_size();
		for (size_t j = 0; j < i_size; j++)
		{
			nothtml::Tabel_elem telem = tabel1.elements(j);

			QLayout *cell = (QLayout *)new QBoxLayout(QBoxLayout::Direction::BottomToTop, gridLayoutWidget);

			nothtml::Layout_content nodes = telem.obj();

			drow_v3(nodes, cell);

			gridLayout_1->addLayout(cell, telem.x(), telem.y(), telem.x_spawn() + 1, telem.y_spawn() + 1);
		}

		ParenntLayout->addWidget(gridLayoutWidget);

	}
	else
		if (element == nothtml::Element::LAYOUT) {

			nothtml::Layout layout;
			layout.ParseFromString(messege.string());

			if (layout.type_layout() == nothtml::Layout_Element::ERROR_READ_PB_LE) {
				assert(true);
				assert(false);
			}
			if (layout.type_layout() == nothtml::Layout_Element::META_L) {

				drow_for_v3(layout.messege(), ParenntLayout);
			}
			else
			{
				QBoxLayout *box_layout;
				QWidget *test;
				auto temp = map_Layout.value(layout.type_layout());

				//box_layout 
				test = (QWidget *)func_drow_ret(temp(make_pair(nullptr, ParenntLayout), ""));
				drow_for_v3(layout.messege(), test->layout());
			}

		}
		else {
			auto i = map_messege_pars.find(element);
			if (i != map_messege_pars.end() && i.key() == element) {

				QString str = i.value()(messege.string());

				func_drow(map_elements.value(element)(make_pair(nullptr, ParenntLayout), str));

			}
		}

}

void draw_engine_v2::drow_for_v3(const google::protobuf::RepeatedPtrField<::nothtml::Layout_content>& messeges, QLayout * ParenntLayout)
{
	int n = messeges.size();
	for (size_t i = 0; i < n; i++)
	{
		nothtml::Layout_content node;
		node = messeges.Get(i);
		drow_v3(node, ParenntLayout);
	}
}

void draw_engine_v2::init_drow(const string & content, QLayout * ParenntLayout)
{
	string LE_messege = content.substr(0, 5);
	//nothtml::Light_Element lightE;
	nothtml::Size_Element esize;
	esize.ParseFromString(LE_messege);

	string messege_child_node = content.substr(5, esize.size());

	nothtml::Layout node;
	node.ParseFromString(messege_child_node);
	drow_v3(node.messege(0), ParenntLayout);
}

pair<pair<QString, QString>, string> draw_engine_v2::pars_head(const QByteArray & messege2)
{
	const int fixed_size_page_v2 = 7;
	string all_messege(messege2.toStdString());
	int lenst = all_messege.size();

	string page1_messege = all_messege.substr(0, fixed_size_page_v2);
	nothtml::Page page1;
	page1.ParseFromString(page1_messege);


	string get_messege;
	QString title;
	QString path_icon;
	if (page1.vers() != 3) {
		throw page1.vers();
	}
	else {
		
		if (page1.head()) {
			
			string LE_messege = all_messege.substr(fixed_size_page_v2, 5);
			//nothtml::Light_Element lightE;
			nothtml::Size_Element esize;
			esize.ParseFromString(LE_messege);
			int point = esize.ByteSize();

			int size = esize.size();
			string messege_chil = all_messege.substr(point, size);

			nothtml::Head head1;
			head1.ParseFromString(messege_chil);

			title = QString::fromStdString(head1.title());
			path_icon = QString::fromStdString(head1.ico());
			get_messege = all_messege.substr(fixed_size_page_v2 + 5 + size, all_messege.size() - fixed_size_page_v2 + 4 + size - size);
		}
		else {
			get_messege = all_messege.substr(fixed_size_page_v2, lenst - fixed_size_page_v2);
		}
	}

	return make_pair(make_pair(title, path_icon), get_messege);
}

void draw_engine_v2::func_drow(const function<QObject*()> &tc, const function<void(QWidget *)> &st)
{
	if (is_it_thread()) {
		
	}
	else {
		if (st == nullptr) {
			tc();
		}
	}
}

QObject * draw_engine_v2::func_drow_ret(const function<QObject*()> &tc, const function<void(QWidget *)> &st)
{
	if (is_it_thread()) {
		
	}
	else {
		if (st == nullptr) {
			return tc();
		}
	}
	return nullptr;
}
