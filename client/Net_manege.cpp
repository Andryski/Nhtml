
#include "Net_manege.h"


/**
* @brief      обработка soket error
*
* @param[in]  socketError  The socket error
*/
void Net_manege::displayError(QAbstractSocket::SocketError socketError)
{
	if (socketError == QTcpSocket::RemoteHostClosedError)
		return;

	QMessageBox::information(0, "Network error",
		QString::fromLatin1("The following error occurred: %1.")
		.arg(tcpClient.errorString()));
}

/**
* @brief      recive messege
* todo: pars not full recive massege
*  meby see old commit
*/
//void MainWindow::
void Net_manege::startRecive() {
	incominngMessege += tcpClient.readAll();
}

void Net_manege::conect_to_server(QString adres) {
	//tcpClient.connect( SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(displayError(QAbstractSocket::SocketError)));
	//connect(&tcpClient, SIGNAL(error(QAbstractSocket::SocketError)), displayError(QAbstractSocket::SocketError));
	//connect(&tcpClient, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error), this, &MainWindow::onError);
	connect(&tcpClient, static_cast<void (QTcpSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error), this, &Net_manege::displayError);
	//connect(&tcpClient, &QTcpSocket::connected, this, &MainWindow::startRecive2);
	connect(&tcpClient, &QTcpSocket::readyRead, this, &Net_manege::startRecive);

	QString addr = adres;

	QStringList slist = addr.split(":");

	int port = slist[1].toInt();

	tcpClient.connectToHost(slist[0], port);

	//tcpClient.connect(&QTcpSocket::disconnected, this, &MainWindow::disconnect_tcp);


	//this->ui->ContentWidget->close();
	incominngMessege.clear();

	QObject::connect(&this->tcpClient, SIGNAL(disconnected()), this, SLOT(disconnect_tcp()));

	//error() const : SocketError
	//error(QAbstractSocket::SocketError)
	//connect(tcpClient, &QAbstractSocket::disconnected, this, &MainWindow::disconnect_tcp);
	//connect(tcpClient, static_cast<void (QTcpSocket::disconnected)()>(&QAbstractSocket::disconnected), this, &MainWindow::disconnect_tcp);
	//tcpClient.connect(tcpClient,&QTcpSocket::disconnected, &disconnect_tcp2);
}

void Net_manege::disconnect_tcp() {

};