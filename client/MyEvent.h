#pragma once
//http://stackoverflow.com/questions/2248009/qt-defining-a-custom-event-type
//http://www.doc.crossplatform.ru/qt/4.6.x/threads-qobject.html#signals-and-slots-across-threads

#include <QEvent>
#include <QWidget>

#include <functional>
using namespace std;

class MyEvent : public QEvent
{

public:

	//function<QString()> t;
	function<QObject*()> t;

	bool isset;
	QWidget* wiob;
	int tread_num;

	MyEvent() :
		MyEvent{ QStringLiteral("") }
	{
		/* noop */
	}

	explicit MyEvent(const QString & message) :
		QEvent{ GetType() },
		_message{ message },
		isset{ false }
	{
		/* noop */
	}


	~MyEvent() override
	{
		/* noop */
	}

	QString const & message() const { return _message; }

	static auto GetType() -> Type
	{
		static int generated_type = registerEventType();

		return static_cast<Type>(generated_type);
	}

private:

	QString _message;

};

