#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qboxlayout.h>
//#include <qwidget.h>
//#include <QWidget>
//#include <QJSEngine>
//#include <QtScript>
#include <QMessageBox>

#include "../draw_engine.h"
#include "Net_manege.h"

#include <QSharedPointer>
#include <functional>
using namespace std;

#include<QEvent>
#include "MyEvent.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

	void Clean(QLayout &oL);
    //quint8 lenmes;

	void init_draw(const QByteArray &messege);

	void select_langu(QString);

public slots:
	void create_new_element(QEvent *t);
	QObject * crete_new_Layout(QEvent *t);
	void on_tabWidget_tabCloseRequested(int index);

protected:
	bool event(QEvent* event);

private slots:
    void on_server_Button_clicked();
	void on_Button_test_clicked();
    void on_file_button_clicked();


signals:
	void startDraw(const std::string &messege, const int &num, QBoxLayout * QBLayoutParennt);

private:
    Ui::MainWindow *ui;
	QActionGroup *langGroup;
	QActionGroup *layoGroup;
};

#endif // MAINWINDOW_H


/*
replays windows title bar
https://forum.qt.io/topic/26108/customize-window-frame
https://ru.stackoverflow.com/questions/443825/c-qt-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2%D0%BD%D0%B5%D1%88%D0%BD%D0%B5%D0%B3%D0%BE-%D0%B2%D0%B8%D0%B4%D0%B0-%D0%B7%D0%B0%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BA%D0%B0-%D0%BE%D0%BA%D0%BD%D0%B0
http://www.ilnurgi1.ru/docs/python/modules_user/pyqt/qtgui/qwidget.html
http://stackoverflow.com/questions/26824226/qt-how-do-you-add-to-the-windows-title-bar
http://itnotesblog.ru/note.php?id=254
http://www.qtcentre.org/threads/55802-how-to-draw-custom-titlebar-for-QMainWindow
https://habrahabr.ru/post/116752/
https://msdn.microsoft.com/en-us/library/windows/desktop/bb688195(v=vs.85).aspx
https://www.codeproject.com/Articles/415102/WinForms-Form-Skin
*/