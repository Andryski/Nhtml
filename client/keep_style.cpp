#include "keep_style.h"



keep_style::keep_style(nothtml::Rule &rule)
{
	if (rule.type_string() == nothtml::enum_css::String) {
		str = rule.string();
	}
} 


keep_style::~keep_style()
{
}

void keep_style::apply(QWidget *obj)
{
	if (tr_font) {
		obj->setFont(*font);
	}
	if (tr_palette) {
		obj->setPalette(*palette);
	}
	if (tr_region) {
		obj->setMask(*region);
	}
	if (str != "") {
		obj->setStyleSheet(QString::fromStdString(str));
	}
}
