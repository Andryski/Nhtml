#pragma once

#include <QObject>
#include <QJSEngine>
#include <functional>

class JScript
{
public:

	JScript()// QObject *temp
	{
		// engine.globalObject().setProperty("Dino", engine.newQObject(temp));
	}

	void evaluate_script(QString script) {
		QJSValue fun = engine.evaluate(script);
		QJSValue value = fun.call();
	}

	~JScript()
	{
	}
	QJSEngine engine;
};


//#define DebugScript 1


//QJSEngine engine;

//#define DebugScript 

#ifdef DebugScript
QScriptEngineDebugger debugger;
debugger.attachTo(&engine);
debugger.action(QScriptEngineDebugger::InterruptAction)->trigger();
QMainWindow *debugWindow = debugger.standardWindow();
#endif

/*
QPushButton *button = w.findChild<QPushButton *>("main_button");
//QScriptValue
QJSValue scriptButton = engine.newQObject(button);
engine.globalObject().setProperty("button", scriptButton);


QFile file2(":/Code2.js");
file2.open(QIODevice::ReadOnly);
QJSValue result = engine.evaluate(file2.readAll(),file2.fileName());
file2.close();


//QScriptValue ctor = engine.evaluate("Calculator");
//result.
//QScriptValue scriptUi = engine.newQObject(button);
QJSValue ctor = engine.evaluate("Calculator");
//QScriptValue ctor;
//QScriptValue ctor2 = ctor.call();
QJSValue calc = ctor.callAsConstructor();
//QScriptValue scriptUi = engine.newQObject(w.centralWidget(), QScriptEngine::ScriptOwnership);
//QScriptValue calc = ctor.construct(QScriptValueList() << scriptUi);

if (ctor.isError()) {
QMessageBox::critical(0, "Hello Script",
QString::fromLatin1("%0:%1: %2")
.arg(file2.fileName())
.arg(ctor.property("lineNumber").toInt())
.arg(ctor.toString()));
}


*/
