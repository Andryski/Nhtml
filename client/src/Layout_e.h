#pragma once

#include <map>
#include <string>
using namespace std;

#include "src\Layout_Holder.h"

class Layout_e
{
public:
	Layout_e();
	~Layout_e();

	void drow(const string & content);
	void set_locale(const string & content);

	void update_set_str(size_t, const string &str);
private:
	// id_l(element) to pointer 
	map<size_t, Layout_Holder *> _id_l_to_point;

	// id_l(element) to name
	map<size_t, string> _id_l_to_name;

	// for locale
	// string to id_l
	map<string, size_t> _id_l_equal_string_an;
};

