#pragma once

#include "draw_engine_v2.h"
#include "src\JScript.h"

#include <QString>
#include <QObject>
#include <functional>

class Dino
{
public:
	Dino()
	{
		active_language = "US";
	}

	~Dino()
	{
	}

	bool loadlayout() {
		QByteArray layout_array = readfile(path + "layout.pbf");

		auto pai = draw_engine_v2::pars_head(layout_array);

		if (pai.first.first != "")
			title = pai.first.first;

		draw2.init_drow(pai.second, BL);

		QByteArray language_array = readfile(path + active_language + ".pbf.lan");

		QByteArray JS_content_array = readfile(path + active_language + ".js.content");
		if (!JS_content_array.isEmpty())
			jscontent.evaluate_script(JS_content_array);

		QByteArray JS_load_array = readfile(path + active_language + ".js.load");
	}
	
	QByteArray readfile(QString spath) {
		QFile file(spath);
		if (!file.open(QIODevice::ReadOnly))
			return false;
		QByteArray read_array = file.readAll();
		file.close();
		return read_array;
	}

	void updateLanguadge() {

	}

	void clean_form() {
		// this->ui->ContentWidget->setUpdatesEnabled(false);
		// qDeleteAll(this->ui->ContentWidget->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));
		// this->ui->ContentWidget->setUpdatesEnabled(true);
		qDeleteAll(BL->findChildren<QWidget*>("", Qt::FindDirectChildrenOnly));

	}

	QString active_language;
	QString title;
	QString color_toolbar;
	QString path;

	draw_engine_v2 draw2;
	QBoxLayout *BL;
	JScript jscontent;
};

class dino_js : public QObject
{
	Q_OBJECT
public:
	dino_js(QObject *parent = nullptr) : QObject(parent) {};
	
	void set(std::function<void(QString)> callback) { setTitle = callback; };

	Q_INVOKABLE void fsetTitle(QString value)
	{
		// setTitle("test");
		int a = 0;
		a += 1;
	};
	std::function<void(QString)> setTitle;
	std::function<void(QString)> notification;
};



/*

void MainWindow::clearLayout(QLayout *layout){
QLayoutItem *item;
if (layout != nullptr) {
while ((item = layout->takeAt(0)) != NULL) {
if (item->layout()) {
clearLayout(item->layout());
delete item->layout();
}
if (item->widget()) {
clearLayout(item->widget()->layout());
delete item->widget();
}
delete item;
}
}
}

void MainWindow::clearWidgets(QLayout * layout) {
if (!layout)
return;
while (auto item = layout->takeAt(0)) {
delete item->widget();
clearWidgets(item->layout());
}
}
*/