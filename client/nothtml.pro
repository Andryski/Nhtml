#-------------------------------------------------
#
# Project created by QtCreator 2016-06-12T12:26:07
#
#-------------------------------------------------

QT       += core gui script scripttools uitools network

RESOURCES += defaultprototypes.qrc

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = nothtml
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    ProtocBuff/PB/Element.pb.cc \
    ProtocBuff/PB/Page.pb.cc \
    ProtocBuff/PB/Button.pb.cc \
    ProtocBuff/PB/Rect.pb.cc \
    ProtocBuff/PB/Light_Element.pb.cc \
    ProtocBuff/PB/JavaScript.pb.cc \
    ProtocBuff/PB/Layout.pb.cc \
    ProtocBuff/PB/StyleSheet.pb.cc

HEADERS  += mainwindow.h \
    ProtocBuff/PB/Element.pb.h \
    ProtocBuff/PB/Page.pb.h \
    ProtocBuff/PB/Button.pb.h \
    ProtocBuff/PB/Rect.pb.h \
    ProtocBuff/PB/Light_Element.pb.h \
    ProtocBuff/PB/JavaScript.pb.h \
    ProtocBuff/PB/Layout.pb.h \
    ProtocBuff/PB/StyleSheet.pb.h

FORMS    += mainwindow.ui


win32-g++: LIBS += -L$$PWD/ProtocBuff/lib_X86_PB_31 -llibprotobuf

#64 bit  win32-msvc2015
win32: LIBS += -L$$PWD/ProtocBuff/lib_X64_PB_31 -llibprotobuf

INCLUDEPATH += $$PWD/ProtocBuff/Sourse


VERSION = 0.0.0.2
QMAKE_TARGET_COMPANY = Zcom
QMAKE_TARGET_PRODUCT = qmake demo nothtml
QMAKE_TARGET_DESCRIPTION = not web not html
QMAKE_TARGET_COPYRIGHT = (c) Andrey Z
