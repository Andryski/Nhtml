#pragma once

#include <QtWidgets>
#include <qstring.h>

#include <QRegion>
#include <QPalette>
#include <QFont>

#include <PB\CSS_style.pb.h>
#include <string>

class keep_style
{
public:
	keep_style() {};
	keep_style(nothtml::Rule &rule);
	~keep_style();
	void apply(QWidget *obj);
private:
	bool tr_region;
	QRegion *region;
	bool tr_palette;
	QPalette *palette;
	bool tr_font;
	QFont *font;

	std::string str;
};

