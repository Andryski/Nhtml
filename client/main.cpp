#include "mainwindow.h"
#include <QApplication>
#include <QtWidgets>
#include <QMessageBox>
#include <QAction>
#include <QCommandLineParser>
typedef void(*func)();

int main(int argc, char *argv[])
{
	//QEvent::Type MyEvent::customEventType2 = QEvent::None;

    QApplication a(argc, argv);
	//QCoreApplication::setApplicationVersion(QT_VERSION_STR);
	QCoreApplication::setApplicationName("my-copy-program1");
	QCoreApplication::setApplicationVersion("1.0");
	QCoreApplication::setOrganizationDomain("setOrganizationDomain");
	QCoreApplication::setOrganizationName("setOrganizationName");

	QCommandLineParser parser;
	parser.setApplicationDescription("Test helper");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to copy."));
	parser.addPositionalArgument("destination", QCoreApplication::translate("main", "Destination directory."));

	// A boolean option with a single name (-p)
	QCommandLineOption file_pbf("pbf");
	parser.addOption(file_pbf);

	parser.process(a);

	QStringList unknownOption = parser.unknownOptionNames();

	const QStringList args = parser.positionalArguments();

	QString targetDir = parser.value(file_pbf);


    MainWindow w;
	w.show();
	return a.exec();
}
