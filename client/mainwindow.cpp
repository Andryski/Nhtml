#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>

#include <QtWidgets>
#include <QApplication>

#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
//#include <QGraphicsAnchorLayout>

#include <QFileDialog>

#include <QFile>

#include "draw_engine_v2.h"

#ifdef _MSC_EXTENSIONS
#pragma warning( push )
#pragma warning( disable : 4100 ; disable : 4267)
#endif // _MSC_EXTENSIONS


#ifdef _MSC_EXTENSIONS
#pragma warning( pop ) 
#endif // _MSC_EXTENSIONS

#include "src\dino.h"

#include <QMessageBox>

#include <QThread>

#ifdef Q_OS_WIN
#include <QtWin>
#include <QWinJumpList>
#include <qwinjumplistitem.h>
#include <qwinjumplistcategory.h>
#endif

// #include "../ImageLogic.h"
// #include "css_style_keep.h"

void MainWindow::Clean(QLayout &oL)
{
	QLayoutItem *poLI;
	QLayout *poL;
	QWidget *poW;

	while ((poLI = oL.takeAt(0)))
	{
		if ((poL = poLI->layout()))
		{
			//EKClean(*poL);
			Clean(*poL);
			delete poL;
		}
		else
			if ((poW = poLI->widget()))
			{
				delete poW;
			}
	}
}


MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	connect(ui->actionUS, &QAction::triggered, [&]() { select_langu("US"); });
	connect(ui->actionRU, &QAction::triggered, [&]() { select_langu("RU"); });

	langGroup = new QActionGroup(this);
	langGroup->addAction(ui->actionRU);
	langGroup->addAction(ui->actionUS);
	ui->actionUS->setChecked(true);

	
	connect(ui->actionAbouteQt, &QAction::triggered, qApp, &QApplication::aboutQt);


	layoGroup = new QActionGroup(this);
	layoGroup->addAction(ui->actionPC);
	layoGroup->addAction(ui->actionFablet);
	layoGroup->addAction(ui->actionTablet);
	layoGroup->addAction(ui->actionsmartfone);
	ui->actionPC->setChecked(true);
	//Clean(*ui->ContentWidget->layout());



	QPushButton *addButton = new QPushButton(this);
	addButton->setText("add");

	ui->tabWidget->setCornerWidget(addButton, Qt::TopRightCorner);//pTabCornerWidget
	//addButton->connect(this, &QPushButton::clicked, [&]() {
	connect(addButton, &QPushButton::clicked, [&]() {
		QWidget *tab = new QWidget(this);
		tab->setObjectName(QStringLiteral("tab"));
		ui->tabWidget->addTab(tab, QString());
		});



#ifdef Q_OS_WIN
	//this->setAttribute(Qt::WA_TranslucentBackground);
	//QtWin::enableBlurBehindWindow(this);
	//QtWin::extendFrameIntoClientArea(this, -1, -1, -1, -1);
#endif

	
	// ui->tabWidget->dragEnterEvent

}

void MainWindow::on_tabWidget_tabCloseRequested(int index)
{
	ui->tabWidget->removeTab(index);
}

void MainWindow::select_langu(QString str) {
	// this->set
};

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::create_new_element(QEvent *ea) {
	MyEvent* postedEvent = (MyEvent*)ea;
	postedEvent->t();
};

QObject * MainWindow::crete_new_Layout(QEvent *t) {
	MyEvent* postedEvent = (MyEvent*)t;
	return postedEvent->t();
};

/**
* @brief      start conetc to server
*/
void MainWindow::on_server_Button_clicked()
{
	/*
	QWinJumpListItem wjli1(QWinJumpListItem::Link);
	wjli1.setArguments(QStringList(QString("as")));
	wjli1.setDescription("Some Text1");
	wjli1.setTitle("Some Text2");

	QWinJumpList wjl;
	//wjl.tasks()->setTitle("Some Title3");
	wjl.tasks()->addItem(&wjli1);
	wjl.tasks()->setVisible(true);
	*/

	QWinJumpList jumplist;
	//jumplist.begin();
	//jumplist.setKnownCategoryShown(QWinJumpList::RecentCategory);

	//jumplist.beginTasks();

	QWinJumpListItem *newProject = new QWinJumpListItem(QWinJumpListItem::Link);
	newProject->setTitle(tr("Create new project"));
	newProject->setFilePath(QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
	newProject->setArguments(QStringList("--new-project"));

	jumplist.tasks()->addLink(tr("Launch SDK Manager"), QDir::toNativeSeparators(QCoreApplication::applicationDirPath()) + "\\sdk-manager.exe");
	jumplist.tasks()->addItem(newProject);
	jumplist.tasks()->setVisible(true);

	auto test1 = jumplist.categories();

	QWinJumpListCategory categ1;
	//categ1.

	//auto newcat = jumplist.addCategory()
	//jumplist.en();


}

/**
* @brief      befor collect all mesege we are is disconet and betvin start drave
*/
void MainWindow::init_draw(const QByteArray &messege) {


	QBoxLayout *BL = new QBoxLayout(QBoxLayout::Direction::Down, this->ui->ContentWidget);
	/*
	QThread *thread = new QThread();
	draw_engine *dr_en = new draw_engine();
	
	dr_en->start(messege);
	if(dr_en->title != "")
		this->setWindowTitle(dr_en->title);
	dr_en->set_layout(BL);
	dr_en->parent = this;
	dr_en->moveToThread(thread);

	//auto point_slot_el = &MainWindow::create_new_element;

	//connect(thread, &QThread::started, &dr_en, &draw_engine::draw);
	//connect(thread, &QThread::started, dr_en, &draw_engine::draw3);
	//connect(this, &MainWindow::startDraw, dr_en, &draw_engine::draw2);
	
	connect(thread, &QThread::started, dr_en, &draw_engine::thread_draw);

	connect(thread, &QThread::finished, thread, &QThread::deleteLater, Qt::DirectConnection);

	connect(dr_en, &draw_engine::sig_create_new_element, this, &MainWindow::create_new_element, \
		Qt::QueuedConnection);

	connect(dr_en, &draw_engine::sig_create_new_Layout, this, &MainWindow::crete_new_Layout, \
		Qt::BlockingQueuedConnection);

	thread->start();
	*/

	//			title		icon?	layout
	// pair<pair<QString, QString>, string>

	auto pai = draw_engine_v2::pars_head(messege);
	if (pai.first.first != "")
		this->setWindowTitle(pai.first.first);
	draw_engine_v2 draw2;
	draw2.init_drow(pai.second, BL);
}

void MainWindow::on_Button_test_clicked()
{
	
}


void MainWindow::on_file_button_clicked()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),"./",tr("proto html (*.pbf)"));



	//QFile file("../convertor/esy.pbf");
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly))
		return;
	QByteArray read_array = file.readAll();
	file.close();
	//emit disconnect_tcp;
	//QString ret_str = QString::fromStdString(array.toStdString());
	
	
	QString fileName_css = fileName + ".css";
	QFile file_css(fileName_css);
	if (file_css.exists()) {
		if (!file_css.open(QIODevice::ReadOnly))
			return;
		QByteArray read_css = file_css.readAll();

		css_style_keep temp;

		temp.SetCssKeep_array(read_css);

		file_css.close();
	}

	init_draw(read_array);
}

bool MainWindow::event(QEvent* event)
{
	//if (event->type() == QEvent::User)
	/*
	if (event->type() == MyEvent::GetType())//MyEvent::type())
	{
		//char const * deb = typeid(event).name();

		MyEvent* postedEvent = static_cast<MyEvent*>(event);
		//QString str = postedEvent->t();
		//this->setWindowTitle(str);
		//this->setWindowTitle(postedEvent->message());

		return true;
	}
	*/

	return QWidget::event(event);
}