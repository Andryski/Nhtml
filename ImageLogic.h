#pragma once

//http://www.qtforum.org/article/26097/qimage-qbytearray-qstring.html

#include <qimage.h>
#include <qbuffer.h>
#include <qbytearray.h>
#include <qfile.h>
#include <qstring.h>

inline QString ImegToStr(QString path) {
	QImage image(path);
	QByteArray array;
	QBuffer buffer(&array);
	image.save(&buffer, "jpg"); //может проблема в формате?


	QByteArray compressed = qCompress(array, 1); // better just open file with QFile, load data, compress and toHex?
	QByteArray hexed = compressed.toHex();
	// save to a file
	QString str(hexed);
	return str;
}

inline QImage StrToImeg(QString image) {

	QByteArray readCompressed = QByteArray::fromHex(image.toLatin1());
	QByteArray readDecompressed = qUncompress(readCompressed);
	QImage readImg;
	readImg.loadFromData(readDecompressed);
	return readImg;
}

//http://www.bogotobogo.com/Qt/Qt5_QMainWindow_QAction_ImageViewer.php
//magic
//http://stackoverflow.com/questions/27343576/how-to-convert-qimage-to-qbytearray
