cd protobuf\cmake
mkdir build\solution
cd build\solution
cmake -G "Visual Studio 14 2015 Win64" ^
-Dprotobuf_BUILD_TESTS=OFF ^
-DCMAKE_INSTALL_PREFIX=../../../../install ../..

bash -c "sed -i 's/<RuntimeLibrary>MultiThreadedDebug<\/RuntimeLibrary>/<RuntimeLibrary>MultiThreadedDebugDLL<\/RuntimeLibrary>/' *.vcxproj"
bash -c "sed -i 's/<RuntimeLibrary>MultiThreaded<\/RuntimeLibrary>/<RuntimeLibrary>MultiThreadedDLL<\/RuntimeLibrary>/' *.vcxproj"

rem -DCMAKE_CXX_FLAGS_DEBUG="/MDd" ^
rem -DCMAKE_C_FLAGS_DEBUG="/MDd" ^
rem -Dprotobuf_BUILD_SHARED_LIBS=OFF ^

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\vsvars32.bat"
devenv protobuf.sln /project libprotobuf.vcxproj /build
devenv protobuf.sln /project protoc.vcxproj /build
copy .\Debug\protoc.exe .\..\..\..\..\protoc.exe


cd ../../../..

call pb_rebild3.bat

xcopy /C /E protobuf\src\google Sourse\google\


devenv .\PB_to_lib\PB_to_lib.vcxproj /build
