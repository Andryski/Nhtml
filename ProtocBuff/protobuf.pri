#OBJECTS_DIR = $$PWD/obj

SOURCES += \
$$PWD/PB/Element.pb.cc \
$$PWD/PB/Page.pb.cc \
$$PWD/PB/Button.pb.cc \
$$PWD/PB/Rect.pb.cc \
$$PWD/PB/Light_Element.pb.cc \
$$PWD/PB/JavaScript.pb.cc \
$$PWD/PB/Layout.pb.cc \
$$PWD/PB/StyleSheet.pb.cc

HEADERS += \
$$PWD/PB/Element.pb.h \
$$PWD/PB/Page.pb.h \
$$PWD/PB/Button.pb.h \
$$PWD/PB/Rect.pb.h \
$$PWD/PB/Light_Element.pb.h \
$$PWD/PB/JavaScript.pb.h \
$$PWD/PB/Layout.pb.h \
$$PWD/PB/StyleSheet.pb.h \
$$PWD/PB/Page.pb.h

win32-g++: LIBS += -L$$PWD/lib_X86_PB_31 -llibprotobuf
#64 bit  win32-msvc2015
win32: LIBS += -L$$PWD/lib_X64_PB_31 -llibprotobuf

INCLUDEPATH += $$PWD/Sourse
INCLUDEPATH += $$PWD/PB
