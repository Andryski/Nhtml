#pragma once
#include <QObject>

#include "../../CompyB/QTCPTalker/QTCPTalker2.h"
#include <QThread>
#include <QtNetwork>
#include <QAbstractSocket>
#include <QTcpServer>
#include <QTcpSocket>

#include <functional>


class MyServer : public QObject
{
	Q_OBJECT
public:
	MyServer(const quint16 &port, QObject *parent = Q_NULLPTR);
	~MyServer();
	void readFiles();

public slots:
	void newConnection();

private:
	QTcpServer * tcpServer;


	QString name_title;
	QString vers;
	QString file_layaut;
	QString file_lang;
	QString type_script;
	QString file_script;
	QVector<QPair<QString, QByteArray>> medias;

};