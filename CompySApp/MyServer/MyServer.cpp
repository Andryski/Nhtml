#include "MyServer.h"
#include "../../CompyB/Tools/MyQThread.h"
#include <QStandardPaths>
#include <QDir>

MyServer::MyServer(const quint16 &port, QObject *parent): QObject(parent)
{
	readFiles();


	tcpServer = new QTcpServer(this);
	connect(tcpServer, &QTcpServer::newConnection, this, &MyServer::newConnection);
	if (!tcpServer->listen(QHostAddress::Any, port)) {
		qWarning() << "cant open port";
		return;
	}
	qDebug() << "socket open";
}

MyServer::~MyServer()
{
}

void MyServer::readFiles()
{
	QString pathFolder = "C:\\Users\\Andry\\source\\Nhtml\\CompyApps\\Apps\\test2/"; //QDir::current().path();
	QFile file(pathFolder + "Layaut/PC.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QByteArray in = file.readAll();
	file_layaut = QString(in);
	file.close();
	in.clear();

	file.setFileName(pathFolder + "vers.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	in = file.readAll();
	vers = QString(in);
	file.close();
	in.clear();

	file.setFileName(pathFolder + "Script_type.txt");
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		//return;
	} else {
		in = file.readAll();
		type_script = QString(in);
		file.close();
		in.clear();

		file.setFileName(pathFolder + "Script/main.js");
		if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			//return;
		} else {
			in = file.readAll();
			file_script = QString(in);
			file.close();
			in.clear();
		}
		
	}

	auto media_folder = QDir(pathFolder);
	media_folder.cd("Media");
	auto test1 = media_folder.entryList();
	auto test2 = media_folder.entryInfoList();

	for (int i = 2; i < test1.count(); ++i)
	{
		file.setFileName(pathFolder + "Media/" + test1[i]);
		if (!file.open(QIODevice::ReadOnly))
			return;
		in = file.readAll();
		auto data = in;
		file.close();
		in.clear();

		medias.append(qMakePair(test1[i], data));
	}
}


void MyServer::newConnection() {
	QTcpSocket* clientSocket = tcpServer->nextPendingConnection();

	//clientSocket->setParent(nullptr);
	MyQThread *thread = new MyQThread("client_thread",this);
	clientSocket->setParent(nullptr);

	connect(thread, &MyQThread::myStarted, [clientSocket,this]() {
		auto peerAddress = clientSocket->peerAddress().toString();
		auto peerPort = clientSocket->peerPort();
		auto peerName = clientSocket->peerName();

		qDebug() << "Connect" + peerAddress + " " + peerName;
		
		//auto sokwrap = new Compy_Network::MyQTCPWrapper(clientSocket);
		//auto *nexttalc = new Compy_Network::QTCPTalker2(nullptr, sokwrap);
		auto *nexttalc = new Compy_Network::QTCPTalker2(nullptr, clientSocket);
		


		nexttalc->send(vers);

		auto read = nexttalc->readString();
		if(read=="get")
		{
			nexttalc->send(type_script);
			nexttalc->send(file_script);
			nexttalc->send(file_layaut);

			nexttalc->send(QString::number(medias.count()));
			for (int i = 0; i < medias.count(); ++i)
			{
				auto item = medias[i];
				nexttalc->send(item.first);
				nexttalc->send(item.second);
			}
		}
		//qInfo() << "request: " + read;
		int a = 0;
		//nexttalc->tcpSocket->close();
		//		delete nexttalc;
	});

	connect(thread, &QThread::finished, thread, &QThread::deleteLater);

	//nexttalc->moveToThread(thread);
	clientSocket->moveToThread(thread);
	thread->start();
}
