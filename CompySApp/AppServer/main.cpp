#include <QtCore/QCoreApplication>

#include "../MyServer/MyServer.h"

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);
	
	MyServer test1(7899, &app);

	return app.exec();
}
