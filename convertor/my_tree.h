#pragma once

#include <vector>
#include <string>
#include <utility>      // std::pair, std::make_pair
using namespace std;

class my_tree
{
public:
	string is_atr(const string &name) {
		auto it = find_if(atrib.begin(), atrib.end(),
			[&](const pair<string, string>& atr) -> bool
		{
			return atr.first == name;
		}
		);
		if (it != atrib.end()) {
			if (it->second != "") {
				return it->second;
			}
		}
		return "";
	}

	vector<my_tree> children;
	vector<pair<string,string>> atrib;
	string content;
	string tag;
	/**/
	bool only_text;
	/**/
	my_tree() {};
	~my_tree() {};

	size_t size() {
		size_t ret = 0;
		// sizeof(v) + sizeof(T) * v.capacity(); 
		// ret += this->tag.length();
		ret += sizeof(string) + sizeof(char) + this->tag.capacity();
		// ret += this->content.length();
		ret += sizeof(string) + sizeof(char) + this->content.capacity();
		
		ret += sizeof(vector<pair<string, string>>);
		for (auto i : atrib)
		{
			// ret += i.first.length();
			// ret += i.second.length();
			ret += sizeof(pair<string, string>);
			ret += sizeof(string) + sizeof(char) + i.second.capacity();
			ret += sizeof(string) + sizeof(char) + i.first.capacity();
		}
		ret += sizeof(this);
		for (auto &i : children)
		{
			// ret += i.size();
			ret += i.size();
		}
		return ret;
	}
};

