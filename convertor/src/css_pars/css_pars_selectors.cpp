#include "css_pars_selectors.h"


#include <sstream> //getline? 
#include <fstream>
using namespace std;

#include "css_pars_rule.h"

css_pars_selectors::css_pars_selectors()
{
}


css_pars_selectors::~css_pars_selectors()
{
}

pair<vector<string>, vector<string>> css_pars_selectors::pars_selectors(const string &strr)
{
	string str(strr);
	//\n
	string delimiter = "\n";
	size_t pos = 0;
	while ((pos = str.find(delimiter)) != string::npos) {
		str.erase(pos, delimiter.length());
	}

	delimiter = ".";
	pos = 0;
	while ((pos = str.find(delimiter)) != string::npos) {
		str.erase(pos, delimiter.length());
	}

	delimiter = "#";
	pos = 0;
	while ((pos = str.find(delimiter)) != string::npos) {
		str.erase(pos, delimiter.length());
	}


	vector<string> selecs;
	vector<string> rules;
	vector<vector<size_t>> my_rules;
	stringstream f(str);
	string s;

	while (getline(f, s, '{')) {
		selecs.push_back(s);
		getline(f, s, '}');
		rules.push_back(s);
	}

	for (size_t i = 0; i < rules.size(); ++i)
	{
		vector<size_t> temp;
		vector<string> strings;
		stringstream f(rules[i]);
		string s;
		while (getline(f, s, ';')) {
			strings.push_back(s);
		}

		for (auto n : strings)
		{
			temp.push_back(css_pars_rule::add_rule(css_pars_rule::parse_cleaning_string(n)));
		}
		my_rules.push_back(temp);
	}

	for (size_t i = 0; i < selecs.size(); ++i)
	{
		stringstream ff(selecs[i]);
		while (getline(ff, s, ',')) {

			stringstream fff(s);
			while (getline(fff, s, ' ')) {

				size_t h1 = hash<string>()(s);
				bool trig3 = s == "sidebar";
				selectors_name[h1] = s;
				// css_pars_selectors::selectors[h1] = css_pars_selectors::selectors[h1].insert(css_pars_selectors::selectors[h1].end(), my_rules[i].begin(), my_rules[i].end());
				auto temp_vector1 = my_rules[i];
				vector<pair<string, string>> temp_vector2;
				for (auto i : temp_vector1) {
					pair<string, string> s1 = css_pars_rule::map_rules[i];
					temp_vector2.push_back(s1);
				}
				
				css_pars_selectors::selectors[h1].insert(css_pars_selectors::selectors[h1].end(), my_rules[i].begin(), my_rules[i].end());

				vector<pair<string, string>> temp_vector3;
				auto vector_test4 = css_pars_selectors::selectors[h1];
				for (auto i : vector_test4) {
					pair<string, string> s1 = css_pars_rule::map_rules[i];
					temp_vector3.push_back(s1);
				}
				int a = 0;
			}
		}
	}

	return make_pair(selecs, rules);
	//return pair<vector<string>, vector<string>>();
}

void css_pars_selectors::add(pair<vector<string>, vector<string>>)
{
}

string css_pars_selectors::css_read_file(const string &path) {
	fstream file;
	string str;
	file.open("test_convert"+path, ios::in);
	if (!file) {
		exception("File not found!\n");
		//std::cout << "File " << " not found!\n";
		//assert(true);
	}

	str.assign((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());

	//stringstream buffer;
	//buffer << file.rdbuf();
	//str = buffer.str();
	/*
	file.seekg(0, ios::end);
	str.resize(file.tellg());
	file.seekg(0, ios::beg);
	file.read(&str[0], str.size());
	*/
	file.close();
	return str;
}

vector<string> css_pars_selectors::pars_selectors_selectors_name(const string &str) {
	string s;
	vector<string> ret;
	stringstream ff(str);
	while (getline(ff, s, ',')) {

		stringstream fff(s);
		while (getline(fff, s, ' ')) {
			ret.push_back(s);
		}
	}
	return ret;
}

/// <summary>
/// ���������� ids ���������� �� ����� ���������
/// </summary>
/// <param name="str"></param>
/// <returns></returns>
vector<size_t> css_pars_selectors::get_selectors_by_name(const string & str)
{
	 vector<size_t> ret;
	 vector<string> names = css_pars_selectors::pars_selectors_selectors_name(str);

	 for (auto i : names) {
		 bool trig1 = i == "sidebar";
		 size_t h1 = hash<string>()(i);
		 auto temp = css_pars_selectors::selectors[h1];
		 ret.insert(ret.end(), temp.begin(), temp.end());
	 }

	 return ret;
}


map<size_t, vector<size_t>> css_pars_selectors::selectors;
map<size_t, string> css_pars_selectors::selectors_name;
map<string, bool> css_pars_selectors::global_selector_use;