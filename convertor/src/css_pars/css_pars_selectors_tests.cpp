#include <gtest\gtest.h>

#include <string>
using namespace std;

#include "css_pars_selectors.h"

TEST(css_pars_selectors, add_rule) {
	string finish_string = ".mod{clear:both}";
	auto temp = css_pars_selectors::pars_selectors(finish_string);
	vector<string> selectors = { "mod" };
	vector<string> rules = { "clear:both" };
	ASSERT_EQ(temp.first, selectors) << "wrong selectors";
	ASSERT_EQ(temp.second, rules) << "wrong rules";
}