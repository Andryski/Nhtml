#include "css_pars_rule.h"

#include <sstream>

css_pars_rule::css_pars_rule()
{
}


css_pars_rule::~css_pars_rule()
{
}

map<size_t, pair<string, string>> css_pars_rule::map_rules;

string css_pars_rule::parse_cleaning_string(string &str)
{
	///todo: wot faster&
	///http://www.cplusplus.com/articles/2wA0RXSz/
	///http://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
	///http://stackoverflow.com/questions/236129/split-a-string-in-c
	///http://stackoverflow.com/questions/5167625/splitting-a-c-stdstring-using-tokens-e-g


	//http://www.charmap.ru/html_table.htm
	string delimiter = "&quot;";

	size_t pos = 0;
	while ((pos = str.find(delimiter)) != string::npos) {
		str.erase(pos, delimiter.length());
	}

	//spase delet
	pos = 0;
	while ((pos = str.find(' ')) != string::npos) {
		str.erase(pos, 1);
	}

	//\n
	delimiter = "\n";
	pos = 0;
	while ((pos = str.find(delimiter)) != string::npos) {
		str.erase(pos, delimiter.length());
	}
	return str;
}

size_t css_pars_rule::add_rule(string &rule_s)
{
	vector<string> strings;
	stringstream f(rule_s);
	string s;
	getline(f, s, ';');
	
	size_t h1 = hash<string>()(s);

	size_t pos = 0;
	pos = s.find(":");
	if (pos == std::string::npos) {
		/*
		std::vector<string>::iterator it;

		it = find(strings.begin(), strings.end(), n);

		strings.erase(it);
		string repl = *it;
		*/
		//replace(strings.begin(), strings.end(), n, n + repl);
	}
	else {
		string first = s.substr(0, pos);
		string second = s.substr(pos + 1);
		/*
		if (first[0] == ' ') {
		first.erase(0, 1);
		}
		if (second[0] == ' ') {
		second.erase(0, 1);
		}
		*/
		//strings_rules.push_back(make_pair(first, second));
		map_rules[h1] = make_pair(first, second);
	}
	return h1;
}

vector<size_t> css_pars_rule::add_rules(string &str)
{
	string str_clean = parse_cleaning_string(str);

	vector<string> strings;
	stringstream f(str_clean);
	string s;
	while (getline(f, s, ';')) {
		strings.push_back(s);
	}

	vector<size_t> mas;
	for (auto n : strings)
	{
		mas.push_back(add_rule(n));
	}

	return mas;
}
