#pragma once

//#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <utility> //pair?
#include <functional> //hash?
using namespace std;

class css_pars_selectors
{
public:
	css_pars_selectors();
	~css_pars_selectors();

	// selectors , rules
	static pair<vector<string>,vector<string>> pars_selectors(const string &);

	
	static void add(pair<vector<string>, vector<string>>);

	static string css_read_file(const string &path);

	static vector<string> pars_selectors_selectors_name(const string &str);
	static vector<size_t> get_selectors_by_name(const string &str);

//private:
	// id-selectors  ; ids-rules
	static map<size_t, vector<size_t>> selectors;
	static map<size_t, string> selectors_name;
	static map<string, bool> global_selector_use;
};

#include "css_pars_rule.h"
#include <PB\CSS_style.pb.h>
class Selection
{
//friend css_pars_selectors;
public:
	Selection() {};
	~Selection() {};

	nothtml::css_float is_has_float() {
		nothtml::css_float ret = nothtml::css_float::none;
		for (size_t selector : selectors)
		{
			vector<size_t> rules = css_pars_selectors::selectors[selector];
			for (size_t rule : rules) {
				auto temp = _is_rule_has_float(rule);
				if (temp != nothtml::css_float::none) {
					ret = temp;
				}
			}
		}
		for (size_t rule : vrules) {
			auto temp = _is_rule_has_float(rule);
			if (temp != nothtml::css_float::none) {
				ret = temp;
			}
		}

		return ret;
	};

	nothtml::css_float _is_rule_has_float(size_t rule) {
		pair<string, string> str_rule = css_pars_rule::map_rules[rule];
		if (str_rule.first == "float") {
			if (str_rule.second == "left") {
				return nothtml::css_float::left;
			}
			if (str_rule.second == "right") {
				return nothtml::css_float::right;
			}
		}

		return nothtml::css_float::none;
	}
//private:
	vector<size_t> selectors;
	vector<size_t> vrules;
};