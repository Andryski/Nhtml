#include "convert.h"

typedef pair<pair<int, nothtml::Element>, string> CO_messege;

#include "convert_tabel.h"

#include "src\css_pars\css_pars_selectors.h"

/// <summary>
/// ����� � �������� ������ ������� � ���������� ��������
/// </summary>
/// <param name="elem">�����</param>
/// <param name="str">����� ������� ����</param>
/// <returns></returns>
string convert_clas::atrib_serch(const my_tree & elem, const string & str)
{
	auto it = find_if(elem.atrib.begin(), elem.atrib.end(),
		[&](const pair<string, string>& atr) -> bool
	{
		return atr.first == str;
	}
	);
	if (it != elem.atrib.end()) {
		if (it->second != "") {
			return it->second;
		}
	}
	return "";
}

/// <summary>
/// ����� ������ �� � �������� �����
/// </summary>
/// <param name="elem"></param>
/// <returns></returns>
pair<string, nothtml::css_float> convert_clas::serch_style(const my_tree & elem)
{
	Selection serch_select;

	string atr_str = atrib_serch(elem, "style");
	if (atr_str != "") {
		serch_select.vrules = css_pars_rule::add_rules(atr_str);
	}

	string selectors = "";

	// selectors += selectors != "" ? " " : "";
	atr_str = atrib_serch(elem, "class");
	if (atr_str != "") {
		bool tr1 = atr_str == "sidebar";
		selectors += atr_str;

	}

	selectors += selectors != "" ? " " : "";
	atr_str = atrib_serch(elem, "id");
	if (atr_str != "") {
		selectors += atr_str;
		
	}

	if (selectors != "") {
		// serch_select.selectors = css_pars_selectors::get_selectors_by_name(atr_str);
		// TODO: ������ � ��� ��������� �� ��������� � �������?!?!?!?!??!
		//serch_select.selectors = css_pars_selectors::get_selectors_by_name(selectors);
		auto vector_rule = css_pars_selectors::get_selectors_by_name(selectors);
		/*if (vector_rule.size() != 0) {
			if (serch_select.vrules.size() == 0) {
				serch_select.vrules = serch_select.selectors;
			}
			else {
				serch_select.vrules.insert(serch_select.selectors.end(), serch_select.selectors.begin(), serch_select.selectors.end());
			}
		}
		*/
		
		if (vector_rule.size() != 0) {
			if (serch_select.vrules.size() == 0) {
				serch_select.vrules = vector_rule;
			}
			else {
				serch_select.vrules.insert(serch_select.vrules.end(), vector_rule.begin(), vector_rule.end());
			}
		}
	}


	vector<pair<string, string>> debug;
	for (size_t i : serch_select.selectors) {
		for (auto j : css_pars_selectors::selectors[i]) {
			debug.push_back(css_pars_rule::map_rules[j]);
		}
	}

	vector<pair<string, string>> debug2;
	for (size_t i : serch_select.selectors) {
		debug2.push_back(css_pars_rule::map_rules[i]);
	}

	nothtml::css_float retcs = serch_select.is_has_float();

	// nothtml::css_float retcs;//// = rules_temp.rule_chek_float_css();
	// retcs = nothtml::css_float::none;
	int b = 0;
	if (retcs != nothtml::css_float::none) {
		int na = 0;
		na += 4;
		b += na;
	}
	return make_pair(selectors, retcs);
}

pair<CO_messege, nothtml::css_float> convert_clas::conv_one_node(const my_tree & elem)
{
	int count = 0;
	nothtml::css_float retcs = nothtml::css_float::none;
	//string temp = "";
	string compare_text = " h1 h2 h3 p li pre strong a spawn";

	CO_messege messege;
	if (elem.tag == "" && elem.content != "") {

		messege.first.second = nothtml::Element::LABEL;
		messege.second = elem.content;
		++count;
	}
	else
		if (compare_text.find(elem.tag) != string::npos) {
			if (elem.content != "") {
				messege.first.second = nothtml::Element::LABEL;
				messege.second = elem.content;
				++count;
			}
			else {
				return make_pair(make_pair(make_pair(0, nothtml::Element::ERROR_READ_PB), string()), retcs);
			}
		}
		else

			if (elem.tag == "img") {
				string adr = atrib_serch(elem, "src");

				messege.first.second = nothtml::Element::PICTURE;
				messege.second = adr;
				++count;
			}
			else

				if (elem.tag == "button") {
					messege.first.second = nothtml::Element::BUTTON;
					messege.second = elem.content;
					++count;
				}

	//string rule = css_parent.get_by_rules(elem.tag);
	//string rule = global_selector.get_by_rules(elem.tag);

	//if (rule != "") {
	//nothtml::StyleSheet style;
	//style.set_sheet(rule);
	//temp += convert(style);
	//++count;
	//}

	if (elem.atrib.size() != 0) {
		//std::vector< pair<string, string> >::iterator it;

		pair<string, nothtml::css_float> temp2 = serch_style(elem);

		if (temp2.first != "") {
			//nothtml::StyleSheet style;
			//style.set_sheet_name(temp2.first);
			//temp += convert(style);
			//++count;
		}
		//retcs = rules_temp.rule_chek_float_css();
		retcs = temp2.second;
	}//end (elem.atrib.size() != 0) 

	if (count != 0) {
		if (count == 1) {
			return make_pair(messege, retcs);
		}
		else {
			//nothtml::Layout lay1;
			//lay1.set_num(count);
			//lay1.set_type(nothtml::Layout_Element::META_L);
			//lay1.set_next_le(temp.size());
			//string temp2 = convert(lay1);
			//return make_pair(temp2 + temp, retcs);
		}
	}
	return make_pair(make_pair(make_pair(0, nothtml::Element::ERROR_READ_PB), ""), retcs);

}

pair<string, nothtml::css_float> convert_clas::conv_nodes(const my_tree & elem)
{
	nothtml::css_float retcs = nothtml::css_float::none;

	pair<string, nothtml::css_float> temp2 = serch_style(elem);

	if (temp2.first != "") {
		//nothtml::StyleSheet style;
		//style.set_sheet_name(temp2.first);
		//temp += convert(style);
		//++count;
	}
	retcs = temp2.second;

	co_vector_elements elements;
	for (const my_tree &child : elem.children)
	{
		if (child.children.size() == 0) {
			// ���� ����
			if (!conv_one_node_meta_chec(child)) {

				auto temp_get = conv_one_node(child);

				if (temp_get.first.second != "") {
					elements.push_back(temp_get);
					if (temp_get.second != nothtml::css_float::none)
					{
						finish_struct_conv_l = convert_clas::finish_struct_conv_with_format;
					}
				}
			}

		}//end one elem
		else
		{
			if (child.tag == "table") {
				convert_tabel tab1;
				tab1.row_work(child.children[0]);
				elements.push_back(make_pair(make_pair(make_pair(0, nothtml::Element::TABLE), tab1.out_tabel()), nothtml::css_float::none));
			}
			else
			{
				// ����� ���
				auto temp_get = conv_nodes(child);
				if (temp_get.first != "") {
					elements.push_back(make_pair(make_pair(make_pair(0, nothtml::Element::LAYOUT), temp_get.first), temp_get.second));
					if (temp_get.second != nothtml::css_float::none)
					{
						//assert(true);
						//assert(false);
						finish_struct_conv_l = convert_clas::finish_struct_conv_with_format;
					}
				}
			}//end if (child.tag == "table") {
		}
	}//end fore each

	size_t count = elements.size();
	string ret_str = "";

	if (count != 0) {
		string temp_debug = finish_struct_conv_l(elements);
		return make_pair(temp_debug, retcs);
	}
	else {
		return make_pair("", nothtml::css_float::none);
	}
}

// �������� ���� �� ���� ��������
bool convert_clas::conv_one_node_meta_chec(const my_tree & elem2)
{
	bool ret = false;
	if (elem2.tag == "link") {
		ret = true;
		//href
		string adr = atrib_serch(elem2, "href");
		if (adr.find(".css") != string::npos) {
			css_pars_selectors::pars_selectors(css_pars_selectors::css_read_file(adr));
		}
		else {
			adr = atrib_serch(elem2, "rel");
			if (adr.find("shortcut icon") != string::npos) {
				global_icon_path = elem2.content;
			}
		}
	}
	else
		if (elem2.tag == "style") {
			ret = true;
			css_pars_selectors::pars_selectors(elem2.content);
		}
		else
			if (elem2.tag == "title") {
				ret = true;
				convert_clas::global_title = elem2.content;
			}
	//
	//<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	return ret;
}

// ����������� ������� ������������ ��������� ���� ���� �������������� ��������� � finish_struct_conv_l
string convert_clas::finish_struct_conv_with_format(const co_vector_elements &elements)
{
	vector <CO_messege> left, centr, right;
	for each (auto var in elements)
	{
		if (var.second == nothtml::css_float::right)
		{
			right.push_back(var.first);
		}
		if (var.second == nothtml::css_float::left)
		{
			left.push_back(var.first);
		}
		if (var.second == nothtml::css_float::none)
		{
			centr.push_back(var.first);
		}
	}
	nothtml::Layout layut;
	layut.set_type_layout(nothtml::Layout_Element::Horizontal);

	nothtml::Layout layut_central;
	layut_central.set_type_layout(nothtml::Layout_Element::Vertical);

	for each (auto var in left)
	{
		nothtml::Layout_content *lc_messege = layut.add_messege();
		lc_messege->set_id_mess(var.first.first);
		lc_messege->set_type_string(var.first.second);
		lc_messege->set_string(var.second);
	}

	for each (auto var in centr)
	{
		nothtml::Layout_content *lc_messege = layut_central.add_messege();
		lc_messege->set_id_mess(var.first.first);
		lc_messege->set_type_string(var.first.second);
		lc_messege->set_string(var.second);
	}

	string central_str = convert(layut_central);
	nothtml::Layout_content *lc_messege_cent = layut.add_messege();
	lc_messege_cent->set_id_mess(0);
	lc_messege_cent->set_type_string(nothtml::Element::LAYOUT);
	lc_messege_cent->set_string(central_str);


	for (size_t i = right.size(); i >= 1; --i)
	{
		nothtml::Layout_content *lc_messege = layut.add_messege();
		lc_messege->set_id_mess(right[i - 1].first.first);
		lc_messege->set_type_string(right[i - 1].first.second);
		lc_messege->set_string(right[i - 1].second);
	}


	 //nothtml::Layout_content *lc_spaser = layut.add_messege();
	 /*lc_messege_cent->set_id_mess(0);
	 lc_messege_cent->set_type_string(nothtml::Element::VSPASER);
	 lc_messege_cent->set_string("");*/
	 //lc_spaser->set_id_mess(0);
	 //lc_spaser->set_type_string(nothtml::Element::VSPASER);
	 //lc_spaser->set_string("");

	string full_lay = convert(layut);

	//return make_pair(full_lay, retcs);
	return full_lay;
}

// ����������� ������� ������������ ��������� ���� ��� �������������� ��������� � finish_struct_conv_l
string convert_clas::finish_struct_conv_with_not_format(const co_vector_elements &elements)
{
	nothtml::Layout lay1;
	lay1.set_type_layout(nothtml::Layout_Element::Vertical);

	for each (auto var in elements)
	{
		nothtml::Layout_content *lc_messege = lay1.add_messege();
		lc_messege->set_id_mess(var.first.first.first);
		lc_messege->set_type_string(var.first.first.second);
		lc_messege->set_string(var.first.second);
	}

	string temp = convert(lay1);

	//return make_pair(temp, retcs);
	return temp;
}

string convert_clas::global_title;
string convert_clas::global_icon_path;