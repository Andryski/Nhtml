#pragma once

#include <string>
#include <vector>

//#include <PB\CSS_style.pb.h>
#include <PB\Tabel.pb.h>
#include <PB\Layout.pb.h>
#include <PB\Light_Element.pb.h>

#include "my_tree.h"
#include "convert.h"
#include "MMy_pars_css_selectors.h"

#include "convert.h"

using namespace std;
//using namespace nothtml;

class matrix_colum {
public:
	matrix_colum() { 
		//colum = -1;
		colum = 0;
		colums.push_back(-1);
	};
	~matrix_colum() {};
	int push_back(const int &num) {
		int col = free();
		colums[col] = num;
		return col;
	}
	int push_back(int &&num) {
		int col = free();
		colums[col] = num;
		return col;
	}
	int& operator[](std::size_t idx) { 
		if (idx > colum) {
			int tempcol = -1;
			for (size_t i = 0; i < idx-colum; i++)
			{
				++tempcol;
				colums.push_back(-1);
			}
			colum += tempcol;
		}
		return colums[idx];
	}
	/*const int& operator[](std::size_t idx) const { 
		if (idx > colum) {
			for (size_t i = 0; i < idx - colum; i++)
			{
				colums.push_back(-1);
			}
		}
		return colums[idx];
	}*/
	int free() {
		int i = -1;
		for each (int var in colums)
		{
			++i;
			if (var == -1) {
				return i;
			}
		}
		colums.push_back(-1);
		++i;
		//colum = i;
		++colum;
		return i;
	}
	int size() {
		return colum;
	}
private:
	int colum;
	vector<int> colums;
};

class matrix
{
public:
	matrix() {
		//rowC = -1;
		rowC = 0;
		row.push_back(matrix_colum());
	};
	~matrix() {};
	int push_back(const int &num) {

	}
	int push_back(int &&num) {

	}
	matrix_colum& operator[](size_t idx) {
		
		if (idx > rowC) {
			int temprc = -1;
			for (size_t i = 0; i < idx - rowC; i++)
			{
				++temprc;
				row.push_back(matrix_colum());
			}
			rowC += temprc + 1;
		}
		return row[idx];
	}
	int get_row() {
		return rowC;
	}
	int get_colum_max() {
		int maxy = -1;
		for (size_t i = 0; i <= rowC; i++)
		{
			int temp = row[i].size();
			if (temp > maxy) {
				maxy = temp;
			}
		}
		return maxy;
	}
private:
	int rowC;
	vector<matrix_colum> row;
};

class convert_tabel
{
public:
	convert_tabel() {};
	~convert_tabel() {};

	void test11() {
		//convert_clas *test2;
	};

	void row_work(const my_tree &tabel) {
		int count = -1;
		int row_count = -1;
		for each (my_tree rows_child in tabel.children)
		{
			++row_count;
			//int colum_count = -1;

			for each (my_tree colum_child in rows_child.children)
			{
				++count;
				
				int colum_count = nrow[row_count].free();

				pair<int, int> ofset;
				string temp = colum_child.is_atr("colspan");
				if (temp != "") {
					int y_span = std::stoi(temp);
					ofset.second = y_span - 1;
				}

				temp = colum_child.is_atr("rowspan");
				if (temp != "") {
					int x_span = std::stoi(temp);
					ofset.first = x_span - 1;
				}

				for (size_t x = 0; x < (ofset.first == 0 ? 1 : ofset.first); x++)
				{
					for (size_t y = 0; y < (ofset.second == 0 ? 1 : ofset.second); y++)
					{
						nrow[row_count + x][colum_count + y] = count;
					}
				}

				pair<CO_messege, nothtml::css_float> answer;
				if (colum_child.children.size() == 0) {
					if (colum_child.content != "") {
						colum_child.tag = "";
						////	answer = conv_one_node(colum_child, MMy_pars_css_selectors());
						convert_clas temp1;
						answer =  temp1.conv_one_node(colum_child);
					}else{
						
					}
				}
				else {
			////		auto temp = conv_nodes(colum_child, MMy_pars_css_selectors());
			////	answer = make_pair(make_pair(make_pair(0, nothtml::Element::LAYOUT), temp.first), temp.second);
					convert_clas temp2;
					auto temp = temp2.conv_nodes(colum_child);
					answer = make_pair(make_pair(make_pair(0, nothtml::Element::LAYOUT), temp.first), temp.second);
				}
				pair<pair<int, int>, CO_messege> end_ob;
				end_ob.first = ofset;
				end_ob.second = answer.first;
				cells.push_back(end_ob);
			}
		}
	}

	string out_tabel() {
		string rest_str;
		nothtml::Tabel tabel1;
		int count = 0;
		int x, y;
		x = nrow.get_row();
		y = nrow.get_colum_max();
		for (size_t ix = 0; ix <= x; ix++)
		{
			for (size_t iy = 0; iy <= y; iy++)
			{
				size_t n = nrow[ix][iy];
				if (n == -1) continue;
				if (n == count) {
					auto xys = cells[count];
					auto xy = xys.first;
					
					CO_messege test_t = xys.second;
					if (test_t.second != "") {
						//nothtml::Tabel::Tabel_elem *elem = tabel1.add_elements();
						nothtml::Tabel_elem *elem = tabel1.add_elements();
						nothtml::Layout_content *mes;
						mes = new nothtml::Layout_content();
						mes->set_id_mess(test_t.first.first);
						mes->set_type_string(test_t.first.second);
						mes->set_string(test_t.second);
						elem->set_allocated_obj(mes);
						elem->set_x(ix);
						elem->set_y(iy);
						elem->set_x_spawn(xy.first);
						elem->set_y_spawn(xy.second);
					}
					
					++count;
				}
			}
		}
		tabel1.set_row(x);
		tabel1.set_colum(y);
		rest_str += convert(tabel1);
		return rest_str;
	};
private:
	//std::dynarray test;
	//std::valarray<int> test2;

	matrix nrow;
	vector<pair<pair<int, int>, CO_messege>> cells;
};