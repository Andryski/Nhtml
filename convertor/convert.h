#pragma once

#include <PB\Page.pb.h>
#include <PB\Layout.pb.h>
#include <PB\CSS_style.pb.h>

#include <string>
#include <functional> //hash
#include <map>
#include <utility> //pair
using namespace std;


#include "my_tree.h"
#include "../for_messege.h"

typedef pair<pair<int, nothtml::Element>, string> CO_messege;

////pair<string, nothtml::css_float> conv_nodes(const my_tree  &elem, const MMy_pars_css_selectors &css_parent);// = MMy_pars_css_selectors());
//pair<string, nothtml::css_float> conv_one_node(my_tree  &elem, MMy_pars_css_selectors &css_parent);// = MMy_pars_css_selectors())
////pair<CO_messege, nothtml::css_float> conv_one_node(my_tree  &elem, MMy_pars_css_selectors &css_parent);// = MMy_pars_css_selectors())


typedef vector <pair<CO_messege, nothtml::css_float>> co_vector_elements;


class convert_clas
{
public:
	convert_clas() {
		finish_struct_conv_l = convert_clas::finish_struct_conv_with_not_format;
	}
	
	// ����� �������� � ��������
	string atrib_serch(const my_tree  &elem, const string &str);
	// 
	pair<string, nothtml::css_float> serch_style(const my_tree  &elem);
	// ����������� ����� ����
	pair<CO_messege, nothtml::css_float> conv_one_node(const my_tree  &elem);
	// ����������� ����� (���)
	pair<string, nothtml::css_float> conv_nodes(const my_tree  &elem);
	bool conv_one_node_meta_chec(const my_tree &elem2);

	// ��������� ��� �������� ������� ������� �������� ������� ����������
	function<string(const vector <pair<CO_messege, nothtml::css_float>>&)> finish_struct_conv_l;

	static string finish_struct_conv_with_format(const co_vector_elements &elements);
	static string finish_struct_conv_with_not_format(const co_vector_elements &elements);

	~convert_clas()
	{
	}

	static string global_title;
	static string global_icon_path;
};