#pragma once
// Copyright 2015 Kevin B. Hendricks, Stratford, Ontario,  All Rights Reserved.
// loosely based on a greatly simplified version of BeautifulSoup4 decode() routine
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: Kevin Hendricks
//
// Serialize back to html / xhtml making as few changes as possible (even in whitespace)

//#pragma once


#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;


#include "my_tree.h"

#include "gumbo.h"

static std::string nonbreaking_inline = "|a|abbr|acronym|b|bdo|big|cite|code|dfn|em|font|i|img|kbd|nobr|s|small|span|strike|strong|sub|sup|tt|";
static std::string empty_tags = "|area|base|basefont|bgsound|br|command|col|embed|event-source|frame|hr|image|img|input|keygen|link|menuitem|meta|param|source|spacer|track|wbr|";
static std::string preserve_whitespace = "|pre|textarea|script|style|";
static std::string special_handling = "|html|body|";
static std::string no_entity_sub = "|script|style|";


static void replace_all(std::string &s, const char * s1, const char * s2)
{
	std::string t1(s1);
	size_t len = t1.length();
	size_t pos = s.find(t1);
	while (pos != std::string::npos) {
		s.replace(pos, len, s2);
		pos = s.find(t1, pos + len);
	}
}


static std::string substitute_xml_entities_into_text(const std::string &text)
{
	std::string result = text;
	// replacing & must come first 
	replace_all(result, "&", "&amp;");
	replace_all(result, "<", "&lt;");
	replace_all(result, ">", "&gt;");
	return result;
}


static std::string substitute_xml_entities_into_attributes(char quote, const std::string &text)
{
	std::string result = substitute_xml_entities_into_text(text);
	if (quote == '"') {
		replace_all(result, "\"", "&quot;");
	}
	else if (quote == '\'') {
		replace_all(result, "'", "&apos;");
	}
	return result;
}


static std::string handle_unknown_tag(GumboStringPiece *text)
{
	std::string tagname = "";
	if (text->data == NULL) {
		return tagname;
	}
	// work with copy GumboStringPiece to prevent asserts 
	// if try to read same unknown tag name more than once
	GumboStringPiece gsp = *text;
	gumbo_tag_from_original_text(&gsp);
	tagname = std::string(gsp.data, gsp.length);
	return tagname;
}


static std::string get_tag_name(GumboNode *node)
{
	std::string tagname;
	// work around lack of proper name for document node
	if (node->type == GUMBO_NODE_DOCUMENT) {
		tagname = "document";
	}
	else {
		tagname = gumbo_normalized_tagname(node->v.element.tag);
	}
	if (tagname.empty()) {
		tagname = handle_unknown_tag(&node->v.element.original_tag);
	}
	return tagname;
}


static std::string build_doctype(GumboNode *node)
{
	std::string results = "";
	if (node->v.document.has_doctype) {
		results.append("<!DOCTYPE ");
		results.append(node->v.document.name);
		std::string pi(node->v.document.public_identifier);
		if ((node->v.document.public_identifier != NULL) && !pi.empty()) {
			results.append(" PUBLIC \"");
			results.append(node->v.document.public_identifier);
			results.append("\" \"");
			results.append(node->v.document.system_identifier);
			results.append("\"");
		}
		results.append(">\n");
	}
	return results;
}


static std::string build_attributes(GumboAttribute * at, bool no_entities)
{
	std::string atts = " ";
	atts.append(at->name);

	// how do we want to handle attributes with empty values
	// <input type="checkbox" checked />  or <input type="checkbox" checked="" /> 

	if ((!std::string(at->value).empty()) ||
		(at->original_value.data[0] == '"') ||
		(at->original_value.data[0] == '\'')) {

		// determine original quote character used if it exists
		char quote = at->original_value.data[0];
		std::string qs = "";
		if (quote == '\'') qs = std::string("'");
		if (quote == '"') qs = std::string("\"");
		atts.append("=");
		atts.append(qs);
		if (no_entities) {
			atts.append(at->value);
		}
		else {
			atts.append(substitute_xml_entities_into_attributes(quote, std::string(at->value)));
		}
		atts.append(qs);
	}
	//printf("win not ecsept \n");
	return atts;
}


// forward declaration
static my_tree* my_serialize(GumboNode*);


// serialize children of a node
// may be invoked recursively

static my_tree* my_serialize_contents(GumboNode* node) {

	my_tree* my_content;

	std::string contents = "";
	std::string tagname = get_tag_name(node);
	std::string key = "|" + tagname + "|";
	bool no_entity_substitution = no_entity_sub.find(key) != std::string::npos;
	bool keep_whitespace = preserve_whitespace.find(key) != std::string::npos;
	bool is_inline = nonbreaking_inline.find(key) != std::string::npos;

	// build up result for each child, recursively if need be
	GumboVector* children = &node->v.element.children;

	if (children->length == 0) {
		my_tree* temp;
		/*
		const GumboVector * attribs = &node->v.element.attributes;
		if (attribs->length < 500) {
			for (int i = 0; i < attribs->length; ++i) {
				GumboAttribute* at = static_cast<GumboAttribute*>(attribs->data[i]);
				temp.atrib.push_back(make_pair(build_attributes(at, no_entity_substitution), "temp"));
			}
		}
		*/
		temp = new my_tree;
		temp->tag = tagname;
		return temp;
	}
	my_content = new my_tree;
	for (unsigned int i = 0; i < children->length; ++i) {
		GumboNode* child = static_cast<GumboNode*> (children->data[i]);
		my_tree* temp;
		temp = new my_tree;
		if (child->type == GUMBO_NODE_TEXT) {
			if (no_entity_substitution) {
				//contents.append(std::string(child->v.text.text));
				temp->only_text = true;
				temp->content = string(child->v.text.text);
			}
			else {
				//contents.append(substitute_xml_entities_into_text(std::string(child->v.text.text)));
				temp->only_text = true;
				temp->content = substitute_xml_entities_into_text(std::string(child->v.text.text));
			}

			try {
				const GumboVector * attribs = &child->v.element.attributes;
				if (attribs != nullptr)
					if (attribs->length < 500) {
						for (int i = 0; i < attribs->length; ++i) {
							if (attribs->data != nullptr) {
								//GumboAttribute* at = static_cast<GumboAttribute*>(attribs->data[i]);
								GumboAttribute* at = (GumboAttribute*)(attribs->data[i]);
								if (int(&at) > 200)
									if (at != nullptr)
										if (at->name != nullptr)
											if (at->value != nullptr)
												temp->atrib.push_back(make_pair(build_attributes(at, no_entity_substitution), "temp"));
							}
						}
					}
			}
			catch (char *e) {
				//printf("Exception Caught: %s\n", e);
			}
			catch (...) {
				//printf("Exception Caught: hz \n");
			}
		}

		else if (child->type == GUMBO_NODE_ELEMENT || child->type == GUMBO_NODE_TEMPLATE) {
			//contents.append(serialize(child));
			my_tree* temp2 = my_serialize(child);
			if (temp2->children.size() == 1) {
				//temp = temp2;
				
				if (temp2->children[0].children.size() != 0) {
					temp->children = temp2->children;
				}

				temp->content = temp2->children[0].content;
				temp->tag = temp2->tag;
				temp->only_text = temp2->children[0].only_text;
			}else {
				temp = temp2;
			}
			const GumboVector * attribs = &child->v.element.attributes;
			if (attribs->length < 500) {
				for (int i = 0; i < attribs->length; ++i) {
					GumboAttribute* at = static_cast<GumboAttribute*>(attribs->data[i]);

					string all_atr = build_attributes(at, no_entity_substitution);
					//string atr1(all_atr.begin(), all_atr.find("=",1));
					string delimiter = "=\"";
					string token = all_atr.substr(0, all_atr.find(delimiter));
					string token2 = all_atr.substr(all_atr.find(delimiter)+2, all_atr.size()-3- all_atr.find(delimiter));

					int ind = token.find(' ');
					if(ind != std::string::npos)	token.erase(ind, 1);

					temp->atrib.push_back(make_pair(token, token2));

					//temp.atrib.push_back(make_pair(build_attributes(at, no_entity_substitution), "temp"));
				}
			}
		}
		else if (child->type == GUMBO_NODE_WHITESPACE) {
			// keep all whitespace to keep as close to original as possible
			//contents.append(std::string(child->v.text.text));
			
			temp->only_text = true;
			//temp.content = string(child->v.text.text);
		}
		else if (child->type != GUMBO_NODE_COMMENT) {
			// Does this actually exist: (child->type == GUMBO_NODE_CDATA)
			fprintf(stderr, "unknown element of type: %d\n", child->type);
		}
		if((temp->children.size()!=0)|| (temp->content.length() != 0) || (temp->tag != ""))
			my_content->children.push_back(*temp);
		delete temp;
		if (child != nullptr) {
			// delete child;
		}
		else {
			int a = 0;
			a = 5;
		}
	}

	

	return my_content;
}


// serialize a GumboNode back to html/xhtml
// may be invoked recursively

static my_tree* my_serialize(GumboNode* node) {

	my_tree* my_result;
	
	// special case the document node
	if (node->type == GUMBO_NODE_DOCUMENT) {
		std::string results = build_doctype(node);
		//my_result.children.push_back(my_serialize_contents(node));
		//results.append(my_serialize_contents(node));
		//return my_result;
		return my_serialize_contents(node);
	}
	my_result = new my_tree;
	
	std::string close = "";
	std::string closeTag = "";
	std::string atts = "";
	std::string tagname = get_tag_name(node);
	std::string key = "|" + tagname + "|";
	bool need_special_handling = special_handling.find(key) != std::string::npos;
	bool is_empty_tag = empty_tags.find(key) != std::string::npos;
	bool no_entity_substitution = no_entity_sub.find(key) != std::string::npos;
	bool is_inline = nonbreaking_inline.find(key) != std::string::npos;

	string tagstyle = "style";
	int debug = tagname.compare(tagstyle);



	my_result->tag = tagname;

	/*/
	// build attr string  
	const GumboVector * attribs = &node->v.element.attributes;
	for (int i = 0; i< attribs->length; ++i) {
		GumboAttribute* at = static_cast<GumboAttribute*>(attribs->data[i]);
		my_result.atrib.push_back(make_pair(build_attributes(at, no_entity_substitution),"temp"));
		//atts.append(build_attributes(at, no_entity_substitution));
	}
	*/

	// determine closing tag type
	/*
	if (is_empty_tag) {
		close = "/";
	}
	else {
		closeTag = "</" + tagname + ">";
	}
	*/
	// serialize your contents
	my_tree* contents = my_serialize_contents(node);
	//my_result.children.push_back(contents);
	my_result->children = contents->children;
	delete contents;
	/*
	if (need_special_handling) {
		ltrim(contents);
		rtrim(contents);
		contents.append("\n");
	}
	*/

	// build results
	//std::string results;
	//results.append("<" + tagname + atts + close + ">");
	/*
	if (need_special_handling) results.append("\n");
	results.append(contents);
	results.append(closeTag);
	if (need_special_handling) results.append("\n");
	*/
	return my_result;
}

vector<string> my_get_style(my_tree &elem) {
	vector<string> result;
	if (elem.children.size() == 0) {
		if (elem.tag == "style") {
			result.push_back(elem.content);
		}
	}
	//for each (my_tree child in elem.children)
	//for (auto child : elem.children)
	for(auto it = elem.children.begin() ; it!= elem.children.end() ; ++it)
	{
		vector<string> temp = my_get_style(*it); //child);
		result.insert(result.end(), temp.begin(), temp.end());
	}
	return result;
}

bool my_tree_empty(const my_tree &elem) {
	if ((elem.children.size() == 0) && (elem.atrib.size() == 0) && (elem.content == "") && (elem.tag == "")) {
		return true;
	}
	else {
		return false;
	}
}

my_tree my_find_elem(const my_tree &elem,const string &str) {
	my_tree result;
	if (elem.tag == str) {
		result = elem;
	}
	for(const my_tree &child : elem.children)
	{
		const my_tree &temp = my_find_elem(child, str);
		if (!my_tree_empty(temp)) {
			return temp;
		}
	}
	return result;
}


my_tree* pars_html(const string &document) {
	GumboOptions options = kGumboDefaultOptions;

	GumboOutput* output = gumbo_parse_with_options(&options, document.data(), document.length());
	//std::cout << my_serialize(output->document) << std::endl;
	my_tree* root = my_serialize(output->document);
	gumbo_destroy_output(&kGumboDefaultOptions, output);
	return root;
}

my_tree pars_html_from_file(fstream &file) {
	if (!file) {
		std::cout << "File " << " not found!\n";
		exit(EXIT_FAILURE);
	}

	std::string contents;
	file.seekg(0, std::ios::end);
	contents.resize(file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&contents[0], contents.size());
	file.close();

	return *pars_html(contents);
}

my_tree pars_html_from_file_name(const string &file) {

	std::fstream in(file, std::ios::in | std::ios::binary);
	if (!in) {
		std::cout << "File " << file << " not found!\n";
		exit(EXIT_FAILURE);
	}
	std::string contents;
	in.seekg(0, std::ios::end);
	contents.resize(in.tellg());
	in.seekg(0, std::ios::beg);
	in.read(&contents[0], contents.size());
	in.close();

	return *pars_html(contents);
}


