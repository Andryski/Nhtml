#include <fstream>
#include <iostream>
#include <string>
using namespace std;


//#include <google/protobuf/io/gzip_stream.h>
//namespace google::protobuf::io

#include <PB\Head.pb.h>
#include <PB\Light_Element.pb.h>

	//need qt :(
	//#include "../ImageLogic.h"

#include <iostream>
#include <string>
using namespace std;

#include <PB\Page.pb.h>
#include "my_tree.h"
#include "my_serialize.h"


#include "convert.h"


int main(int argc, char** argv) {

	if (argc != 2) {
		cout << "serialize <html filename>\n";
		exit(EXIT_FAILURE);
	}


	my_tree root = pars_html_from_file_name(argv[1]);
	//my_tree root = pars_html_from_file_name(path);

	//vector<string> style = my_get_style(root);

	size_t size_debug = root.size();

	my_tree html = my_find_elem(root, "html");

	cout << "start convert" << endl;

	string str_page1, full_mes, str_head;
	str_page1 = "";
	full_mes = "";
	str_head = "";
	nothtml::Page page1;
	page1.set_vers(3);

	convert_clas converter;

	pair<string, nothtml::css_float>  get_messege = converter.conv_nodes(html);

	if (convert_clas::global_icon_path != "" || convert_clas::global_title != ""){
		// || convert_clas::global_selector_use.size() != 0){
		
		page1.set_head(true);
		nothtml::Head head1;
		if (convert_clas::global_title != "")
			head1.set_title(convert_clas::global_title);
		if (convert_clas::global_icon_path != "")
			head1.set_ico(convert_clas::global_icon_path);
		//if (global_selector_use.size() != 0) {
			/*for each (auto var in global_selector_use)
			{
				auto select_rules = global_selector.get_mmap();
				nothtml::Head_Repeat_CSS *nhcss;

				auto ret = select_rules.equal_range(var.first);
			//	for (auto it = ret.first; it != ret.second; ++it) {

				//	nhcss = head1.add_css();
					//nhcss->set_rule(var.first);
					//nhcss->set_value(it->second.get_all_rules());

			//	}
			}
			//
			//get_use_css();
		*/
		//}
		//head1.SerializeToString(&str_head);
		//str_head = convert(head1);
		string temp;
		head1.SerializeToString(&temp);
		nothtml::Size_Element siz_ele;
		siz_ele.set_size(temp.size());
		siz_ele.SerializeToString(&str_head);
		str_head += temp;
	}

	page1.SerializeToString(&str_page1);
	full_mes += str_page1;
	full_mes += str_head;

	string temp;
	nothtml::Size_Element siz_ele;
	siz_ele.set_size(get_messege.first.size());
	siz_ele.SerializeToString(&temp);
	full_mes += temp;
	full_mes += get_messege.first;

	
	string path_pbf = argv[1];
	path_pbf.pop_back();
	path_pbf.pop_back();
	path_pbf.pop_back();
	path_pbf.pop_back();
	path_pbf += "pbf";
	fstream file(path_pbf, ios::out | ios::binary);
	file << full_mes;
	file.close();

	//system("pause");
	return 0;
}