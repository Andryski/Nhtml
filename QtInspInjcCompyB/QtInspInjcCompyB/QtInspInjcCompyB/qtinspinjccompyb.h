#ifndef QTINSPINJCCOMPYB_H
#define QTINSPINJCCOMPYB_H

#include <QtWidgets/QMainWindow>
#include "ui_qtinspinjccompyb.h"

class QtInspInjcCompyB : public QMainWindow
{
	Q_OBJECT

public:
	QtInspInjcCompyB(QWidget *parent = 0);
	~QtInspInjcCompyB();

public slots:
	void on_pushButton_clicked();

private:
	Ui::QtInspInjcCompyBClass ui;
};

#endif // QTINSPINJCCOMPYB_H
