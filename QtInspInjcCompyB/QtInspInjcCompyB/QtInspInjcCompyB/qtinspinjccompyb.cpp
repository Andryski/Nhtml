#include "qtinspinjccompyb.h"

#include <qsharedmemory.h>
#include <qbuffer.h>


QtInspInjcCompyB::QtInspInjcCompyB(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

QtInspInjcCompyB::~QtInspInjcCompyB()
{

}

void QtInspInjcCompyB::on_pushButton_clicked()
{
	QSharedMemory sharedMemory("qtsharedMem");
	if (sharedMemory.attach()) {
		//sharedMemory.create(1024 * 1024);
		sharedMemory.lock();

		QBuffer buffer;
		QDataStream in(&buffer);

		QVector<QString> vecStr;
		//vecStr.push_back(QString("test1"));
		//vecStr.push_back(QString("test2"));

		//buffer.setData((char*)sharedMemory.constData(), sharedMemory.size());
		buffer.setData((char*)sharedMemory.data(), sharedMemory.size());
		buffer.open(QBuffer::ReadWrite);
		QString temp1;
		in >> temp1;
		vecStr.push_back(temp1);
		in >> temp1;
		vecStr.push_back(temp1);


		sharedMemory.unlock();
		//sharedMemory.detach();
	}
}


