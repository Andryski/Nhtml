# [CompyB]

|  | Windows | Linux | Mac OS | Android |
| -------- | -------- | -------- | -------- | -------- |
| Build | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] |
| Tests | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | [![build status](https://gitlab.com/Andryski/Nhtml/badges/master/build.svg)][ci_result] | --- |

[![coverage report](https://gitlab.com/Andryski/Nhtml/badges/master/coverage.svg)](https://gitlab.com/Andryski/Nhtml/commits/master)
![build status](https://img.shields.io/badge/CI-automated-blue.svg)
![build status](https://img.shields.io/badge/version-v0.0.1-blue.svg)
![build status](https://img.shields.io/badge/uptime-0%25-lightgrey.svg)
![build status](https://img.shields.io/badge/license-unknown-lightgrey.svg)

[ci_result]: https://gitlab.com/Andryski/Nhtml/commits/master

## Supported systems

* Windows 10 (**not(now)** RT)
* Mac OS X 10.13 - Mac OS X 10.14.2
* Ubuntu ???
* Android ???

## Third-party

* [Qt 5.?.? - 5.12.?](https://www.qt.io/) ([LGPL](http://doc.qt.io/qt-5/lgpl.html))
* [CMakeHelpers](https://github.com/halex2005/CMakeHelpers) ([MIT](https://github.com/halex2005/CMakeHelpers/blob/master/LICENSE))
* [di](https://github.com/boost-experimental/di) ([Boost](https://www.boost.org/LICENSE_1_0.txt))
* [googletest](https://github.com/google/googletest) ([BSD 3-Clause](https://github.com/google/googletest/blob/master/LICENSE))
* [qt-android-cmake](https://github.com/LaurentGomila/qt-android-cmake) ([self](https://github.com/LaurentGomila/qt-android-cmake/blob/master/license.txt))
* [Protocol Buffers](https://github.com/protocolbuffers/protobuf) ([self](https://github.com/protocolbuffers/protobuf/blob/master/LICENSE))
* [fruit](https://github.com/google/fruit) ([Apache-2.0](https://github.com/google/fruit/blob/master/COPYING))
* [crashpad](https://github.com/chromium/crashpad) ([Apache-2.0](https://github.com/chromium/crashpad/blob/master/LICENSE))

(Need add)
* Google Breakpad ([License](https://chromium.googlesource.com/breakpad/breakpad/+/master/LICENSE))
* Google Crashpad ([Apache License 2.0](https://chromium.googlesource.com/crashpad/crashpad/+/master/LICENSE))

(On windows)
* Ninja ([Apache License 2.0](https://github.com/ninja-build/ninja/blob/master/COPYING))


## Build instructions

* git submodule init
* git submodule update
* [Windows][Visual Studio 2017][msvc]
* [Mac OS][Xcode 10][xcode]
* [Linux][cmake]
* Android ...

## About

This project was created by [Andry](https://t.me/Andryski)
<[telegram](https://t.me/Andryski)>
