//
// Created by Andry on 2019-03-09.
//
#pragma once
#include <Api/IRAndC.h>

#include "export.h"


class Native_Module : public IRAndC {
public:
	Native_Module();
	virtual ~Native_Module();

	void Start() override;

	void Start(std::string string) override;

	void Load() override;

	void Unload() override;

	void Translate() override;

	std::string gettest() override;

	void SetCallBack2(IDinoAPI *call) override;

	void PushButton(int id) override;

private:
	IDinoAPI *API;
};

extern "C"
//EXPORT
IRAndC *getNewClass(){
	return new Native_Module();
}

extern "C" inline
void test1(){}

extern "C"
void test2(){}

EXPORT
void test3(){}

extern "C" inline
EXPORT
void test4(){}